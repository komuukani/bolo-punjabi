-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 09, 2024 at 04:00 AM
-- Server version: 8.0.31
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bolo_punjabi`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `ProG`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ProG` ()   begin 
SELECT * FROM hs_hr_employee_leave_quota;
end$$

DROP PROCEDURE IF EXISTS `ProGS`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ProGS` ()   begin 
SELECT * FROM hs_hr_employee_leave_quota;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_aboutus`
--

DROP TABLE IF EXISTS `tbl_aboutus`;
CREATE TABLE IF NOT EXISTS `tbl_aboutus` (
  `aboutus_id` int NOT NULL AUTO_INCREMENT,
  `about` text NOT NULL,
  PRIMARY KEY (`aboutus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_aboutus`
--

INSERT INTO `tbl_aboutus` (`aboutus_id`, `about`) VALUES
(7, '<p><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"></span></p><h3 style=\"font-family: sans-serif;\"><font color=\"#efefef\">About</font><font color=\"#ff0000\"> Armster</font></h3><p><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><a href=\"https://skin-2-love.com/\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; box-shadow: none;\">Skin-2-Love</a></span>&nbsp;was born out of a thirst for knowledge and a desire to act against pollution and waste.&nbsp; As many of you have done, I wanted to know how skincare was made.&nbsp; The purple unicorn, adored by many, understood by few! A few years ago, I was standing in my local Chemist warehouse, looking for a moisturiser as I had run out the night before.&nbsp; At this point I was unemployed (Thanks Covid), and price really was important, however, as someone who applies a moisturiser at least twice a day I did not have an option NOT to get it.&nbsp; I bought the cheapest facial moisturiser I could find ($7 on special) but it was watery and underwhelming, and I felt the difference on my skin immediately.&nbsp; So started my journey…. Why would one product be luxurious while another not, why do some break the bank and others do not?</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `admin_id` int NOT NULL AUTO_INCREMENT,
  `user_role_id` int NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `path` varchar(500) NOT NULL COMMENT 'Profile Photo',
  `address` varchar(200) NOT NULL,
  `bio` varchar(1000) NOT NULL,
  `theme` varchar(10) NOT NULL,
  `lastseen` datetime NOT NULL,
  `register_date` date NOT NULL,
  `status` int NOT NULL COMMENT 'Profile Status',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `user_role_id`, `admin_name`, `email`, `phone`, `password`, `path`, `address`, `bio`, `theme`, `lastseen`, `register_date`, `status`) VALUES
(2, 1, 'Admin', 'admin@gmail.com', '+64 (0) 22 079 8832', '341c3862157a967b269645a552fcf45d65e153abebb3edd4b915837cbca8ca2a1f93c81e382746cd27076e9106654d17232f359fc72c43e428c2649dbc9c8990crWE3iM4+YPFHWxae4L9Q9pnXdWofBy6AuSkgPL7flY=', './admin_asset/profile/8ff6e62a2d1ad156ab21f2edb6bca80d.png', 'North Shore, Auckland, New Zealand', '<p>Hello I am admin</p>', 'theme-5', '2024-03-08 11:47:06', '2022-09-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

DROP TABLE IF EXISTS `tbl_banner`;
CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `banner_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `subtitle` varchar(500) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `position` int NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`banner_id`, `title`, `subtitle`, `path`, `position`) VALUES
(40, 'Beauty Inspired <br> by Real Life', 'Made using clean, non-toxic ingredients, our products are designed for everyone.', './admin_asset/banner/30eacafde8ff62bc3a5a67ec6eb544a9.jpg', 1),
(41, 'Get The Perfectly <br> Hydrated Skin', 'Made using clean, non-toxic ingredients, our products are designed for everyone.', './admin_asset/banner/06f649aa79967346497bfcd40f8c0f69.jpg', 2),
(42, '', '', './admin_asset/banner/d6564dfb4a5b811b63041032cf9acae9.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bill`
--

DROP TABLE IF EXISTS `tbl_bill`;
CREATE TABLE IF NOT EXISTS `tbl_bill` (
  `bill_id` int NOT NULL AUTO_INCREMENT,
  `order_id` char(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `unique_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `register_id` int NOT NULL,
  `user_type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'regular',
  `fname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `subtotal` double NOT NULL,
  `shipping_charge` double NOT NULL,
  `tax` double NOT NULL,
  `netprice` double NOT NULL,
  `currency_code` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `country_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `country` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `postal_code` int NOT NULL,
  `coupon_id` int DEFAULT NULL,
  `notes` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_type` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `card` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `upi_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `upi_transaction_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `payment_status` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_date` date DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `Register ID` (`register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_bill`
--

INSERT INTO `tbl_bill` (`bill_id`, `order_id`, `unique_id`, `register_id`, `user_type`, `fname`, `email`, `phone`, `subtotal`, `shipping_charge`, `tax`, `netprice`, `currency_code`, `country_name`, `country`, `city`, `state`, `address`, `postal_code`, `coupon_id`, `notes`, `entry_type`, `transaction_id`, `card`, `upi_id`, `upi_transaction_id`, `payment_status`, `entry_date`, `status`) VALUES
(1, 'nishant j thummar-2023120', '', 1, 'regular', '', '', '', 0, 0, 0, 2500, '', '', 'india', 'surat', 'gujarat', '170 shubham row house', 395006, NULL, '', '', 'pay_N8bCNaC3mWEI1R', '', '', '', '', '2023-12-05', NULL),
(2, 'nishant j thummar-2023120', '', 1, 'regular', '', '', '', 0, 0, 0, 4500, '', '', 'india', 'surat', 'gujarat', '170 shubham row house', 395006, NULL, '', '', 'pay_N8bJEsL3TjObPf', '', '', '', '', '2023-12-05', NULL),
(3, 'nishant j thummar-2023120', '', 1, 'regular', '', '', '', 0, 0, 0, 4700, '', '', 'india', 'surat', 'gujarat', '170 shubham row house', 395006, NULL, '', '', 'pay_N8bL5F4cPAvftH', '', '', '', '', '2023-12-05', NULL),
(4, 'nishant j thummar-2023120', '::1', 1, 'regular', 'nishant j thummar', 'nishant@gmail.com', '8460124262', 0, 0, 0, 5100, '', '', 'india', 'surat', 'gujarat', '170 shubham row house', 395006, NULL, '', '', 'pay_N8chLfvZfravPt', '{\"id\":\"card_N8chLiuOyTc9uk\",\"entity\":\"card\",\"name\":\"\",\"last4\":\"1111\",\"network\":\"Visa\",\"type\":\"debit\",\"issuer\":null,\"international\":false,\"emi\":false,\"sub_type\":\"consumer\",\"token_iin\":null}', '', '', 'captured', '2023-12-05', NULL),
(5, 'nishant j thummar-2023120', '::1', 1, 'regular', 'nishant j thummar', 'nishant@gmail.com', '8460124262', 0, 0, 0, 2500, '', '', 'india', 'surat', 'gujarat', '170 shubham row house', 395006, NULL, '', '', 'pay_N8cp8taPYgCBq6', '', '{\"vpa\":\"success@razorpay\"}', '6C3D2DE36AC96601C49186834782798C', 'captured', '2023-12-05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

DROP TABLE IF EXISTS `tbl_blog`;
CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `path` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_desc` varchar(500) NOT NULL,
  `blogdate` date NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `title`, `slug`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
(11, 'Vitamin E', 'vitamin-e', './admin_asset/blog/0fe93e311e6dd17e97c9e153be4525a0.png', '<p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Just about everyone knows about Vitamin E.&nbsp; I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.&nbsp; It is not.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Vitamin E is an oil-soluble antioxidant. It has an amber colour and is a&nbsp;<span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">very</span>&nbsp;sticky oil.&nbsp; Pure Vitamin E is therefore not fit for direct application to the skin.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Regarding Vitamin E and its function as an antioxidant, this is relevant to your skin but also in oils.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">It protects our skin from the damage caused by excess free radicals (aka oxidative stress) and it slows down the oxidation of oils to prevent rancidity.&nbsp; This makes it a powerful ingredient to include in skincare formulations.&nbsp; It is not the only antioxidant used for this purpose, but it is certainly one of the most common.&nbsp;&nbsp;<a href=\"https://formulabotanica.com/antioxidants-in-cosmetics/#:~:text=Antioxidants%20and%20Cosmetic%20Formulations\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s; cursor: pointer; text-underline-offset: 3px;\">Formulabotanica</a>&nbsp;has an in-depth article that explains how that all works.&nbsp; In short, some ingredients are prone to oxidation, caused by heat, light, metal ions and oxygen exposure, which makes them unstable and therefore reduces their shelf life.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The article goes into more details around some common Primary and Secondary antioxidants.&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Depending on the product, it’s ingredients and purpose, a formulator may use one or a combination of antioxidants to protect the oil in their formulation from oxidising.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">With that said, many antioxidants used in skincare also act on our skin.&nbsp; This is one of the reasons why Vitamin E is so popular, it acts in two ways and therefore eliminates the need to double up on these ingredients, leaving more “room” for other actives and “goody” ingredients.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Secondary antioxidants like Chelating agents binds to metal ions which can cause oxidation and is well placed for formulations with a high percentage of water.&nbsp; Carotenoids (another much less common secondary antioxidant) depending on the product may be best placed.</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">When we are born, our antioxidant production is at a maximum whereas ROS (Reactive Oxygen species) production is very low.&nbsp; As we age, this changes.&nbsp; ROS is an important part of cell regeneration, however, as we age and with pollution around us, the ROS production, and the reduced antioxidant production causes damage to our skin.&nbsp; It is therefore important to wear skincare that will protect you from the elements.</p><figure class=\"wp-block-image size-full\" style=\"margin: 1.5em auto; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><img decoding=\"async\" width=\"554\" height=\"493\" src=\"https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants.png\" alt=\"\" class=\"wp-image-4593\" srcset=\"https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants.png 554w, https://skin-2-love.com/wp-content/uploads/2023/05/Antioxidants-300x267.png 300w\" sizes=\"(max-width: 554px) 100vw, 554px\" style=\"height: auto; max-width: 100%; margin: 0px auto; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: bottom; display: block;\"></figure><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Wear sunscreen.&nbsp; It is such a small thing that makes such a big difference.&nbsp; We won’t be going into the benefits of sunscreen here, however, I’m sure you have a voice inside you saying “yes, that is a good idea” right now.&nbsp;&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The point is, buying a product specifically because it promotes its Vitamin E content does not mean you are getting more bang for your buck.&nbsp; You are better off looking at what else it contains as an antioxidant with any product containing oil is a given (from a good manufacturing practice perspective)&nbsp;</p><p class=\"has-medium-font-size\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Look for the actives in the product and then do an internet search to the value that an active brings to your skincare routine.</p>', 'Vitamin E', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', 'Just about everyone knows about Vitamin E.  I mistakenly believed that the beautiful tissue oil my mother uses worked so well because it was a pure Vitamin E oil.  It is not.', '2023-09-29'),
(12, 'How do I use skincare products', 'how-do-i-use-skincare-products', './admin_asset/blog/2b4f0dd8f63bf4a070c66ef155964cfe.png', '<p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Today, you are probably going to learn something new and if you adopt the new ways of applying your skincare you will save money too!</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The skin can only absorb so much and must be prepped to absorb your product. When you have been out and about and you have sweat, dirt particles and other impurities on your skin, applying a product is not going to be very useful (the exception is sunblock when you protecting yourself while out)</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Your skincare routine does not have to be complicated. 3 Steps is all you need to give your skin the best chance of staying radiant. With that said, these days there are a ton of products and the list is growing week by week.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">The table below gives you a suggested order to apply your skincare,</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">The general rule of thumb is to apply your product from the least viscous (runny) to most viscous (oily serums etc.)</span></p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">That rule isn’t 100% accurate but a good starting point. To understand in which order you should apply what, you really need to understand “what” each product does.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"></div></div>', 'How do I use skincare products', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', 'If you are like most people you dip your finger into your skincare and then rub the lotion or cream into your skin until it is all absorbed. You learned from television how to apply your skincare or you saw a relative or friend do it and figured out the rest from there.', '2023-09-29'),
(13, 'THE SECRET OF SERUMS', 'the-secret-of-serums', './admin_asset/blog/f4417a87966238d208a69bb98f9093b7.png', '<div class=\"entry-content clr\" itemprop=\"text\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; counter-reset: footnotes 0;\"><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Serums offer a way to deliver high quality actives to the deeper layers of the skin.&nbsp; One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.</p><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">Serums can help balance your skin’s PH and help protect your skin from harsh elements.&nbsp; Our serum is particularly good with reducing fine lines &amp; wrinkles and smoothing out skin texture.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"><figure class=\"wp-block-table\" style=\"margin-bottom: 1em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; overflow-x: auto;\"><table style=\"margin: 0px 0px 2.618em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1250px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SERUM</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">DESCRIPTION</span></font></span></td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">VITAMIN C</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Vitamin C serums can be used at the same time as retinoids, however, make sure you apply your Vitamin C serum first, then make sure it dries properly before adding your retinol serum.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">RETINOIDS (Retinol or Vitamin A)</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Retinol&nbsp;(from Retinoids) form part of the Vitamin A family.<br>Retinol stimulates collagen production and cell renewal improving skin texture and evens tones.&nbsp; It targets fine lines and wrinkles.<br>Must be applied to a dry skin. If you apply your retinol in the morning, please use a sunscreen SPF 30 or higher as it is phototoxic (Makes your skin sensitive to light and cause damage)</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SALICYLIC ACID</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">This treats skin conditions by softening and loosening dry, scaly, or thickened skin.&nbsp; It reduces swelling and redness and clears pores.&nbsp; It’s very good for reducing acne, pimples, white and blackheads.<br>It penetrates the deeper layers of your skin to dissolve keratin.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">HYALURONIC ACID</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Hyaluronic Acid can hold up to 1000 times its weight in water.&nbsp;<br>Hyaluronic Acid helps your skin to stretch and has also been shown to reduce scarring, reduce wrinkles and help wounds hear faster. It is a powerful moisturiser (plumps the outer layers of skin) and humectant (used to prevent the loss of moisture).</td></tr></tbody></table></figure></div></div><div class=\"wp-block-group is-layout-constrained wp-block-group-is-layout-constrained\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><div class=\"wp-block-group__inner-container\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: var(--wp--style--global--content-size); margin-right: auto !important; margin-left: auto !important;\"></div></div></div><div class=\"post-tags clr\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span class=\"owp-tag-text\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">TAGS:&nbsp;</span><a href=\"https://skin-2-love.com/tag/dry-skin/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">DRY SKIN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/humectant/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HUMECTANT</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/hyaluronic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HYALURONIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/pores/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">PORES</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/retinol/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/retinols/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOLS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/salicylic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SALICYLIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/serums/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SERUMS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/skin-moisture/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN MOISTURE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/vitamin-a/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN A</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span>&nbsp;<a href=\"https://skin-2-love.com/tag/vitamin-c/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN C</a></div>', 'THE SECRET OF SERUMS', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', 'Serums offer a way to deliver high quality actives to the deeper layers of the skin.  One may think that an oily serum would be heavy on the skin, but in contrast it is most often not. Providing that the ingredients used are of a high quality and are fast absorbing, you will notice that your skin readily accepts oil.', '2023-09-29');
INSERT INTO `tbl_blog` (`blog_id`, `title`, `slug`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
(14, 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'whats-really-in-my-skin-care-products', './admin_asset/blog/bbae51e74453b17cc614617952cfb0f2.jpg', '<div class=\"entry-content clr\" itemprop=\"text\" style=\"margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; counter-reset: footnotes 0;\"><p style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\">What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.</p><div class=\"wp-block-columns is-layout-flex wp-container-2 wp-block-columns-is-layout-flex\" style=\"margin: 1.5em auto; gap: 2em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; display: flex; align-items: center; flex-wrap: nowrap; max-width: 1250px;\"><div class=\"wp-block-column is-layout-flow wp-block-column-is-layout-flow\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; flex-grow: 0; min-width: 0px; overflow-wrap: break-word; word-break: break-word; flex-basis: 100%;\"><figure class=\"wp-block-table\" style=\"margin-bottom: 1em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; overflow-x: auto;\"><table style=\"margin: 0px 0px 2.618em; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1250px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">ISSUE</span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">LOOK FOR THESE PRODUCTS IN YOUR SKINCARE</span></td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">SIGNS OF AGING</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Hyaluronic Acid, Hydrolised Quinoa, L Leucine, L Proline, Marine Collagen, Green Tea extract, Vegetable Glycerin, Argan Oil, Baobab Oil, Jojoba Oil, Lanolin, Rosehip Oil, vitamin A/retinoids, vitamin C, Vitamin E. SPF-boosting ingredients (e.g., Zinc Oxide, Titanium Dioxide) Good Sunscreen to prevent further damage)</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">DRYNESS</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Hyaluronic Acid, Green Tea Extract, Sweet Orange Essential Oil, Baobab Oil, Macadamia oil, Rosehip oil, Vitamin C, Rose Hydrosol and Vitamin E.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">PIGMENTATION</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Niacinamide, Green Tea Extract, Bakuchi Oil, Glycerin, Rose Hydrosol, Kojic Acid, AHAs, BHA, Hydroquinone, Vitamin C.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ACNE</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Alpha Bisabolol, Bakuchiol, Kelp Cellular Extract, Niacinamide, Salicylic Acid, Green Tea extract, Witch Hazel Extract, Lactic Acid, Bakuchi Oil. Essential oils like Egyptian Geranium, Grapefruit, Rose Geranium , Sweet Orange and Ylang Ylang. Glycerin, Jojoba Oil, Rosehip Oil. Hydrosols like Lavender, Neroli and Rose hydrosol. Azelaic acid, Vitamin A/Retinoids, Benzoyl Peroxide.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ROSACEA</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Bakuchiol, Niacinamide, Green Tea Extract, Bakuchi Oil, Egyptian Geranium Essential Oil, Rose Geranium Essential Oil, Rose Hydrosol and Sulphur.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">PSORIASIS</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Hyaluronic Acid, Green Tea Extract, Glycerin, Rose Hydrosol, Vitamin A/retinoids, Vitamin D, Salicylic Acid, Urea and Lactic Acid.</td></tr><tr style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\"><span style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><font face=\"inherit\"><span style=\"border-style: initial; border-color: initial; border-image: initial; outline-color: initial; outline-style: initial; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; font-weight: inherit;\">ECZEMA</span></font></span></td><td style=\"margin: 0px; padding: 0.5em; border-color: initial; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; word-break: normal;\">Allantoin, Bakuchiol, Green Tea extract, Glycerin, Rose Hydrosol.</td></tr></tbody></table></figure></div></div><div class=\"wp-block-group is-layout-constrained wp-block-group-is-layout-constrained\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\"><div class=\"wp-block-group__inner-container\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: var(--wp--style--global--content-size); margin-right: auto !important; margin-left: auto !important;\"></div></div></div><div class=\"post-tags clr\" style=\"margin: 1.5em auto; padding: 0px 20px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; max-width: 1250px;\"><span class=\"owp-tag-text\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">TAGS: </span><a href=\"https://skin-2-love.com/tag/acne/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ACNE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/aging/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">AGING</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/argan-oil/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ARGAN OIL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/dry-skin/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">DRY SKIN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/eczema/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ECZEMA</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/green-tea-extract/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">GREEN TEA EXTRACT</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/hyaluronic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">HYALURONIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/jojoba-oil/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">JOJOBA OIL</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/marine-collagen/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">MARINE COLLAGEN</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/psoriasis/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">PSORIASIS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/retinols/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">RETINOLS</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/rosacea/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ROSACEA</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/rosehip/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">ROSEHIP</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/salicylic-acid/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SALICYLIC ACID</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skin-care/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN CARE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skin-pigmentation/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKIN PIGMENTATION</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/skincare/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">SKINCARE</a><span class=\"owp-sep\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">,</span> <a href=\"https://skin-2-love.com/tag/vitamin-c/\" rel=\"tag\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-alternates: inherit; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; transition-duration: 0.3s;\">VITAMIN C</a></div>', 'WHAT’S REALLY IN MY SKIN CARE PRODUCTS?', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', 'What ingredients are going to work best for you? Skin treatment options focus on the common issues we can have with our skin. We have listed some of our favourite ingredients.', '2023-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

DROP TABLE IF EXISTS `tbl_cart`;
CREATE TABLE IF NOT EXISTS `tbl_cart` (
  `cart_id` int NOT NULL AUTO_INCREMENT,
  `register_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `price` double DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `netprice` double DEFAULT NULL,
  `unique_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `path` varchar(500) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `title`, `slug`, `description`, `path`) VALUES
(11, 'Language Learning', 'language-learning', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px; text-align: center;\">Lore Ipsum a simply dummy text of the printin</span><br></p>', './admin_asset/category/970f7906d2280183d09c7a5f44206583.jpg'),
(12, 'Graphics Design', 'graphics-design', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px; text-align: center;\">Lore Ipsum a simply dummy text of the printin</span><br></p>', './admin_asset/category/6d4c71d0831d917f29f7b8d16b6c1aeb.jpg'),
(13, 'Web designer', 'web-designer', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px; text-align: center;\">Lore Ipsum a simply dummy text of the printin</span><br></p>', './admin_asset/category/1cb1f084d95ac7c69b5378596c57c913.jpg'),
(14, 'Web Development', 'web-development', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px; text-align: center;\">Lore Ipsum a simply dummy text of the printin</span><br></p>', './admin_asset/category/39bad3cc632bc4c541ee6b7a61afbf4c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

DROP TABLE IF EXISTS `tbl_client`;
CREATE TABLE IF NOT EXISTS `tbl_client` (
  `client_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `path` varchar(500) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`client_id`, `title`, `path`) VALUES
(10, 'Demo Client', './admin_asset/client/46fa3ab5f0eb5af7f7af9bafbf83c0fb.png'),
(11, 'parttner 2', './admin_asset/client/e90f4d7acfbc7d3dcc22d3b11d035cca.png'),
(12, 'insta', './admin_asset/client/445a04d7ee0a92883a17317216b2e482.png'),
(13, 'raja', './admin_asset/client/d9eb75fc13f75c1955d06215d8dda11a.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `subject` varchar(2000) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `datetime` varchar(50) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `name`, `email`, `phone`, `subject`, `message`, `datetime`) VALUES
(27, 'nishant', 'nishant@gmail.com', '789461320', '', 'demo mail', '2023-08-24 17:50:23'),
(28, 'nishant thummar', 'nishant@gmail.com', '5783453489', '', 'demo mail', '2023-08-24 17:51:25'),
(31, 'anuj', 'anuj@gmail.com', '789456130', 'Question or Comment', 'demo mail', '2023-08-31 19:49:02'),
(32, 'nishatn', 'nishatn@gmail.com', '8460124263', '', 'sdfsdfsdf', '2023-10-01 19:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control`
--

DROP TABLE IF EXISTS `tbl_control`;
CREATE TABLE IF NOT EXISTS `tbl_control` (
  `control_id` int NOT NULL AUTO_INCREMENT,
  `aboutus` enum('1','0') NOT NULL,
  `banner` enum('1','0') NOT NULL,
  `blog` enum('1','0') NOT NULL,
  `category` enum('1','0') NOT NULL,
  `client` enum('1','0') NOT NULL,
  `email_subscriber` enum('1','0') NOT NULL,
  `contact` enum('1','0') NOT NULL,
  `feedback` enum('1','0') NOT NULL,
  `gallery` enum('1','0') NOT NULL,
  `inquiry` enum('1','0') NOT NULL,
  `mainmenu` enum('1','0') NOT NULL,
  `permission` enum('1','0') NOT NULL,
  `policy` enum('1','0') NOT NULL,
  `review` enum('1','0') NOT NULL,
  `role` enum('1','0') NOT NULL,
  `seo` enum('1','0') NOT NULL,
  `subcategory` enum('1','0') NOT NULL,
  `submenu` enum('1','0') NOT NULL,
  `terms` enum('1','0') NOT NULL,
  `team` enum('1','0') NOT NULL,
  `user` enum('1','0') NOT NULL,
  `video` enum('1','0') NOT NULL,
  PRIMARY KEY (`control_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_control`
--

INSERT INTO `tbl_control` (`control_id`, `aboutus`, `banner`, `blog`, `category`, `client`, `email_subscriber`, `contact`, `feedback`, `gallery`, `inquiry`, `mainmenu`, `permission`, `policy`, `review`, `role`, `seo`, `subcategory`, `submenu`, `terms`, `team`, `user`, `video`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_control_feature`
--

DROP TABLE IF EXISTS `tbl_control_feature`;
CREATE TABLE IF NOT EXISTS `tbl_control_feature` (
  `control_feature_id` int NOT NULL AUTO_INCREMENT,
  `general` varchar(200) NOT NULL,
  `social` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `security` varchar(200) NOT NULL,
  `system` varchar(200) NOT NULL,
  `captcha` varchar(200) NOT NULL,
  `theme` varchar(200) NOT NULL,
  `payment` varchar(200) NOT NULL,
  `language` varchar(200) NOT NULL,
  `backup` varchar(200) NOT NULL,
  PRIMARY KEY (`control_feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_control_feature`
--

INSERT INTO `tbl_control_feature` (`control_feature_id`, `general`, `social`, `email`, `security`, `system`, `captcha`, `theme`, `payment`, `language`, `backup`) VALUES
(1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(2, 'fas fa-cog', 'fas fa-search', 'fas fa-envelope', 'fas fa-lock', 'fas fa-power-off', 'fas fa-sync-alt', 'fas fa-palette', 'far fa-credit-card', 'fas fa-language', 'fas fa-database'),
(3, 'General settings such as, site title, site description, address and so on.', 'Search engine optimization settings, such as meta tags and social media.', 'Email SMTP settings, notifications and others related to email.', 'Security settings such as change admin login password and others.', 'PHP version settings, time zones and other environments.', 'Google Captcha Security settings for contact, inquiry and other form.', 'Change admin theme and other appearance changes as per your choice.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Set the backup settings below');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coupon`
--

DROP TABLE IF EXISTS `tbl_coupon`;
CREATE TABLE IF NOT EXISTS `tbl_coupon` (
  `coupon_id` int NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `coupon_description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `coupon_percentage` int NOT NULL,
  `expiry_date` date NOT NULL,
  `product` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `minimum_subtotal_india` int NOT NULL,
  `maximum_subtotal_india` int NOT NULL,
  `minimum_subtotal_usa` int NOT NULL,
  `maximum_subtotal_usa` int NOT NULL,
  `free_shipping` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` tinyint NOT NULL,
  `entry_date` date DEFAULT NULL,
  `modify_date` date DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `coupon_code` (`coupon_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_coupon`
--

INSERT INTO `tbl_coupon` (`coupon_id`, `coupon_code`, `coupon_description`, `coupon_percentage`, `expiry_date`, `product`, `category`, `minimum_subtotal_india`, `maximum_subtotal_india`, `minimum_subtotal_usa`, `maximum_subtotal_usa`, `free_shipping`, `status`, `entry_date`, `modify_date`) VALUES
(1, 'freecop', 'test', 10, '2023-04-13', '[\"3\",\"4\",\"9\",\"11\"]', '[\"15\"]', 2500, 3500, 50, 5, 'no', 1, '2023-04-11', '2023-04-13'),
(2, 'free250', 'fsd fdemo ', 15, '2023-05-23', '[\"6\",\"7\",\"13\"]', '', 500, 50, 60, 6, NULL, 1, '2023-04-13', '2023-05-04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

DROP TABLE IF EXISTS `tbl_courses`;
CREATE TABLE IF NOT EXISTS `tbl_courses` (
  `courses_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category_id` int NOT NULL,
  `photo` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `entry_date` timestamp NOT NULL,
  `modify_date` timestamp NOT NULL,
  PRIMARY KEY (`courses_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tbl_courses`
--

INSERT INTO `tbl_courses` (`courses_id`, `title`, `category_id`, `photo`, `description`, `status`, `entry_date`, `modify_date`) VALUES
(1, 'Master Native English Class This Speaking Skills', 11, './admin_asset/courses/9dbef23c98fcd1772bea7ec3fa046df9.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '0000-00-00 00:00:00', '2024-03-08 12:21:39'),
(2, 'The Basic Of Financial Analyst Online Course', 11, './admin_asset/courses/7ed2366ae922f34231d74a81c59f3eb7.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '2024-03-08 07:56:59', '2024-03-08 12:21:27'),
(3, 'Introduction to Javascript for The Beginners', 12, './admin_asset/courses/b004ff3de1dbc1311145417b9d779e54.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '2024-03-08 08:03:01', '2024-03-08 12:21:04'),
(4, 'Oracle SQL Developer : Essentials Tips and Tricks', 11, './admin_asset/courses/a7388cb177eecb42704ff835f9ae44af.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '2024-03-08 08:03:28', '2024-03-08 12:21:10'),
(5, 'Java Programming Masterclass for Software Developers', 12, './admin_asset/courses/a86cb4240e87d66b62db66f4438f3e81.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '2024-03-08 08:03:59', '2024-03-08 12:20:21'),
(6, 'Python Django Web Development: To-Do App', 11, './admin_asset/courses/66f91fa8d18416f47382dbd050a165c3.jpg', '<p><span style=\"font-family: Urbanist, sans-serif; font-size: 16px;\">There are many variations of passages of Lorem Ipsaums available, but the majority have suffered alteration. generators on the Internet tend to repeat.</span><br></p>', 0, '2024-03-08 08:04:23', '2024-03-08 12:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_subscriber`
--

DROP TABLE IF EXISTS `tbl_email_subscriber`;
CREATE TABLE IF NOT EXISTS `tbl_email_subscriber` (
  `email_subscriber_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(500) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_subscriber_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email_subscriber`
--

INSERT INTO `tbl_email_subscriber` (`email_subscriber_id`, `email`, `datetime`) VALUES
(9, 'example1@mail.com', '2022-10-06 12:50:35'),
(10, 'example2@mail.com', '2022-10-06 12:50:35'),
(11, 'fgfg@sdf.dfgfd', '2023-09-30 18:55:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

DROP TABLE IF EXISTS `tbl_faq`;
CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `faq_id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`faq_id`, `question`, `answer`, `entry_date`, `modify_date`) VALUES
(7, 'HOW LONG WILL IT TAKE FOR MY ORDER TO BE READY?', 'Most of our standard print products are completed within 24 – 48 hours from the time we receive your artwork.\r\n\r\nPlease check with us for custom orders or orders over 1,000 qty to confirm turnaround time.', '2023-09-12', '0000-00-00'),
(8, 'WHAT TYPE OF FILE SHOULD I SEND FOR THE BEST QUALITY PRINT?', 'A PDF file is the best format for print. If you do not have a PDF file of your design, you can also send us a JPEG or PNG file if it is high resolution.\r\n\r\nMake sure there is a 1/8″ bleed all around or make sure the text / images are not near the edge of the design, as we need a 1/8″ trim / safety margin for full bleed.', '2023-09-12', '0000-00-00'),
(9, 'HOW DO I ADD BLEED TO MY FILE?', 'The design program you are working in, usually has a function to add bleed.\r\n\r\nIf you cannot add bleed, just make sure you have at least 1/8″ all around the design with no text or images that will be trimmed off.', '2023-09-12', '0000-00-00'),
(10, 'HOW DO I GET IN CONTACT FOR FURTHER QUESTIONS?', 'If you have any questions regarding your design or print project please email us at info@dhillonprinting.com or you can call us during business hours and we will be happy to assist you. Call us at 403 615 2727 between 10am – 6pm, Monday to Friday.', '2023-09-12', '0000-00-00'),
(11, 'WHAT FORMAT OR SETTINGS DO I USE TO SAVE MY FILE FOR THE BEST PRINT QUALITY?', 'The best quality for print is 300dpi resolution in CMYK colour mode (not RGB).\r\n\r\nCMYK is used for print and RGB is used for screen graphics.', '2023-09-12', '0000-00-00'),
(12, 'WHERE IS YOUR SHOP LOCATED?', 'Unit# 521, 5075 Falconridge Blvd. NE, Calgary, AB T3J 3K9', '2023-09-12', '0000-00-00'),
(13, 'What services does Dhillon Printing offer?', 'Dhillon Printing offers a wide range of printing services, including business cards, flyers, brochures, banners, posters, stationery, and more. We also provide graphic design services to assist with your printing needs.', '2023-09-12', '0000-00-00'),
(14, 'How can I request a quote for a printing project?', 'You can request a quote by visiting our website and using our online quote request form. Alternatively, you can contact our customer service team via phone or email to discuss your project and get a quote.', '2023-09-12', '0000-00-00'),
(15, 'Can you help with design and artwork for my printing project?', 'Yes, we offer graphic design services to help create or enhance artwork for your printing project. Our design team can work with you to ensure your materials look professional and eye-catching.', '2023-09-12', '0000-00-00'),
(16, 'Do you offer bulk printing discounts?', 'Yes, we offer discounts for bulk or large quantity printing orders. The exact discount may vary based on the project, so please reach out to our team for pricing details.', '2023-09-12', '0000-00-00'),
(17, 'What types of paper and finishes are available for printing projects?', 'We offer a variety of paper options, including gloss, matte, and specialty papers. We also provide different finishes such as UV coating, embossing, and more. You can discuss your preferences with our team to choose the best options for your project.', '2023-09-12', '0000-00-00'),
(18, 'Can you handle rush orders or same-day printing?', 'Depending on our current workload, we may be able to accommodate rush orders or same-day printing requests. Please contact us as soon as possible to check availability.', '2023-09-12', '0000-00-00'),
(19, 'What payment methods do you accept?', 'We accept various payment methods, including credit cards, debit cards, checks, and bank transfers. Payment details will be provided when you place your order.', '2023-09-12', '0000-00-00'),
(20, 'How can I place an order with Dhillon Printing?', 'You can place an order by visiting our website and using our online order form, by calling our customer service team, or by visiting our physical location. Our team will guide you through the process.', '2023-09-12', '0000-00-00'),
(21, 'How can I share my Canva design file with Dhillon Printing?', 'To share your Canva design file with Dhillon Printing, follow these steps:\r\n1. Open your Canva design project.\r\n2. Click on the \"Share\" button in the upper right corner.\r\n3. Choose the \"Share a link\" option.\r\n4. Click \"Copy Link\" to copy the shareable link to your clipboard.\r\n5. Send the copied link to Dhillon Printing through email or the preferred communication method.', '2023-09-12', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

DROP TABLE IF EXISTS `tbl_feedback`;
CREATE TABLE IF NOT EXISTS `tbl_feedback` (
  `feedback_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `message` varchar(1500) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

DROP TABLE IF EXISTS `tbl_gallery`;
CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `gallery_id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `path` varchar(2000) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inquiry`
--

DROP TABLE IF EXISTS `tbl_inquiry`;
CREATE TABLE IF NOT EXISTS `tbl_inquiry` (
  `inquiry_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `customer_id` int NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` varchar(2000) NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int NOT NULL COMMENT '0-pending | 1- approve',
  PRIMARY KEY (`inquiry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mainmenu`
--

DROP TABLE IF EXISTS `tbl_mainmenu`;
CREATE TABLE IF NOT EXISTS `tbl_mainmenu` (
  `mainmenu_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int NOT NULL,
  `icon` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`mainmenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mainmenu`
--

INSERT INTO `tbl_mainmenu` (`mainmenu_id`, `title`, `slug`, `position`, `icon`, `controller`, `url`, `status`) VALUES
(22, 'Manage FAQ\'s', 'manage-faq/?(:any)?/?(:any)?', 1, 'fas fa-question-circle', 'faq', 'Admin/Pages/faq/$2/$3', 1),
(30, 'Courses', 'manage-courses/?(:any)?/?(:any)?', 3, 'fas fa-book-open', 'courses', 'Admin/Pages/courses/$2/$3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `menu_id` int NOT NULL AUTO_INCREMENT,
  `text` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `href` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'slug',
  `target` varchar(10) NOT NULL,
  `parent` int NOT NULL,
  `position` int NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE IF NOT EXISTS `tbl_permission` (
  `permission_id` int NOT NULL AUTO_INCREMENT,
  `menu` char(50) NOT NULL,
  `type` varchar(15) NOT NULL,
  `feature` varchar(30) NOT NULL,
  `all` enum('0','1') NOT NULL,
  `read` enum('0','1') NOT NULL,
  `write` enum('0','1') NOT NULL,
  `edit` enum('0','1') NOT NULL,
  `delete` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`permission_id`, `menu`, `type`, `feature`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(1, 'site pages', 'page', 'contact', '0', '1', '0', '0', '1', '0'),
(2, 'site pages', 'page', 'aboutus', '1', '1', '1', '1', '1', '0'),
(3, 'site pages', 'page', 'banner', '1', '1', '1', '1', '1', '0'),
(4, 'site pages', 'page', 'gallery', '1', '1', '1', '1', '1', '0'),
(5, 'site pages', 'page', 'client', '1', '1', '1', '1', '1', '0'),
(6, 'site pages', 'page', 'terms', '1', '1', '1', '1', '1', '0'),
(7, 'site pages', 'page', 'policy', '1', '1', '1', '1', '1', '0'),
(8, 'site pages', 'page', 'review', '1', '1', '1', '1', '1', '0'),
(9, 'site pages', 'page', 'blog', '1', '1', '1', '1', '1', '0'),
(10, 'site pages', 'page', 'feedback', '0', '1', '0', '0', '1', '0'),
(11, 'site pages', 'page', 'team', '1', '1', '1', '1', '1', '0'),
(12, 'site pages', 'page', 'video', '1', '1', '1', '1', '1', '0'),
(13, 'site pages', 'page', 'inquiry', '0', '1', '0', '0', '1', '0'),
(14, 'site pages', 'page', 'seo', '1', '1', '1', '1', '1', '0'),
(15, 'site pages', 'page', 'category', '1', '1', '1', '1', '1', '0'),
(16, 'site pages', 'page', 'subcategory', '1', '1', '1', '1', '1', '0'),
(17, 'site pages', 'page', 'user', '1', '1', '1', '1', '1', '1'),
(18, 'site pages', 'page', 'role', '1', '1', '1', '1', '1', '0'),
(19, 'site pages', 'page', 'permission', '1', '1', '1', '1', '1', '0'),
(20, 'site pages', 'page', 'mainmenu', '1', '1', '1', '1', '1', '0'),
(21, 'site pages', 'page', 'submenu', '1', '1', '1', '1', '1', '0'),
(22, '', 'feature', 'general', '1', '1', '0', '1', '0', '0'),
(23, '', 'feature', 'social', '1', '1', '0', '1', '0', '0'),
(24, '', 'feature', 'email', '1', '1', '0', '1', '0', '0'),
(25, '', 'feature', 'security', '1', '1', '0', '1', '0', '0'),
(26, '', 'feature', 'system', '1', '1', '0', '1', '0', '0'),
(27, '', 'feature', 'captcha', '1', '1', '0', '1', '0', '0'),
(28, '', 'feature', 'theme', '1', '1', '0', '1', '0', '0'),
(29, '', 'feature', 'payment', '1', '1', '0', '1', '0', '0'),
(30, '', 'feature', 'language', '1', '1', '0', '1', '0', '0'),
(31, '', 'feature', 'backup', '1', '1', '0', '1', '0', '0'),
(39, 'faq', 'page', 'faq', '1', '1', '1', '1', '1', '1'),
(48, 'courses', 'page', 'courses', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_assign`
--

DROP TABLE IF EXISTS `tbl_permission_assign`;
CREATE TABLE IF NOT EXISTS `tbl_permission_assign` (
  `permission_assigned_id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NOT NULL,
  `permission_id` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `all` enum('1','0') NOT NULL,
  `read` enum('1','0') NOT NULL,
  `write` enum('1','0') NOT NULL,
  `edit` enum('1','0') NOT NULL,
  `delete` enum('1','0') NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`permission_assigned_id`),
  KEY `Property ID` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=609 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission_assign`
--

INSERT INTO `tbl_permission_assign` (`permission_assigned_id`, `role_id`, `permission_id`, `type`, `all`, `read`, `write`, `edit`, `delete`, `status`) VALUES
(600, 1, '[28,27,26,25,23,22]', 'feature', '1', '1', '1', '1', '1', '1'),
(601, 1, '[28,27,26,25,23,22]', 'feature', '0', '1', '0', '0', '0', '0'),
(602, 1, '[28,27,26,25,23,22]', 'feature', '0', '0', '0', '1', '0', '0'),
(603, 1, '[48,15,8,7,6,3,2]', 'page', '1', '1', '1', '1', '1', '1'),
(604, 1, '[48,8,7,6,3,2,1]', 'page', '0', '1', '0', '0', '0', '0'),
(605, 1, '[48,8,7,6,3,2]', 'page', '0', '0', '1', '0', '0', '0'),
(606, 1, '[48,8,7,6,3,2]', 'page', '0', '0', '0', '1', '0', '0'),
(607, 1, '[48,8,7,6,3,2]', 'page', '0', '0', '0', '0', '1', '0'),
(608, 1, '[48]', 'page', '0', '0', '0', '0', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_policy`
--

DROP TABLE IF EXISTS `tbl_policy`;
CREATE TABLE IF NOT EXISTS `tbl_policy` (
  `policy_id` int NOT NULL AUTO_INCREMENT,
  `policy` text NOT NULL,
  PRIMARY KEY (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_policy`
--

INSERT INTO `tbl_policy` (`policy_id`, `policy`) VALUES
(5, '<p>test</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `sku` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `standard_price` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `standard_qty` int NOT NULL,
  `short_description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `additional_info` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `photos` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tags` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `meta_title` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `meta_keyword` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `meta_desc` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `featured` int NOT NULL,
  `status` tinyint NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `category_id`, `sku`, `title`, `slug`, `standard_price`, `standard_qty`, `short_description`, `description`, `additional_info`, `photos`, `tags`, `meta_title`, `meta_keyword`, `meta_desc`, `featured`, `status`, `entry_date`, `modify_date`) VALUES
(14, 9, 'DXLX-11', 'Analog Watch - For Women DXLX-11', 'analog-watch-for-women-dxlx-11', '4500', 6, 'Apply to your face and rinse generously. Use as frequently as needed.\r\n\r\nUnlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.\r\nGive your skin the winning advantage!\r\n\r\nThe product is supplied in a re-usable and recyclable glass bottle with a pump dispenser.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.\r\n\r\nUse within 9 months of opening.  This will last between 2 and 3 months with daily use.\r\n\r\nAll prices include GST.\r\nProduct weight: 100g', 'Channel letters are digital numbers to show your brands, name and logo. It is provide powerful impact on your visual communication.', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">Apply to your face and rinse generously. Use as frequently as needed.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">Unlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.<br>Give your skin the winning advantage!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">The product is supplied in a re-usable and recyclable glass bottle with a pump dispenser.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">Use within 9 months of opening.  This will last between 2 and 3 months with daily use.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">All prices include GST.<br>Product weight: 100g</p>', './admin_asset/product_photos/c2ae605760f4ebec63ae11fa5d0c9638.jpeg', '', '', '', '', 1, 1, '2023-08-28', '2023-12-02'),
(15, 9, 'DF3455', 'Analog Watch - For Women Fresh Fashion Best In Market Black Girls Watch', 'analog-watch-for-women-fresh-fashion-best-in-market-black-girls-watch', '52000', 10, 'Apply to your face and rinse generously. Use as frequently as needed.\r\n\r\nUnlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.\r\nGive your skin the winning advantage!\r\n\r\nThe product is supplied in a re-usable and recyclable glass bottle with a pump dispenser.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.\r\n\r\nUse within 9 months of opening.  This will last between 2 and 3 months with daily use.\r\n\r\nAll prices include GST.\r\nProduct weight: 100g', '<span style=\"text-align: justify;\">Presenting The Ultimate &amp; Unique Watch From The House Of\" Holcano\" . This Watch Offers The Priceless Looks And Class You Always Wanted. This Watch Offers True Craftsmanship That Only People With A Distinct Taste Can Identify. This Watch Is Designed Using Technology And Innovation, All Of Which Is Wrapped Up In Eye-catchy Dials Encased In quality Case That Can Be Beautifully Placed On Your Wrist Using Its Comfortable And Luxuriously Finished Leather Strap/METAL/Rubber/Steel Belt or bracelets . Being Battery Powered, This Watch Offers Amazing Long Life And Durability That You Would Look For. We Are Sure That This Watch Is Going To Be Your Favorite For All Those Business Meetings, Events, Outings And Special Occasions Because It’s Not A Watch, It’s Your Id‘</span>', '<div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Display Type</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Analog</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Style Code</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Fresh Fashion Best In Market Black Girls Watch</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Occasion</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Casual, Party-Wedding, Formal</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Pack of</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">1</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Sales Package</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">1 Watvcj</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Mechanism</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Quartz</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Strap Color</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Silver</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Net Quantity</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">1</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">World Time</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">No</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Dual Time</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">No</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Strap Type</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Belt</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Strap Design</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Mesh</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">GPS</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">NO</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Clasp Type</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Buckle</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Dial Color</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">Pink</div></div><div class=\"row\" style=\"margin: 0px; padding: 0px; flex-direction: row; width: 749.662px;\"><div class=\"col col-3-12 _2H87wv\" style=\"margin: 0px; padding: 0px 0px 24px; width: 187.413px; display: inline-block; vertical-align: top;\">Domestic Warranty</div><div class=\"col col-9-12 _2vZqPX\" style=\"margin: 0px; padding: 0px 0px 24px; width: 562.237px; display: inline-block; vertical-align: top;\">6 Months</div></div>', './admin_asset/product_photos/82af98db5e23e4a13b69f888291a023e.png,./admin_asset/product_photos/b5c1369ff8e62b8b7fdd752d0b3aa753.png', '', '', '', '', 1, 1, '2023-08-28', '2023-12-02'),
(16, 7, 'LD-L144-BLU-CH', 'LD-L144-BLU-CH Silver Mesh Strap Girls Analog Watch - For Women LD-L144-BLU-CH', 'ld-l144-blu-ch-silver-mesh-strap-girls-analog-watch-for-women-ld-l144-blu-ch', '2500', 5, 'A small amount is enough to clean your skin from even the toughest makeup and it won’t leave you gasping for air with an overload of bubbles.\r\n\r\nIt glides on effortlessly and spreads beautifully to lift dirt and oil particles from your skin. It is gently fragranced with our Vanilla Rose blend of fragrance and essential oil to help your skin feel refreshed.', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">A small amount is enough to clean your skin from even the toughest makeup and it won’t leave you gasping for air with an overload of bubbles.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: \"Open Sans\", sans-serif; font-size: 15px;\">It glides on effortlessly and spreads beautifully to lift dirt and oil particles from your skin. It is gently fragranced with our Vanilla Rose blend of fragrance and essential oil to help your skin feel refreshed.</p>', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Apply to your face and rinse generously. Use as frequently as needed.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Unlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.<br>Give your skin the winning advantage!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">The product is supplied in a re-usable and recyclable glass bottle with a pump dispenser.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Use within 9 months of opening.  This will last between 2 and 3 months with daily use.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">All prices include GST.<br>Product weight: 100g</p>', './admin_asset/product_photos/9c24abc61b3c175e953c91c765d105ba.png', 'Absorbing, No-comedogenic, Occlusive, Oily Serum, Relaxing, Serum, Skincare', '', 'Absorbing, No-comedogenic, Occlusive, Oily Serum, Relaxing, Serum, Skincare', '', 1, 1, '2023-08-28', '2023-12-02'),
(18, 6, '4049-BL', '4049-BL Elegant Analog Watch - For Men 4049-BL', '4049-bl-elegant-analog-watch-for-men-4049-bl', '4700', 10, 'A good cleanser can be difficult to find. Some leave your skin feeling dry and sensitive while others foam so much that you feel like suffocating while trying to rinse off the suds!\r\nThe purpose of a cleanser is to lift dirt and oil particles from your skin which is then rinsed away with water.\r\n\r\nUnfortunately, not all manufacturers use quality ingredients and that could leave your skin paying the ultimate price for a buck saved.', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Our cleanser is a must try!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">A small amount is enough to clean your skin from even the toughest makeup and it won’t leave you gasping for air with an overload of bubbles.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">It glides on effortlessly and spreads beautifully to lift dirt and oil particles from your skin. It is gently fragranced with our Vanilla Rose blend of fragrance and essential oil to help your skin feel refreshed.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">We use the gentlest of surface actives (known as surfactants) and completely natural ingredients to give you the best results, time after time.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Cleansing should not be a chore – keep your cleanser in your shower for a quick and gentle wash.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Apply to your face and rinse generously. Use as frequently as needed.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Unlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.<br>Give your skin the winning advantage!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">The product is supplied in a re-usable and recyclable glass bottle with a pump dispenser.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Use within 9 months of opening.  This will last between 2 and 3 months with daily use.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">All prices include GST.<br>Product weight: 100g</p>', '<table class=\"woocommerce-product-attributes shop_attributes\" style=\"margin: 0px 0px 2.618em; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(228, 228, 228); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1190px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--weight\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">WEIGHT</th><td class=\"woocommerce-product-attributes-item__value\" data-o_content=\"0.185 kg\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\">0.120 kg</td></tr><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--dimensions\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">DIMENSIONS</th><td class=\"woocommerce-product-attributes-item__value\" data-o_content=\"4 × 4 × 14 cm\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\">6 × 3 × 14 cm</td></tr><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_order-type\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">ORDER TYPE</th><td class=\"woocommerce-product-attributes-item__value\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\">Standard option, Refill option</p></td></tr></tbody></table>', './admin_asset/product_photos/20a0d4f29bb0cdd91d8697127e3f39d9.png,./admin_asset/product_photos/1cb8b0626beadf4993d5e1f977ab725e.png,./admin_asset/product_photos/2c476dd9998f89c35077b29d8cb987d5.png,./admin_asset/product_photos/78283976097f42bc4e61edca2fa5cf5c.png', 'Cleanser, Face Wash, Gel Cleanser, Remove Makeup, Skincare, Surfactant', 'Gel Cleanser (Foaming)', 'Cleanser, Face Wash, Gel Cleanser, Remove Makeup, Skincare, Surfactant', 'A good cleanser can be difficult to find. Some leave your skin feeling dry and sensitive while others foam so much that you feel like suffocating while trying to rinse off the suds!', 1, 1, '2023-10-06', '2023-12-02'),
(19, 8, '4054-PK', 'Pink Day and Date Analog Watch - For Men 4054-PK', 'pink-day-and-date-analog-watch-for-men-4054-pk', '5100', 0, 'Hyaluronic Acid – By far our favourite ingredient! It holds between 600 – 1000 times its weight in water (Yep, you read that right)\r\n\r\nIt draws moisture from around it (the air) and keeps your skin hydrated the whole day! A gentle formulation designed to give your skin that “ahhh” feeling without adding weight or interfering with your makeup.\r\n\r\nApply before your daily moisturiser. Use as often as you want. A few drops spread into your palms can be pressed onto your skin. Don’t forget to spread the love to your neck and hands too!  Pro tip:  work the last little bits into your hair for instant hydration.\r\n\r\nWhy not get the best without compromise. A little kindness for the Planet, Your Skin & Your Wallet.', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">A small amount goes a long way, just a drop or two pressed onto your skin will leave your face looking radiant while plumping fine lines and wrinkles.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">It glides on effortlessly and spreads beautifully. Works well on hair too!  Users rave about the near instant results they have experienced on dry and damaged hair.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Using OceanDerMX ® LIFT & FIRM, made right here in New Zealand to keep working for your skin while you focus on life.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">This serum contains a powerful humectant, which combined with Rose Hydrosol and Hyaluronic Acid makes this serum the ultimate weapon in your arsenal.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Contrary to claims, Hyaluronic Acid molecules can’t actually penetrate your skin, they are simply too large.  What it is very good at doing is attracting moisture from your environment to your skin.  This means that your skin is continuously receiving moisture.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet. Unlike some commercial brands, we do not use cheap ingredients to create bulk, it’s all natural ingredients which you can trace back to its origins.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Apply a couple of drops to your palms and press onto your face and neck after using your toner.<br>Give your skin the winning advantage!</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">If you would like to see how it’s done, you can watch our tutorial here. It takes about 5 minutes to make.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">The product is supplied in a re-usable and recyclable glass bottle with a dropper.  Consider re-using your old packaging on your next order and you will be doing a little kindness for the planet AND your wallet.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">Use within 12 months of opening.  The product will last between 6 and 8 months with daily use.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline;\">All prices include GST.<br>Product weight:  100g</p>', '<table class=\"woocommerce-product-attributes shop_attributes\" style=\"margin: 0px 0px 2.618em; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-right-style: initial; border-bottom-style: initial; border-left-style: initial; border-top-color: rgb(228, 228, 228); border-right-color: initial; border-bottom-color: initial; border-left-color: initial; border-image: initial; outline: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-variant-alternates: inherit; font-variant-position: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; border-spacing: 0px; width: 1190px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--weight\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">WEIGHT</th><td class=\"woocommerce-product-attributes-item__value\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\">0.185 kg</td></tr><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--dimensions\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">DIMENSIONS</th><td class=\"woocommerce-product-attributes-item__value\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\">4 × 4 × 14 cm</td></tr><tr class=\"woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_order-type\" style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\"><th class=\"woocommerce-product-attributes-item__label\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-left: 0px; border-right-color: rgb(228, 228, 228); border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit; text-align: left; text-transform: uppercase;\">ORDER TYPE</th><td class=\"woocommerce-product-attributes-item__value\" style=\"margin: 0px; padding-right: 15px; padding-left: 15px; border-top: 0px; border-right: 0px; border-left: 0px; border-bottom-color: rgb(228, 228, 228); outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: top; font-family: inherit; font-style: inherit;\"><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-variant: inherit; font-stretch: inherit; line-height: inherit; font-optical-sizing: inherit; font-kerning: inherit; font-feature-settings: inherit; font-variation-settings: inherit; vertical-align: baseline; font-family: inherit; font-style: inherit; font-weight: inherit;\">Standard option, Refill Option</p></td></tr></tbody></table>', './admin_asset/product_photos/a77edbefb0bfdfa7a4dccc78146d4504.png', 'Hyaluronic Acid, Hydration, Moisturize, OceanDerMX ® LIFT & FIRM, Serum, Skincare, Water based', 'Hyaluronic Acid Serum', 'Hyaluronic Acid, Hydration, Moisturize, OceanDerMX ® LIFT & FIRM, Serum, Skincare, Water based', 'Hyaluronic Acid – By far our favourite ingredient! It holds between 600 – 1000 times its weight in water (Yep, you read that right)', 1, 1, '2023-10-06', '2023-12-02');
INSERT INTO `tbl_product` (`product_id`, `category_id`, `sku`, `title`, `slug`, `standard_price`, `standard_qty`, `short_description`, `description`, `additional_info`, `photos`, `tags`, `meta_title`, `meta_keyword`, `meta_desc`, `featured`, `status`, `entry_date`, `modify_date`) VALUES
(20, 9, 'JOK234', 'JOKER & WITCH', 'joker-witch', '25000', 0, 'Fastrack Play Plus Classic Nitro Fast Charging Auto Stress and Mood Monitoring with Black Colour Strap', '<div style=\"box-sizing: inherit;\"><h4 class=\"pdp-product-description-title\" style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;\">PRODUCT DETAILS&nbsp;<span class=\"myntraweb-sprite pdp-productDetailsIcon sprites-productDetailsIcon\" style=\"box-sizing: inherit; background-image: url(&quot;https://constant.myntassets.com/web/assets/img/MyntraWebSprite_27_01_2021.png&quot;); background-position: -231px -58px; background-size: 1404px 105px; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; display: inline-block; width: 21px; height: 22px; vertical-align: middle; margin-left: 5px;\"></span></h4><p class=\"pdp-product-description-content\" style=\"box-sizing: inherit; line-height: 1.4; margin-top: 12px; width: 491px;\">Display: Analogue<br style=\"box-sizing: inherit;\">Movement: Quartz<br style=\"box-sizing: inherit;\">Power source: Battery<br style=\"box-sizing: inherit;\">Dial style: Solid round stainless steel dial<br style=\"box-sizing: inherit;\">Features: Reset Time<br style=\"box-sizing: inherit;\">Strap style: Rose Gold-Toned regular, stainless steel strap with a foldover closure<br style=\"box-sizing: inherit;\">Water resistance: Non-Resistant<br style=\"box-sizing: inherit;\">Warranty: 1 year<br style=\"box-sizing: inherit;\">Warranty provided by brand/manufacturer<br style=\"box-sizing: inherit;\">Comes in a signature JOKER &amp; WITCH case</p></div><div class=\"pdp-sizeFitDesc\" style=\"box-sizing: inherit; border: none; margin-top: 12px;\"><h4 class=\"pdp-sizeFitDescTitle pdp-product-description-title\" style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border: none; padding-bottom: 5px;\">Size &amp; Fit</h4><p class=\"pdp-sizeFitDescContent pdp-product-description-content\" style=\"box-sizing: inherit; line-height: 1.4; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; width: 526.075px;\">Dial width: 38 mm<br style=\"box-sizing: inherit;\">Strap width: 12 mm<br style=\"box-sizing: inherit;\">Weight: 70 g</p></div><div class=\"pdp-sizeFitDesc\" style=\"box-sizing: inherit; border: none; margin-top: 12px;\"><h4 class=\"pdp-sizeFitDescTitle pdp-product-description-title\" style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border: none; padding-bottom: 5px;\">Material &amp; Care</h4><p class=\"pdp-sizeFitDescContent pdp-product-description-content\" style=\"box-sizing: inherit; line-height: 1.4; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; width: 526.075px;\">Mineral glass<br style=\"box-sizing: inherit;\">Keep Away From Moisture And Avoid Rough Usage</p></div><div class=\"index-sizeFitDesc\" style=\"box-sizing: inherit; border: none; margin-top: 12px;\"><h4 class=\"index-sizeFitDescTitle index-product-description-title\" style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-bottom: 12px; border: none;\">Specifications</h4><div class=\"index-tableContainer\" style=\"box-sizing: inherit; display: flex; -webkit-box-pack: start; justify-content: flex-start; flex-flow: wrap; -webkit-box-orient: horizontal; -webkit-box-direction: normal;\"><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 58.45px 12px 0px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Display</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Analogue</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 0px 12px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Features</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Reset Time</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 58.45px 12px 0px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Movement</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Quartz</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 0px 12px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Power Source</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Battery</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 58.45px 12px 0px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Type</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Regular</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 0px 12px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Dial Shape</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Round</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 58.45px 12px 0px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Make</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Non-Swiss Made</div></div><div class=\"index-row\" style=\"box-sizing: inherit; position: relative; border-bottom: 1px solid rgb(234, 234, 236); margin: 0px 0px 12px; padding-bottom: 10px; flex-basis: 40%;\"><div class=\"index-rowKey\" style=\"box-sizing: inherit; position: relative; line-height: 1; margin-bottom: 5px;\">Dial Pattern</div><div class=\"index-rowValue\" style=\"box-sizing: inherit; position: relative; line-height: 1.2;\">Solid</div></div></div></div>', '<div class=\"meta-info\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; position: relative;\"><div class=\"meta-desc\" style=\"box-sizing: inherit; margin: 5px 0px; display: inline-block; width: 526.075px; vertical-align: top;\">100% Original Products</div></div><div class=\"meta-info\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; position: relative;\"><div class=\"meta-desc\" style=\"box-sizing: inherit; margin: 5px 0px; display: inline-block; width: 526.075px; vertical-align: top;\">Pay on delivery might be available</div></div><div class=\"meta-info\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; position: relative;\"><div class=\"meta-desc\" style=\"box-sizing: inherit; margin: 5px 0px; display: inline-block; width: 526.075px; vertical-align: top;\">Easy 14 days returns and exchanges</div></div><div class=\"meta-info\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; position: relative;\"><div class=\"meta-desc\" style=\"box-sizing: inherit; margin: 5px 0px; display: inline-block; width: 526.075px; vertical-align: top;\">Try &amp; Buy might be available</div></div>', './admin_asset/product_photos/fd0b667275759f86c63a9494b7042a34.png,./admin_asset/product_photos/05d70cd95ff040517df1647489f04229.png', '', '', '', '', 1, 1, '2023-12-02', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_review`
--

DROP TABLE IF EXISTS `tbl_product_review`;
CREATE TABLE IF NOT EXISTS `tbl_product_review` (
  `product_review_id` int NOT NULL AUTO_INCREMENT,
  `register_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `rating` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `review` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  PRIMARY KEY (`product_review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_product_review`
--

INSERT INTO `tbl_product_review` (`product_review_id`, `register_id`, `product_id`, `rating`, `review`, `entry_date`) VALUES
(1, 1, 3, '3', 'this is demo rate and review', '2023-03-27'),
(2, 1, 3, '3', 'this is demo second rate and review', '2023-03-27'),
(3, 1, 1, '5', 'hello', '2023-03-27'),
(4, 1, 4, '5', 'deee', '2023-04-01'),
(5, 1, 3, '5', 'dsf sdf sd fds f', '2023-04-01'),
(6, 1, 3, '4', '', '2023-04-01'),
(7, 1, 3, '3', 'sfsd fsfdsf', '2023-04-01'),
(8, 1, 3, '5', 'sfsd fsfdsf sdfsf sdf', '2023-04-01'),
(9, 1, 3, '1', 'sfsd fsfdsf sdf sdf sdf sdf sd', '2023-04-01'),
(10, 1, 3, '2', 'revv', '2023-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register`
--

DROP TABLE IF EXISTS `tbl_register`;
CREATE TABLE IF NOT EXISTS `tbl_register` (
  `register_id` int NOT NULL AUTO_INCREMENT,
  `fname` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` char(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gender` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `country` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `state` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `postal_code` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `delete_status` int DEFAULT NULL,
  `payment_status` tinyint DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `modify_date` date DEFAULT NULL,
  PRIMARY KEY (`register_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_register`
--

INSERT INTO `tbl_register` (`register_id`, `fname`, `email`, `phone`, `gender`, `country`, `state`, `city`, `address`, `postal_code`, `path`, `password`, `status`, `delete_status`, `payment_status`, `register_date`, `modify_date`) VALUES
(1, 'nishant j thummar', 'nishant@gmail.com', '8460124262', 'male', 'india', 'gujarat', 'surat', '170 shubham row house', '395006', './admin_asset/customer/4f2d8225f76dbf5a59aabbc4911833ea.png', '7eafa30109230865c2bbf7139d086a9879ff2668230c679cfaa1eb0a4f95b6420509d0a06fc64767cdfc6a950dfd1ce951666bd1d388fd3ddde04df59222d2cbHm9HETnADcFlumuWTBTrSP/NOgC1b3fR64+N5kkcGwE=', NULL, 0, 0, '2022-12-16', NULL),
(2, 'komal ukani', 'komal@gmail.com', '9925400512', '', '', '0', NULL, NULL, '', '', '4d6e8b769f205da8c0983f9bbba9fc9696dfd7185bf36a17d2e052775ddd3bbc353c106e3fd038272458c3f3458a8540ded40d5568c2742bc0b2a09cc0f6d10fv8kSJoYbygoQr+otDfRDvlZjA5P2JPisHhhnz3rTVcM=', NULL, 0, 0, '2022-12-17', NULL),
(4, 'Nisha Patel', 'nisha@gmail.com', '84601242633', 'female', '', '12', '1041', '45 avenue park near new coloni', '395006', '', '36dbcecedc19b51772406053dc7ff246732f11f5a20237977286c99e5b0931dcee8997d7982752baa78be59460e97220327005a5d6da5133136268d445ec3a78HE8ixBP8J9un9GquerppaodVyUUJT0PTSNuZzToJdIc=', 1, 0, 0, '2023-01-27', NULL),
(8, 'Jiten Chauhan', 'jiten@gmail.com', '8460124263', 'male', '', '15', '1285', 'demo ', '789798', '', '6bd3d48c27bb35f0d795edd5a7a27390f3d5ef2d1145aea33d8d83c1a56bf1a664567c789cd9fe3c344e4f02d7f12cf8198badabf6f90d7089bd2e9dec8fdc9a0I7NFo/v9NsNUF6w0QubPQTqlBNyVVYxQDyLE1BALGk=', 1, 0, 0, '2023-01-29', NULL),
(9, 'Ronak Sir', 'ronak@gmail.com', '9925544000', 'male', '', '31', '3119', 'dsf', '345345', '', 'bb7e5d53fb39fa5d4c98fb680b5bc26d27b551c0b87da1542ec68e75094e43fef97bb74b23553b7fcead05f65c386870debc04c9e3f4d4ecdbd581e5d40cc043b5WlckAGnMtXImMENfrbcKjXHdghVcZzJaQI9oECJio=', 1, 0, 0, '2023-01-30', NULL),
(12, 'vira patel', 'vira@gmail.com', '789456130111', 'female', '', NULL, NULL, '170 shubham row house', '789456', './admin_asset/customer/b8a03b2ffec1a2ac986976bf7eede281.png', 'd551707827a817adb285e30185a49318fdd2484b04537d59e69728ebb37446da5f02fc1d0dd92a0f093d03f6835ede641d72553103a5979be2fcd7b50a237758f3Oo36vZKLGY+urZD/kg3GbJmQz9npmbQdVc0H1pkf0=', 1, NULL, NULL, '2023-02-16', NULL),
(13, 'komal', 'komal123@gmail.com', '784561300000', NULL, '', NULL, NULL, NULL, NULL, NULL, '998f3f81a7955afd87a0d0c471e65b4e744c49ad434be85049309f4e680aaee6fa526555d8638a85b6d7749cb41968c7fc85b630a3941dc373a15f4e9f4fe33dJSC2ZwAAmhD4bP3L+OuC+nLMhzl1jUHxBb9tytnSasE=', 1, NULL, NULL, '2023-04-14', NULL),
(14, 'karan kikani', 'karan@gmail.com', '7485125263', 'male', 'nz', 'wall', 'wall', 'wall', '4652', './admin_asset/customer/5092498f2d0a40c6726acdd77452b93f.png', '0864bc47f866c80536f1d711e06c7d7b932e322defd3f341fab0e4606090d89c504b19135c90dfef5356f87be9161e811a94c4fcc182a8d8e28d5984f5c12f6dno4m093LFdX0PboyjXhW45XLLqil2BISkkWe2Gzav+M=', 1, NULL, NULL, '2023-10-04', NULL),
(15, 'hira thakur', 'hira@gmail.com', '6541324456', NULL, '', NULL, NULL, NULL, NULL, NULL, '8c733e47b0e151d0ab23dd31c6f36f7b9c58d6f3b773d728a5ff9b568e83f5e3af21aa0db3af3d9dd4b1e036ae7ad10b30cad1c81c998eaff6ae5889526833aefhCGo8uoX2qj/CnXZL8mvCLGIef1HQGYgYe8URjSIFw=', 1, NULL, NULL, '2023-10-30', NULL),
(16, 'virat kohli', 'virat@gmail.com', '7894654560', NULL, '', NULL, NULL, NULL, NULL, NULL, '868ee20aaf113d61e9db11ab15373ca427c270962bf55b44904747704cda7b698b5803e93066fe14b5a5b4f943af3fc6456b64df9351aeccda41386dd569c4f6Za6DXpLl1Mvjy0skYRkNcxbu6f04Aw4ugG8MiL6TM40=', 1, NULL, NULL, '2023-12-04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

DROP TABLE IF EXISTS `tbl_review`;
CREATE TABLE IF NOT EXISTS `tbl_review` (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `review` varchar(2000) NOT NULL,
  `datetime` datetime NOT NULL,
  `path` varchar(100) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_review`
--

INSERT INTO `tbl_review` (`review_id`, `username`, `review`, `datetime`, `path`) VALUES
(18, 'John William', 'With a deep understanding of our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2022-10-06 00:00:00', './admin_asset/review/32ea0dacee64f1c86f829b5230cb9394.jpg'),
(19, 'Mayur Patel', 'Dhillon Printing take care of people because they care! They pay attention to the little things and provide outstanding employee care throughout their journey from the day an offer is made.', '2023-06-28 00:00:00', './admin_asset/review/f40e1630b8794e16238c1b045461b210.jpg'),
(20, 'Herry Tom', 'Dhillon Printing did an incredible job in creating our new marketing & sales pieces. Their prices are very competitive, and their quality is excellent. Thank you for the excellent service on the brochure reprints and the new display posters.', '2023-08-24 00:00:00', './admin_asset/review/6fd690e7baa1584679c03b1c44e1caab.jpg'),
(21, 'John Carter', 'Our client’s business needs areas, extensive resources, and a client people focused approach, Collabera has developed specialized recruiting teams who provide resources in various areas.', '2023-10-02 00:00:00', './admin_asset/review/bb1d941f11e058c10812d53b5291e4d8.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `title`, `remark`) VALUES
(1, 'admin', 'admin role');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seo`
--

DROP TABLE IF EXISTS `tbl_seo`;
CREATE TABLE IF NOT EXISTS `tbl_seo` (
  `seo_id` int NOT NULL AUTO_INCREMENT,
  `page` varchar(500) NOT NULL,
  `title` varchar(2000) NOT NULL,
  `description` text NOT NULL,
  `keyword` text NOT NULL,
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_seo`
--

INSERT INTO `tbl_seo` (`seo_id`, `page`, `title`, `description`, `keyword`) VALUES
(33, 'index', 'Home - Skin2Love', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcategory`
--

DROP TABLE IF EXISTS `tbl_subcategory`;
CREATE TABLE IF NOT EXISTS `tbl_subcategory` (
  `subcategory_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `path` varchar(500) NOT NULL,
  `parent` int NOT NULL,
  PRIMARY KEY (`subcategory_id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

DROP TABLE IF EXISTS `tbl_submenu`;
CREATE TABLE IF NOT EXISTS `tbl_submenu` (
  `submenu_id` int NOT NULL AUTO_INCREMENT,
  `mainmenu_id` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `position` int NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`submenu_id`),
  KEY `Main Menu ID` (`mainmenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team`
--

DROP TABLE IF EXISTS `tbl_team`;
CREATE TABLE IF NOT EXISTS `tbl_team` (
  `team_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `path` varchar(1000) NOT NULL,
  `facebook` varchar(1000) NOT NULL,
  `instagram` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `phone` varchar(1000) NOT NULL,
  `linkedin` varchar(1000) NOT NULL,
  `twitter` text NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_team`
--

INSERT INTO `tbl_team` (`team_id`, `name`, `path`, `facebook`, `instagram`, `email`, `phone`, `linkedin`, `twitter`) VALUES
(9, 'Demo User', './admin_asset/team/fb1973a4c4376ac6528f6eb0d51ef998.png', 'https://www.fb.com', 'https://www.instagram.com', 'demo@mail.com', '7894561230', 'https://www.linkedin.com', 'https://www.twitter.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_terms`
--

DROP TABLE IF EXISTS `tbl_terms`;
CREATE TABLE IF NOT EXISTS `tbl_terms` (
  `terms_id` int NOT NULL AUTO_INCREMENT,
  `terms` text NOT NULL,
  PRIMARY KEY (`terms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_terms`
--

INSERT INTO `tbl_terms` (`terms_id`, `terms`) VALUES
(6, '<p>test</p><p><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timezone`
--

DROP TABLE IF EXISTS `tbl_timezone`;
CREATE TABLE IF NOT EXISTS `tbl_timezone` (
  `timezone_id` int NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  PRIMARY KEY (`timezone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timezone`
--

INSERT INTO `tbl_timezone` (`timezone_id`, `identifier`, `timezone`) VALUES
(1, 'Africa', 'Africa/Abidjan'),
(2, 'Africa', 'Africa/Accra'),
(3, 'Asia', 'Asia/Aden'),
(4, 'Asia', 'Asia/Dhaka'),
(5, 'Asia', 'Asia/Almaty'),
(6, 'Asia', 'Asia/Karachi'),
(7, 'Asia', 'Asia/Kathmandu'),
(8, 'Asia', 'Asia/Kolkata'),
(9, 'Asia', 'Asia/Shanghai'),
(10, 'Asia', 'Asia/Tokyo'),
(11, 'Asia', 'Asia/Bangkok');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

DROP TABLE IF EXISTS `tbl_transaction`;
CREATE TABLE IF NOT EXISTS `tbl_transaction` (
  `transaction_id` int NOT NULL AUTO_INCREMENT,
  `bill_id` int NOT NULL,
  `product_id` int NOT NULL,
  `category_id` int NOT NULL,
  `price` double NOT NULL,
  `qty` int NOT NULL,
  `netprice` double NOT NULL,
  `currency_code` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `country_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_type` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `unique_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `register_id` int NOT NULL,
  `entry_date` date NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `Bill ID` (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`transaction_id`, `bill_id`, `product_id`, `category_id`, `price`, `qty`, `netprice`, `currency_code`, `country_name`, `entry_type`, `unique_id`, `register_id`, `entry_date`) VALUES
(1, 1, 16, 0, 2500, 1, 2500, '', '', '', '', 1, '2023-12-05'),
(2, 2, 14, 0, 4500, 1, 4500, '', '', '', '', 1, '2023-12-05'),
(3, 3, 18, 0, 4700, 1, 4700, '', '', '', '', 1, '2023-12-05'),
(4, 4, 19, 0, 5100, 1, 5100, '', '', '', '', 1, '2023-12-05'),
(5, 5, 16, 0, 2500, 1, 2500, '', '', '', '', 1, '2023-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_video`
--

DROP TABLE IF EXISTS `tbl_video`;
CREATE TABLE IF NOT EXISTS `tbl_video` (
  `video_id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `title` varchar(150) NOT NULL,
  `video` varchar(500) NOT NULL,
  `status` tinyint NOT NULL,
  `entry_date` date NOT NULL,
  `modify_date` date NOT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_video`
--

INSERT INTO `tbl_video` (`video_id`, `brand_id`, `title`, `video`, `status`, `entry_date`, `modify_date`) VALUES
(11, 1, 'sdfsd', 'fsdfdsf', 1, '2023-08-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_web_data`
--

DROP TABLE IF EXISTS `tbl_web_data`;
CREATE TABLE IF NOT EXISTS `tbl_web_data` (
  `web_data_id` int NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `email_address` varchar(500) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `whatsapp` varchar(50) NOT NULL,
  `office` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(1000) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `favicon` varchar(300) NOT NULL,
  `footer` varchar(500) NOT NULL,
  `mail_smtp_host` varchar(200) NOT NULL,
  `mail_protocol` varchar(100) NOT NULL,
  `mail_port` int NOT NULL,
  `mail_email` varchar(100) NOT NULL,
  `mail_password` varchar(50) NOT NULL,
  `forgot_mail_subject` varchar(200) NOT NULL,
  `forgot_mail_message` varchar(2000) NOT NULL,
  `welcome_mail_subject` varchar(200) NOT NULL,
  `welcome_mail_message` varchar(2000) NOT NULL,
  `inquiry_mail_subject` varchar(200) NOT NULL,
  `inquiry_mail_message` varchar(2000) NOT NULL,
  `active_mail_subject` varchar(200) NOT NULL,
  `active_mail_message` varchar(2000) NOT NULL,
  `inactive_mail_subject` varchar(200) NOT NULL,
  `inactive_mail_message` varchar(2000) NOT NULL,
  `captcha_site_key` varchar(100) NOT NULL,
  `captcha_secret_key` varchar(100) NOT NULL,
  `captcha_visibility` int NOT NULL,
  `date_format` varchar(50) NOT NULL,
  `time_format` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `meta_keyword` varchar(200) NOT NULL,
  `meta_desc` varchar(200) NOT NULL,
  PRIMARY KEY (`web_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_web_data`
--

INSERT INTO `tbl_web_data` (`web_data_id`, `project_name`, `email_address`, `phone`, `facebook`, `instagram`, `twitter`, `linkedin`, `youtube`, `whatsapp`, `office`, `address`, `map`, `logo`, `favicon`, `footer`, `mail_smtp_host`, `mail_protocol`, `mail_port`, `mail_email`, `mail_password`, `forgot_mail_subject`, `forgot_mail_message`, `welcome_mail_subject`, `welcome_mail_message`, `inquiry_mail_subject`, `inquiry_mail_message`, `active_mail_subject`, `active_mail_message`, `inactive_mail_subject`, `inactive_mail_message`, `captcha_site_key`, `captcha_secret_key`, `captcha_visibility`, `date_format`, `time_format`, `timezone`, `meta_keyword`, `meta_desc`) VALUES
(1, 'Bolo Punjabi', 'support@skin-2-love.com', '+64 (0) 22 079 8832', 'https://www.facebook.com/Skin2LoveS2L/', 'https://www.instagram.com/skin_2_love/', '#', '#', 'https://www.youtube.com/@skin-2-love89', '', '', 'North Shore, Auckland, New Zealand', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d102300.24954657751!2d174.64018393781427!3d-36.749384192146636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d37836a409301%3A0xf1e988239d183f11!2sNorth%20Shore%2C%20Hauraki%2C%20Auckland%2C%20New%20Zealand!5e0!3m2!1sen!2sin!4v1696394502098!5m2!1sen!2sin\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>', './admin_asset/logo/ac062cd12bbfe67c8339eda18821ae6c.png', './admin_asset/favicon/26a994b8b29a1695a777e465f45fe345.png', '©2024 BoloPunjabi. All Rights Reserved.', 'smtp', 'ssmtp', 465, 'demo@gmail.com', 'demo123', 'Recover your password - Enzo Admin', '<p>Hello user,</p><p>Click on below link to reset your current password.</p><p><br></p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Register Success - Welcome to Enzo Admin ', '<p>Hello user,</p><p>Firstly Thank you for register with us. You have successfully created account in our admin. You can access all the granted functionality in your account. If you want any further help regarding account, drop your issues on admin mail</p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Inquiry Submitted - Enzo Admin ', '<p>Hello user,</p><p>We have received your inquiry and we look into it very soon.Â </p><p><br></p><p>Thanks & Regards.</p><p><b><i>Enzo Admin.</i></b></p>', 'Profile Activated Successfully - Enzo Admin', '<p>Hello user,</p><p>Congratulations, Your Enzo admin <b>profile is now activated successfully</b>. You are eligible to access our fully functional admin panel.</p><p><br></p><p>Thanks & Regards</p><p><b><i>Enzo Admin.</i></b></p>', 'Your Profile Inactivated - Enzo Admin', '<p>Hello user,</p><p>Your Enzo admin&nbsp;<span style=\"font-weight: bolder;\">profile is now inactivated due to some reason</span>. You are not eligible to access our fully functional admin panel. For more information contact to our admin or team.</p><p><br></p><p>Thanks &amp; Regards</p><p><span style=\"font-weight: bolder;\"><i>Enzo Admin.</i></span></p>', '6LfksB8oAAAAADIqBWS9VBInmh4lmGL73RnLyqQu', '6LfksB8oAAAAAMwwau7hBdvdfsPMo4buMoy3bt8E', 1, 'd-M-Y', 'h', '8', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ', 'Skin-2-Love was born out of a thirst for knowledge and a desire to act against pollution and waste.  As many of you have done, I wanted to know how skincare was made. ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlist`
--

DROP TABLE IF EXISTS `tbl_wishlist`;
CREATE TABLE IF NOT EXISTS `tbl_wishlist` (
  `wishlist_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `unique_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`wishlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tbl_wishlist`
--

INSERT INTO `tbl_wishlist` (`wishlist_id`, `product_id`, `unique_id`, `email`) VALUES
(14, 19, '', 'hira@gmail.com'),
(18, 15, '', 'nishant@gmail.com'),
(27, 16, '', 'virat@gmail.com'),
(34, 14, '', 'virat@gmail.com'),
(39, 19, '', 'virat@gmail.com'),
(40, 20, '', 'virat@gmail.com'),
(41, 19, '', 'nishant@gmail.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_subcategory`
--
ALTER TABLE `tbl_subcategory`
  ADD CONSTRAINT `maincategory` FOREIGN KEY (`parent`) REFERENCES `tbl_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD CONSTRAINT `Main Menu ID` FOREIGN KEY (`mainmenu_id`) REFERENCES `tbl_mainmenu` (`mainmenu_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD CONSTRAINT `Bill ID` FOREIGN KEY (`bill_id`) REFERENCES `tbl_bill` (`bill_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
