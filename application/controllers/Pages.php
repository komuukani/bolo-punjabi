<?php
/* * *
 * Project:    Dhillon Printing
 * Name:       Admin Side Pages
 * Package:    Pages.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2023
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    /*
      P A G E S
     */

    // Get ID Address or Unique ID of user
    public function page_not_found()
    {
        $data = $this->setParameter(__FUNCTION__, "Page Not Found", false);
        $this->load->view('master', $data);
    }

    // Page not found 404
    protected function setParameter($page, $title, $page_breadcumb)
    {
        $location = $this->getLocationInfo();
        // generate unique_id
        if (!$this->input->cookie('unique_id')) {
            $this->load->helper('cookie');
            $uid = $this->input->cookie('ci_session');
            set_cookie('unique_id', $uid, 60 * 60 * 24 * 365);
        }
        $data = [];
        $data['page_title'] = $title;
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = $this->website;
        $data['location'] = $location;
        $data['developer'] = $this->developer;
        $data['web_link'] = $this->web_link;
        $data['page_breadcumb'] = $page_breadcumb;
        $data['page'] = $page;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Get Visitor IP Address
    public function getLocationInfo()
    {
        // Store the IP address
        $vis_ip = $this->getVisIPAddr();
        // Use JSON encoded string and converts it into a PHP variable
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $vis_ip));
        $data = array("country" => $ipdat->geoplugin_countryName, "city" => $ipdat->geoplugin_city, "continent" => $ipdat->geoplugin_continentName, "latitude" => $ipdat->geoplugin_latitude, "longitude" => $ipdat->geoplugin_longitude, "currency" => $ipdat->geoplugin_currencySymbol, "currency_code" => $ipdat->geoplugin_currencyCode, "timezone" => $ipdat->geoplugin_timezone);

        return $data;
    }

    // Get Country Name from  IP of Visitor
    public function getVisIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    // Home Page
    public function index()
    {
        $data = $this->setParameter(__FUNCTION__, "Home", false);
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    $insert_data['name'] = $this->input->post('fname');
                    $insert_data['email'] = $this->input->post('email');
                    $insert_data['phone'] = $this->input->post('phone');
                    $insert_data['message'] = $this->input->post('message');
                    $insert_data['datetime'] = date('Y-m-d H:i:s');
                    if ($this->md->insert($this->table_prefix . 'contact', $insert_data)) {
                        $data['success'] = 'Contact Submitted successfully.';
                    } else {
                        $data['error'] = 'Sorry, Contact not submitted!';
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    // Contact Page
    public function contact()
    {
        $data = $this->setParameter(__FUNCTION__, "Get in Touch", true);
        if ($this->input->post('send')) {
            $this->form_validation->set_rules('fname', 'First Name', 'required|regex_match[/^[a-zA-Z ]+$/]', array("required" => "Enter Full Name!"));
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email', array("required" => "Enter Email Address!"));
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|numeric|min_length[8]|max_length[14]', array("required" => "Enter Phone Number!"));
            $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
            if ($this->form_validation->run() == TRUE) {
                $captchaStatus = $this->md->verify_recaptcha($this->input->post('g-recaptcha-response')); // Verify Google recaptcha
                if ($captchaStatus) {
                    if ($this->input->post('extra_field')) {
                        $data['error'] = 'Sorry, You are a bot!';
                    } else {
                        $insert_data['name'] = $this->input->post('fname');
                        $insert_data['email'] = $this->input->post('email');
                        $insert_data['phone'] = $this->input->post('phone');
                        $insert_data['message'] = $this->input->post('message');
                        $insert_data['datetime'] = date('Y-m-d H:i:s');
                        if ($this->md->insert($this->table_prefix . __FUNCTION__, $insert_data)) {
                            $data['success'] = 'Contact Submitted successfully.';
                        } else {
                            $data['error'] = 'Sorry, Contact not submitted!';
                        }
                    }
                } else {
                    $data['error'] = 'Oops, Validate google reCaptcha!';
                }
            }
        }
        $this->load->view('master', $data);
    }

    // Aboutus Page
    public function aboutus()
    {
        $data = $this->setParameter(__FUNCTION__, "About us", true);
        $this->load->view('master', $data);
    }

    // Privacy policy Page
    public function policy()
    {
        $data = $this->setParameter(__FUNCTION__, "Privacy & Policy", true);
        $this->load->view('master', $data);
    }

    // Terms & Condition Page
    public function terms()
    {
        $data = $this->setParameter(__FUNCTION__, "Terms & Condition", true);
        $this->load->view('master', $data);
    }

    // FAQ
    public function faq()
    {
        $data = $this->setParameter(__FUNCTION__, "Frequently Asked Questions", true);
        $this->load->view('master', $data);
    }

    // Services
    public function services()
    {
        $data = $this->setParameter(__FUNCTION__, "Our Services", true);
        $this->load->view('master', $data);
    }

    // Courses
    public function courses()
    {
        $slug = $this->uri->segment(2) ? $this->uri->segment(2) : ''; // Blog Slug
        $pro = '';
        if ($slug) :
            $pro = $this->md->select_where('tbl_courses', array('courses_id' => $slug));
            if (empty($pro)) :
                redirect('404');
            endif;
        endif;
        $data = $this->setParameter(($pro ? 'courses_detail' : __FUNCTION__), ($pro ? ucfirst($pro[0]->title) : "Courses"), true);
        (!empty($pro)) && ($data['courses'] = $pro[0]);
        $this->load->view('master', $data);
    }

    // Search Course
    public function searchCourses()
    {
        $slug = $this->uri->segment(2) ? $this->uri->segment(2) : ''; // Have levels (Basic/ Intermediate/ Advance)
        $pro = '';
        if ($slug) :
            $pro = $this->md->select_where('tbl_courses', array('levels' => $slug));
        endif;
        $data = $this->setParameter('courses', (ucfirst($slug) . ' Courses'), true);
        $data['courses'] = $pro ? $pro : [];
        $this->load->view('master', $data);
    }

    // Search Level
    public function levels()
    {
        $data = $this->setParameter(__FUNCTION__, "Levels", true);
        $this->load->view('master', $data);
    }

    // Newsletter Page
    public function newsletter()
    {
        if ($this->input->post('newsletter')) {
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[tbl_email_subscriber.email]', array("required" => "Enter Email Address!"));
            if ($this->form_validation->run() == TRUE) {
                $insert_data['email'] = $this->input->post('email');
                $insert_data['datetime'] = date('Y-m-d H:i:s');
                if ($this->md->insert($this->table_prefix . 'email_subscriber', $insert_data)) {
                    $this->session->set_flashdata('newsletter', 'Email Registered successfully.');
                    redirect($_SERVER['HTTP_REFERER'], 'refresh');
                } else {
                    $data['error'] = 'Sorry, Email not registered!';
                }
            }
        }
    }
}
