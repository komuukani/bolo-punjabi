<?php
/* * *
 * Project:    Enzo Admin Pro
 * Name:       Admin Side Pages
 * Package:    Pages.php
 * About:      A controller that handle backend of all pages
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    // Global Variables
    public $website, $developer, $web_link, $table_prefix, $web_data, $admin_data, $savepath = './admin_asset/';

    // Constructor
    public function __construct()
    {
        parent::__construct();
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
        // Check if database already connected or not [STATUS = PRE/POST] 
        $settings = INSTALLER_SETTING;
        if ($settings['status'] == "pre") {
            redirect('/install');
        }
        $this->web_data = $this->md->select('tbl_web_data');    // SELECT WEBSITE DATA
        if ($this->session->userdata('aemail') == USERNAME) {
            $this->admin_data = array((object)USERDATA);
        } else {
            $this->admin_data = $this->md->select_where('tbl_admin', array('email' => $this->session->userdata('aemail'))); // SELECT ADMIN DATA
        }
        date_default_timezone_set(($this->web_data) ? $this->md->getItemName('tbl_timezone', 'timezone_id', 'timezone', $this->web_data[0]->timezone) : DEFAULT_TIMEZONE);
    }

    // Check login security
    public function security()
    {
        if ($this->session->userdata('aemail') == "")
            redirect('authorize');
    }

    // Common Function for setting up global page variable and send it to page.
    protected function setParameter($path, $page, $title, $page_breadcumb, $breadcrumb_icon, $datatables)
    {
        $this->security();
        $control_data = $this->md->select_where($this->table_prefix . 'control', array('control_id' => 1));
        if ($this->db->field_exists($page, $this->table_prefix . 'control') && !$control_data[0]->$page)
            redirect('authorize');
        $data = [];
        $data['page_title'] = ucwords($title);  // set page title
        $data['web_data'] = $this->web_data;    // get all Website data
        $data['admin_data'] = $this->admin_data;    // get all Admin data
        $data['website_title'] = ($this->web_data) ? $this->web_data[0]->project_name : 'Admin Panel'; // get project name from admin table
        $data['developer'] = $this->developer;  // get developer name
        $data['web_link'] = $this->web_link;    // get developer website link
        $data['page'] = $path . '/' . $page;   //  open page from admin folder
        $data['active_page'] = $page;   // get active page name
        $data['current_page'] = $this->uri->segment(1);     // get current page
        $data['page_type'] = ($this->uri->segment(2) ? $this->uri->segment(2) : '');  // Add/Edit/Show
        $data['header'] = true;
        $data['sidebar'] = true;
        $data['breadcrumb'] = $page_breadcumb;
        $data['breadcrumb_icon'] = $breadcrumb_icon;
        $data['controller'] = $this;
        $data['datatables'] = $datatables;
        if ($this->db->table_exists($this->table_prefix . $page))
            $data[$page] = $this->md->select($this->table_prefix . $page);
        return $data;
    }

    // Fetch Data from model, than it will return data to the function
    public function fetchData($class, $path)
    {
        $this->load->model('Admin/' . $path . '/' . ucfirst($class), $class);
        $postData = $this->input->post();  // get data from ajax call // POST data
        if ($postData):
            $data = $this->$class->getData($postData); // Get data
        endif;
        return json_encode($data);
    }

    // Check User has permission to access page
    public function check_permission($page)
    {
        $all = $read = $write = $edit = $delete = $status = false;
        if ($this->admin_data):
            if ($this->admin_data[0]->admin_id != 1):
                $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $this->admin_data[0]->user_role_id, 'type' => 'page'));   // get current role's permisison
                $per_id = array();
                foreach ($admin_menu as $ext_per) {
                    $per = json_decode($ext_per->permission_id);
                    foreach ($per as $per_val):
                        $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                        if ($exist_prmsn):
                            if ($exist_prmsn[0]->feature == $page):
                                if ($ext_per->all):
                                    $all = true;
                                endif;
                                if ($ext_per->read):
                                    $read = true;
                                endif;
                                if ($ext_per->write):
                                    $write = true;
                                endif;
                                if ($ext_per->edit):
                                    $edit = true;
                                endif;
                                if ($ext_per->delete):
                                    $delete = true;
                                endif;
                                if ($ext_per->status):
                                    $status = true;
                                endif;
                            endif;
                        endif;
                    endforeach;
                }
                $grant_permission = array('all' => $all, 'read' => $read, 'write' => $write, 'edit' => $edit, 'delete' => $delete, 'status' => $status); // other role
            else:
                $grant_permission = array('all' => true, 'read' => true, 'write' => true, 'edit' => true, 'delete' => true, 'status' => true);   // super admin
            endif;
        endif;
        return $grant_permission;
    }

    // Update status to active or inactive in common function
    public function statusUpdate()
    {
        if (web_status()):
            $table = $this->input->post('tbl'); // table name
            $id = $this->input->post('id'); // item id
            $type = $this->input->post('type'); // active/inactive
            $mail_status = $this->input->post('mail'); // if status is true then mail send, if false then no action.

            $web_data = $this->web_data;    //  get all website data for email sending
            $protocol = $web_data[0]->mail_protocol;    // Protocol
            $host = $web_data[0]->mail_smtp_host;   // Host Email Service Provider
            $sender = $web_data[0]->mail_email;     // Sender Email
            $password = $web_data[0]->mail_password;    // Sender Password
            $port = $web_data[0]->mail_port;    // PORT

            $this->md->update($this->table_prefix . $table, array('status' => ($type == "active") ? 1 : 0), array($table . '_id' => $id));
            // if ($mail_status):
            //     $item_data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id));
            //     if (!empty($item_data)):
            //         if ($type == 'active'):
            //             $subject = $web_data[0]->active_mail_subject;   // active mail subject from website data table
            //             $message = $web_data[0]->active_mail_message;    // active mail message from website data table
            //         else:
            //             $subject = $web_data[0]->inactive_mail_subject;  // inactive mail subject from website data table
            //             $message = $web_data[0]->inactive_mail_message;  // inactive mail message from website data table
            //         endif;
            //         $web_data && $this->md->sendMail($protocol, $host, $sender, $password, $port, $subject, $message, $item_data[0]->email); // Send mail
            //     endif;
            // endif;
            echo "true";
        else:
            echo "false";
        endif;
    }

    /*
      S I T E  -  P A G E S
     */

    // Banner - Sliders
    public function banner()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                if (web_status()):
                    /* $this->form_validation->set_rules('title', 'Set Banner Title', 'required', array("required" => "Enter Banner Title!"));
                      $this->form_validation->set_rules('subtitle', 'Set Banner Sub Title', 'required', array("required" => "Enter Banner Sub Title!"));
                      if ($this->form_validation->run() == TRUE) { */
                    $dt['title'] = $this->input->post('title');
                    $dt['position'] = $this->input->post('position');
                    $dt['subtitle'] = $this->input->post('subtitle');
                    (!empty($_FILES['banner']['name']) && $dt['path'] = $this->md->uploadFile('banner')); // check if file is selected than only it will upload
                    $this->md->insert($this->table_prefix . $class, $dt);
                    $this->session->set_flashdata('success', 'Yeah, Banner added successfully!');
                    redirect($data['current_page'] . "/" . $data['page_type']);
                endif;
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    // Check already photo is exist or not, if yes than it will remove and than upload.
                    $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                    $dt['title'] = $this->input->post('title');
                    $dt['position'] = $this->input->post('position');
                    $dt['subtitle'] = $this->input->post('subtitle');
                    $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('banner'));    // Upload Photo and return path from model
                    if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                        redirect('manage-' . $class . "/show");
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Blog
    public function blog()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-clipboard font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Blog Title', 'required', array("required" => "Enter Blog Title!"));
                $this->form_validation->set_rules('about', 'Set Blog Description', 'required', array("required" => "Enter Blog Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug') ? $this->input->post('slug') : $this->md->generateSeoURL($this->input->post('title'));
                        (!empty($_FILES['blog']['name']) && $dt['path'] = $this->md->uploadFile('blog')); // check if file is selected than only it will upload
                        $dt['description'] = $this->input->post('about');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['blogdate'] = date('Y-m-d H:i:s');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Blog Title', 'required', array("required" => "Enter Blog Title!"));
                $this->form_validation->set_rules('about', 'Set Blog Description', 'required', array("required" => "Enter Blog Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug') ? $this->input->post('slug') : $this->md->generateSeoURL($this->input->post('title'));
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('blog'));    // Upload photo and return path from model
                        $dt['description'] = $this->input->post('about');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Courses
    public function courses()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-clipboard font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Courses Title', 'required', array("required" => "Enter Courses Title!"));
                $this->form_validation->set_rules('about', 'Set Courses Description', 'required', array("required" => "Enter Courses Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        (!empty($_FILES['courses']['name']) && $dt['photo'] = $this->md->uploadFile('courses')); // check if file is selected than only it will upload
                        $dt['description'] = $this->input->post('about');
                        $dt['duration'] = $this->input->post('duration');
                        $dt['levels'] = $this->input->post('levels');
                        $dt['category_id'] = $this->input->post('parent');
                        $dt['entry_date'] = date('Y-m-d H:i:s');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Courses Title', 'required', array("required" => "Enter Courses Title!"));
                $this->form_validation->set_rules('about', 'Set Courses Description', 'required', array("required" => "Enter Courses Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $this->input->post('updateStatus') && ($dt['photo'] = $this->md->uploadFile('courses'));    // Upload photo and return path from model
                        $dt['description'] = $this->input->post('about');
                        $dt['duration'] = $this->input->post('duration');
                        $dt['levels'] = $this->input->post('levels');
                        $dt['category_id'] = $this->input->post('parent');
                        $dt['modify_date'] = date('Y-m-d H:i:s');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Client
    public function client()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Client Title', 'required', array("required" => "Enter Client Title!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        (!empty($_FILES['client']['name']) && $dt['path'] = $this->md->uploadFile('client')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, Client added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Client Title', 'required', array("required" => "Enter Client Title!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('client'));    // Upload Photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Contact
    public function contact()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Contact Data", TRUE, "fas fa-headphones-alt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }


    // Email Subscriber - Newsletter
    public function email()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Email Subscriber", TRUE, "fas fa-envelope-open-text font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($this->input->post('send_mail')) {
                $this->form_validation->set_rules('subject', 'Subject', 'required', array("required" => "Enter Subject!"));
                $this->form_validation->set_rules('message', 'Message', 'required', array("required" => "Enter Message!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $emails = $this->input->post('email');
                        if (!empty($emails)):
                            foreach ($emails as $eml):
                                $subject = $this->input->post('subject');
                                $message = $this->input->post('message');
                                if ($data['web_data']):
                                    $protocol = $data['web_data'][0]->mail_protocol;    // Protocol
                                    $host = $data['web_data'][0]->mail_smtp_host;   // Host Email Service Provider
                                    $sender = $data['web_data'][0]->mail_email;  // Sender Email
                                    $password = $data['web_data'][0]->mail_password;    // Sender Password
                                    $port = $data['web_data'][0]->mail_port;    // PORT
                                    $this->md->sendMail($protocol, $host, $sender, $password, $port, $subject, $message, $eml); // Send mail
                                endif;
                            endforeach;
                            $this->session->set_flashdata('success', 'Yeah, mail sent successfully.');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        else:
                            $this->session->set_flashdata('error', 'Sorry, select atleast one email!');
                        endif;
                    endif;
                }
            }   // Add Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Feedback
    public function feedback()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comment-dots font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Gallery - Sliders
    public function gallery()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-image font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                if (web_status()):
                    /* $this->form_validation->set_rules('title', 'Set Gallery Title', 'required', array("required" => "Enter Gallery Title!"));
                      if ($this->form_validation->run() == TRUE) { */
                    $dt['title'] = $this->input->post('title');
                    (!empty($_FILES['gallery']['name']) && $dt['path'] = $this->md->uploadFile('gallery')); // check if file is selected than only it will upload
                    $this->md->insert($this->table_prefix . $class, $dt);
                    $this->session->set_flashdata('success', 'Yeah, Gallery added successfully!');
                    redirect($data['current_page'] . "/" . $data['page_type']);
                endif;
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    $dt['title'] = $this->input->post('title');
                    // Check already photo is exist or not, if yes than it will remove and than upload.
                    $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                    $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('gallery'));    // Upload Photo and return path from model
                    if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                        redirect('manage-' . $class . "/show");
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Inquiry
    public function inquiry()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comment-dots font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Careers
    public function careers()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-user-check font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Review
    public function review()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "far fa-comments font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('username', 'Set Review Title', 'required', array("required" => "Enter Username!"));
                $this->form_validation->set_rules('msg', 'Set Review', 'required', array("required" => "Enter Review!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['username'] = $this->input->post('username');
                        $dt['review'] = $this->input->post('msg');
                        (!empty($_FILES['review']['name']) && $dt['path'] = $this->md->uploadFile('review')); // check if file is selected than only it will upload
                        $dt['datetime'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('username', 'Set Review Title', 'required', array("required" => "Enter Username!"));
                $this->form_validation->set_rules('msg', 'Set Review', 'required', array("required" => "Enter Review!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['username'] = $this->input->post('username');
                        $dt['datetime'] = $this->input->post('datetime');
                        $dt['review'] = $this->input->post('msg');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('review'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Faq
    public function faq()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('question', 'Set Faq Question', 'required', array("required" => "Enter Question!"));
                $this->form_validation->set_rules('answer', 'Set Faq Answer', 'required', array("required" => "Enter Answer!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['question'] = $this->input->post('question');
                        $dt['answer'] = $this->input->post('answer');
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('question', 'Set Faq Question', 'required', array("required" => "Enter Question!"));
                $this->form_validation->set_rules('answer', 'Set Faq Answer', 'required', array("required" => "Enter Answer!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['question'] = $this->input->post('question');
                        $dt['answer'] = $this->input->post('answer');
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // SEO
    public function seo()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-angle-double-up font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('page', 'Select Page Title', 'required', array("required" => "Select Page Title!"));
                $this->form_validation->set_rules('meta_title', 'Set Meta Title', 'required', array("required" => "Enter Meta Title!"));
                $this->form_validation->set_rules('meta_keyword', 'Set Meta Keyword', 'required', array("required" => "Enter SEO Keyword!"));
                $this->form_validation->set_rules('meta_desc', 'Set Meta Description', 'required', array("required" => "Enter Meta Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['page'] = $this->input->post('page');
                        $dt['title'] = $this->input->post('meta_title');
                        $dt['description'] = $this->input->post('meta_desc');
                        $dt['keyword'] = $this->input->post('meta_keyword');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('page', 'Select Page Title', 'required', array("required" => "Select Page Title!"));
                $this->form_validation->set_rules('meta_title', 'Set Meta Title', 'required', array("required" => "Enter Meta Title!"));
                $this->form_validation->set_rules('meta_keyword', 'Set Meta Keyword', 'required', array("required" => "Enter SEO Keyword!"));
                $this->form_validation->set_rules('meta_desc', 'Set Meta Description', 'required', array("required" => "Enter Meta Description!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['page'] = $this->input->post('page');
                        $dt['title'] = $this->input->post('meta_title');
                        $dt['description'] = $this->input->post('meta_desc');
                        $dt['keyword'] = $this->input->post('meta_keyword');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Team
    public function team()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-users font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', '', 'required', array("required" => "Enter User FullName!"));
                $this->form_validation->set_rules('email', '', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Phone!"));
                /* $this->form_validation->set_rules('facebook', '', 'required', array("required" => "Enter Facebook Link!"));
                  $this->form_validation->set_rules('instagram', '', 'required', array("required" => "Enter Instagram Link!"));
                  $this->form_validation->set_rules('linkedin', '', 'required', array("required" => "Enter Linkedin Link!")); */
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['name'] = $this->input->post('title');
                        $dt['email'] = $this->input->post('email');
                        $dt['phone'] = $this->input->post('phone');
                        $dt['facebook'] = $this->input->post('facebook');
                        $dt['instagram'] = $this->input->post('instagram');
                        $dt['twitter'] = $this->input->post('twitter');
                        $dt['linkedin'] = $this->input->post('linkedin');
                        (!empty($_FILES['team']['name']) && $dt['path'] = $this->md->uploadFile('team')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', '', 'required', array("required" => "Enter User FullName!"));
                $this->form_validation->set_rules('email', '', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('phone', '', 'required', array("required" => "Enter Phone!"));
                /* $this->form_validation->set_rules('facebook', '', 'required', array("required" => "Enter Facebook Link!"));
                  $this->form_validation->set_rules('instagram', '', 'required', array("required" => "Enter Instagram Link!"));
                  $this->form_validation->set_rules('linkedin', '', 'required', array("required" => "Enter Linkedin Link!")); */
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['name'] = $this->input->post('title');
                        $dt['email'] = $this->input->post('email');
                        $dt['phone'] = $this->input->post('phone');
                        $dt['facebook'] = $this->input->post('facebook');
                        $dt['instagram'] = $this->input->post('instagram');
                        $dt['twitter'] = $this->input->post('twitter');
                        $dt['linkedin'] = $this->input->post('linkedin');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('team'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Video
    public function video()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fas fa-play-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('brand_id', '', 'required', array("required" => "Select Brand!"));
                $this->form_validation->set_rules('title', 'Set Video Title', 'required', array("required" => "Enter Video Title!"));
                $this->form_validation->set_rules('video', 'Set Video Path', 'required', array("required" => "Enter Video Path!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['brand_id'] = $this->input->post('brand_id');
                        $dt['title'] = $this->input->post('title');
                        $dt['video'] = $this->input->post('video');
                        $dt['status'] = 1;
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('brand_id', '', 'required', array("required" => "Select Brand!"));
                $this->form_validation->set_rules('title', 'Set Video Title', 'required', array("required" => "Enter Video Title!"));
                $this->form_validation->set_rules('video', 'Set Video Path', 'required', array("required" => "Enter Video Path!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['brand_id'] = $this->input->post('brand_id');
                        $dt['title'] = $this->input->post('title');
                        $dt['video'] = $this->input->post('video');
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, enter proper data!');
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      I N F O  -  P A G E S
     */

    // About us
    public function aboutus()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage " . $class, TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('about', 'About Us', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['about'] = $this->input->post('about');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('about', 'About Us', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['about'] = $this->input->post('about');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Policy
    public function policy()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage Privacy & Policy", TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('policy', 'Privacy & Policy', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['policy'] = $this->input->post('policy');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('policy', 'Privacy & Policy', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['policy'] = $this->input->post('policy');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Terms
    public function terms()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/info", $class, "Manage Terms & Condition", TRUE, "far fa-question-circle font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'info');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif;  // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('terms', 'Terms & Condition', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['terms'] = $this->input->post('terms');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, something went wrong!');
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, insert proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('terms', 'Terms & Condition', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['terms'] = $this->input->post('terms');
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Update Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      C A T E G O R I E S   -  P A G E S
     */

    // Category
    public function category()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/categories", $class, "Manage Category", TRUE, "far fa-list-alt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'categories');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('slug', 'Slug', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug');
                        $dt['description'] = $this->input->post('description');
                        (!empty($_FILES['category']['name']) && $dt['path'] = $this->md->uploadFile('category')); // check if file is selected than only it will upload
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('slug', 'Slug', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug');
                        $dt['description'] = $this->input->post('description');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('category'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                }
            }   // Edit Data
            if ($this->input->post('importData')) {
                $path = './admin_asset/importData/';
                require_once APPPATH . "/third_party/PHPExcel.php";

                $config['upload_path'] = $path;
                if (!is_dir($config['upload_path']))
                    mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
                $config['allowed_types'] = 'xlsx|xls|csv';
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('importedFile')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
                if (empty($error)) {
                    if (!empty($data['upload_data']['file_name'])) {
                        $import_xls_file = $data['upload_data']['file_name'];
                    } else {
                        $import_xls_file = 0;
                    }
                    $inputFileName = $path . $import_xls_file;
                    try {
                        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFileName);
                        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                        $flag = true;
                        $i = 0;
                        foreach ($allDataInSheet as $value) {
                            if ($flag) {
                                $flag = false;
                                continue;
                            }
                            $inserdata[$i]['title'] = $value['A'];
                            $inserdata[$i]['description'] = $value['B'];
                            $i++;
                        }
                        $result = $this->md->insert_import('tbl_category', $inserdata);
                        if ($result) {
                            echo "Imported successfully";
                        } else {
                            echo "ERROR !";
                        }
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                            . '": ' . $e->getMessage());
                    }
                } else {
                    echo $error['error'];
                }
            }   // Import Data From EXCEL / CSV
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Sub Category
    public function subcategory()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/categories", $class, "Manage Career Library Sub Category", TRUE, "far fa-list-alt font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'categories');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('parent', 'Parent', 'required');
                $this->form_validation->set_rules('title', 'Title', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['parent'] = $this->input->post('parent');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['description'] = $this->input->post('description');
                        (!empty($_FILES['category']['name']) && $insert_data['path'] = $this->md->uploadFile('category')); // check if file is selected than only it will upload
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'title', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo
                        $dt['parent'] = $this->input->post('parent');
                        $dt['title'] = $this->input->post('title');
                        $dt['description'] = $this->input->post('description');
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('category'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Product
    public function product()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage " . $class, TRUE, "fab fa-product-hunt font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Set Product Title', 'required', array("required" => "Enter Product Title!"));
                $this->form_validation->set_rules('slug', 'Set Product slug', 'required', array("required" => "Enter Product slug!"));
                $this->form_validation->set_rules('category_id', '', 'required', array("required" => "Select Category ID!"));
                $this->form_validation->set_rules('short_description', '', 'required', array("required" => "Enter Short Description!"));
                $this->form_validation->set_rules('description', '', 'required', array("required" => "Enter Description!"));
                $this->form_validation->set_rules('additional_info', '', 'required', array("required" => "Enter Additional Info!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['category_id'] = $this->input->post('category_id');
                        $dt['sku'] = $this->input->post('sku');
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug');
                        $dt['standard_price'] = $this->input->post('standard_price');
                        $dt['short_description'] = $this->input->post('short_description');
                        $dt['description'] = $this->input->post('description');
                        $dt['additional_info'] = $this->input->post('additional_info');
                        (strlen($_FILES['product_photos']['name'][0]) > 0) && $dt['photos'] = $this->md->uploadMultiFile('product_photos', count($_FILES['product_photos']['name'])); // check if file is selected than only it will upload
                        $dt['tags'] = $this->input->post('tags');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['featured'] = $this->input->post('featured') ? 1 : 0;
                        $dt['status'] = 1;
                        $dt['entry_date'] = date('Y-m-d');
                        $this->md->insert($this->table_prefix . $class, $dt);
                        $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Set Product Title', 'required', array("required" => "Enter Product Title!"));
                $this->form_validation->set_rules('slug', 'Set Product slug', 'required', array("required" => "Enter Product slug!"));
                $this->form_validation->set_rules('category_id', '', 'required', array("required" => "Select Category ID!"));
                $this->form_validation->set_rules('short_description', '', 'required', array("required" => "Enter Short Description!"));
                $this->form_validation->set_rules('description', '', 'required', array("required" => "Enter Description!"));
                $this->form_validation->set_rules('additional_info', '', 'required', array("required" => "Enter Additional Info!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $dt['category_id'] = $this->input->post('category_id');
                        $dt['sku'] = $this->input->post('sku');
                        $dt['title'] = $this->input->post('title');
                        $dt['slug'] = $this->input->post('slug');
                        $dt['standard_price'] = $this->input->post('standard_price');
                        $dt['short_description'] = $this->input->post('short_description');
                        $dt['description'] = $this->input->post('description');
                        $dt['additional_info'] = $this->input->post('additional_info');
                        (strlen($_FILES['product_photos']['name'][0]) > 0) && $photos = $this->md->uploadMultiFile('product_photos', count($_FILES['product_photos']['name'])); // check if file is selected than only it will upload
                        if ($photos) :
                            $dt['photos'] = $this->input->post('oldPath') ? $this->input->post('oldPath') . "," . $photos : $photos;
                        endif;
                        $dt['tags'] = $this->input->post('tags');
                        $dt['meta_title'] = $this->input->post('meta_title');
                        $dt['meta_keyword'] = $this->input->post('meta_keyword');
                        $dt['meta_desc'] = $this->input->post('meta_desc');
                        $dt['featured'] = $this->input->post('featured') ? 1 : 0;
                        $dt['modify_date'] = date('Y-m-d');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Bill
    public function bill()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Bill Data", TRUE, "fas fa-file-invoice font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // Register
    public function register()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/site-pages", $class, "Manage Register Data", TRUE, "fas fa-file-invoice font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'site-pages');    // Fetch Data from model in return to function
        else:
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
     PDF REPORT - INVOICE
    */
    // Generate PDF Report
    public function generate_report()
    {
        $downloadStatus = $this->uri->segment(2);   // (view / download)
        $invoice_number = $this->uri->segment(3);   // transaction_id
        $this->load->library('PDF');
        $invoiceData = $this->md->select_where('tbl_bill', array('transaction_id' => $invoice_number));
        $html = $this->load->view('admin/site-pages/report', array('invoiceData' => $invoiceData), true);
        $this->pdf->createPDF($html, "Invoice No. - " . $invoice_number, ($downloadStatus == 'download' ? TRUE : FALSE));
    }


    /*
      U S E R  -  P A G E S
     */

    // User - Admin - Super Admin Manage
    public function user()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage " . $class, TRUE, "fas fa-users font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . 'admin', array('admin_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('username', 'Set User Fullname', 'required', array("required" => "Enter User Fullname!"));
                $this->form_validation->set_rules('email', 'Set User Email', 'required', array("required" => "Enter Email!"));
                $this->form_validation->set_rules('password', 'Set User Password', 'required', array("required" => "Enter Password!"));
                $this->form_validation->set_rules('role', 'Select User Role', 'required', array("required" => "Select User Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $wh['email'] = $this->input->post('email'); // where condition
                        $admin_data = $this->md->select_where($this->table_prefix . 'admin', $wh); // get admin data from where condition
                        if (empty($admin_data)):
                            $dt['admin_name'] = $this->input->post('username');
                            $dt['email'] = $this->input->post('email');
                            $dt['phone'] = ($this->input->post('phone') ? $this->input->post('phone') : '');
                            $dt['address'] = ($this->input->post('address') ? $this->input->post('address') : '');
                            $dt['user_role_id'] = ($this->input->post('role') ? $this->input->post('role') : '');
                            $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                            $dt['password'] = $this->encryption->encrypt($this->input->post('password'));
                            (!empty($_FILES['user']['name']) && $dt['path'] = $this->md->uploadFile('user')); // check if file is selected than only it will upload
                            $dt['register_date'] = date('Y-m-d');
                            $this->md->insert($this->table_prefix . 'admin', $dt);
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        else:
                            $this->session->set_flashdata('error', 'Sorry, Email already registered!');
                        endif;
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('username', 'Set User Fullname', 'required', array("required" => "Enter User Fullname!"));
                $this->form_validation->set_rules('password', 'Set User Password', 'required', array("required" => "Enter Password!"));
                $this->form_validation->set_rules('role', 'Select User Role', 'required', array("required" => "Select User Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        // Check already photo is exist or not, if yes than it will remove and than upload.
                        $this->input->post('updateStatus') && (($this->input->post('oldPath') != FILENOTFOUND) ? unlink($this->input->post('oldPath')) : '');  // Remove Old Photo

                        $dt['admin_name'] = $this->input->post('username');
                        $dt['phone'] = ($this->input->post('phone') ? $this->input->post('phone') : '');
                        $dt['address'] = ($this->input->post('address') ? $this->input->post('address') : '');
                        $dt['user_role_id'] = ($this->input->post('role') ? $this->input->post('role') : '');
                        $dt['status'] = (($this->input->post('status') == 1) ? 1 : 0);
                        $dt['password'] = $this->encryption->encrypt($this->input->post('password'));
                        $this->input->post('updateStatus') && ($dt['path'] = $this->md->uploadFile('user'));    // Upload photo and return path from model
                        if ($this->md->update($this->table_prefix . 'admin', $dt, array('admin_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, Enter proper data!');
                }
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // User Role
    public function role()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage User Role", TRUE, "fas fa-user-tag font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'User Role', 'required|is_unique[tbl_role.title]', array('is_unique' => 'Sorry, Role already exist!'));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['remark'] = $this->input->post('remark');
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    $exist = $this->md->select_where($this->table_prefix . $class, array('title' => $this->input->post('title')));
                    if (empty($exist)) {
                        $dt['title'] = $this->input->post('title');
                        $dt['remark'] = $this->input->post('remark');
                        if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    } else {
                        if ($exist[0]->role_id == $id) {
                            $dt['title'] = $this->input->post('title');
                            $dt['remark'] = $this->input->post('remark');
                            if ($this->md->update($this->table_prefix . $class, $dt, array($class . '_id' => $id))) {
                                $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                                redirect('manage-' . $class . "/show");
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Role Already exist!');
                        }
                    }
                endif;
            }   // Edit Data
            if ($this->input->post('assign')) {
                $this->form_validation->set_rules('role_id', 'Role ID', 'required', array("required" => "Select Role!"));
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $this->md->delete($this->table_prefix . 'permission_assign', array('role_id' => $this->input->post('role_id')));  // Delete current role's permisison
                        // Feature Permission
                        $feature_all = ($this->input->post('feature_all_per') ? json_encode(array_keys($this->input->post('feature_all_per'), 1)) : '');
                        $feature_read = ($this->input->post('feature_read_per') ? json_encode(array_keys($this->input->post('feature_read_per'), 1)) : '');
                        $feature_edit = ($this->input->post('feature_edit_per') ? json_encode(array_keys($this->input->post('feature_edit_per'), 1)) : '');

                        // Pages Permission
                        $page_all = ($this->input->post('all_per') ? json_encode(array_keys($this->input->post('all_per'), 1)) : '');
                        $page_read = ($this->input->post('read_per') ? json_encode(array_keys($this->input->post('read_per'), 1)) : '');
                        $page_write = ($this->input->post('write_per') ? json_encode(array_keys($this->input->post('write_per'), 1)) : '');
                        $page_edit = ($this->input->post('edit_per') ? json_encode(array_keys($this->input->post('edit_per'), 1)) : '');
                        $page_delete = ($this->input->post('delete_per') ? json_encode(array_keys($this->input->post('delete_per'), 1)) : '');
                        $page_status = ($this->input->post('status_per') ? json_encode(array_keys($this->input->post('status_per'), 1)) : '');

                        $permissions = array('feature_all', 'feature_read', 'feature_edit', 'page_all', 'page_read', 'page_write', 'page_edit', 'page_delete', 'page_status');
                        if (!empty($permissions)) :
                            foreach ($permissions as $per_item):
                                $per_type = substr($per_item, (strrpos($per_item, '_') ?: -1) + 1);
                                $dt['role_id'] = $this->input->post('role_id'); // Role ID
                                $dt['permission_id'] = $$per_item;   // permission ID
                                $dt['type'] = strtok($per_item, '_');   // Get type from array value, before '_'
                                $dt['all'] = ($per_type == 'all' ? '1' : '0');   // All permission
                                $dt['read'] = ($per_type == 'all' ? '1' : ($per_type == 'read' ? '1' : '0'));   // Read permission
                                $dt['edit'] = ($per_type == 'all' ? '1' : ($per_type == 'edit' ? '1' : '0'));   // Edit permission
                                $dt['write'] = ($per_type == 'all' ? '1' : ($per_type == 'write' ? '1' : '0'));   // Write permission
                                $dt['delete'] = ($per_type == 'all' ? '1' : ($per_type == 'delete' ? '1' : '0'));   // Delete permission
                                $dt['status'] = ($per_type == 'all' ? '1' : ($per_type == 'status' ? '1' : '0'));   // Status permission
                                (!empty($$per_item) && $this->md->insert($this->table_prefix . 'permission_assign', $dt));
                            endforeach;
                        endif;

                        $this->session->set_flashdata('success', 'Yeah, Permission granted successfully.');
                        redirect('manage-' . $class . "/show");
                    endif;
                } else {
                    $this->session->set_flashdata('error', 'Sorry, permission not granted!');
                }
            }   // Assign Permission Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // User Permission
    public function permission()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/user", $class, "Manage User Permission", TRUE, "fas fa-user-check font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'user');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('type', 'Permission Type', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        if ($this->input->post('type') == "page"):
                            $options = $this->input->post('page_option');
                            $insert_data['feature'] = strtolower($this->input->post('page'));
                            $insert_data['menu'] = strtolower($this->input->post('menu'));
                        else:
                            $options = $this->input->post('feature_option');
                            $insert_data['feature'] = strtolower($this->input->post('feature'));
                        endif;
                        $insert_data['type'] = $this->input->post('type');

                        // Check data already exist or not
                        $exist = $this->md->select_where($this->table_prefix . $class, array('type' => $this->input->post('type'), 'feature' => $insert_data['feature']));
                        if (empty($exist)):
                            if ($options):
                                foreach ($options as $option_item):
                                    if ($option_item == 'all'):
                                        $insert_data['all'] = '1';
                                        $insert_data['read'] = '1';
                                        $insert_data['edit'] = '1';
                                        if ($this->input->post('type') == "page"):
                                            $insert_data['write'] = '1';
                                            $insert_data['delete'] = '1';
                                            $insert_data['status'] = '1';
                                        endif;
                                    else:
                                        $insert_data[$option_item] = '1';
                                    endif;
                                endforeach;
                                if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                                    $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                                } else {
                                    $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                                }
                            else:
                                $this->session->set_flashdata('error', 'Sorry, select atleast one permission!');
                            endif;
                        else:
                            $this->session->set_flashdata('error', 'Sorry, Data already exist!');
                        endif;
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                if (web_status()):
                    if ($this->input->post('type') == "page"):
                        $options = $this->input->post('page_option');
                        $insert_data['feature'] = strtolower($this->input->post('page'));
                        $insert_data['menu'] = strtolower($this->input->post('menu'));
                    else:
                        $options = $this->input->post('feature_option');
                        $insert_data['feature'] = strtolower($this->input->post('feature'));
                    endif;
                    $insert_data['type'] = $this->input->post('type');
                    $updata = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
                    $up_data['all'] = '0';
                    $up_data['read'] = '0';
                    $up_data['edit'] = '0';
                    $up_data['write'] = '0';
                    $up_data['delete'] = '0';
                    $up_data['status'] = '0';
                    if ($this->md->update($this->table_prefix . $class, $up_data, array($class . '_id' => $id))) {
                        $type = $updata ? $updata[0]->type : '';
                        $options = ($type == "page") ? $this->input->post('page_option') : $options = $this->input->post('feature_option');
                        if (!empty($options)):
                            foreach ($options as $option_item):
                                if ($option_item == 'all'):
                                    $insert_data['all'] = '1';
                                    $insert_data['read'] = '1';
                                    $insert_data['edit'] = '1';
                                    if ($type == "page"):
                                        $insert_data['write'] = '1';
                                        $insert_data['delete'] = '1';
                                        $insert_data['status'] = '1';
                                    endif;
                                else:
                                    $insert_data[$option_item] = '1';
                                endif;
                            endforeach;
                        endif;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not updated!');
                            redirect($data['current_page'] . "/" . $data['page_type']);
                        }
                    }
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      M E N U  -  P A G E S
     */

    // User Mainmenu
    public function mainmenu()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/menu", $class, "Manage Mainmenu", TRUE, "fas fa-ellipsis-h font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'menu');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Mainmenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('controller', 'Mainmenu controller', 'required');
                $this->form_validation->set_rules('title', 'Mainmenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        $insert_data['controller'] = $this->input->post('controller');
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    // User Submenu
    public function submenu()
    {
        $class = __FUNCTION__;  // Get Current Function Name
        $data = $this->setParameter("admin/menu", $class, "Manage Submenu", TRUE, "fas fa-ellipsis-v font-20 mr-5 font-20 mr-5", TRUE);
        $data['permission'] = $this->check_permission($class);  // check current user has permission or not to use this page
        if ($data['page_type'] == "getdata"):
            echo $this->fetchData($class, 'menu');    // Fetch Data from model in return to function
        else:
            if ($data['page_type'] == "edit"):
                $id = $this->uri->segment(3); // Get Record ID
                $data['updata'] = $this->md->select_where($this->table_prefix . $class, array($class . '_id' => $id));
            endif; // Get Editable Record
            if ($this->input->post('add')) {
                $this->form_validation->set_rules('title', 'Submenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) {
                    if (web_status()):
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['mainmenu_id'] = $this->input->post('parent');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->insert($this->table_prefix . $class, $insert_data)) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' added successfully!');
                        } else {
                            $this->session->set_flashdata('error', 'Sorry, Data not insterted!');
                        }
                        redirect($data['current_page'] . "/" . $data['page_type']);
                    endif;
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                }
            }   // Add Data
            if ($this->input->post('update')) {
                $this->form_validation->set_rules('title', 'Submenu Title', 'required');
                $this->form_validation->set_rules('url', 'Menu URL', 'required');
                if ($this->form_validation->run() == TRUE) :
                    if (web_status()):
                        $insert_data['title'] = $this->input->post('title');
                        $insert_data['mainmenu_id'] = $this->input->post('parent');
                        $insert_data['slug'] = $this->input->post('slug') ? strtolower(str_replace(" ", "-", $this->input->post('slug'))) : strtolower(str_replace(" ", "-", $this->input->post('title')));
                        $insert_data['position'] = $this->input->post('position');
                        $insert_data['icon'] = $this->input->post('icon');
                        $insert_data['url'] = $this->input->post('url');
                        $insert_data['status'] = $this->input->post('status') ? $this->input->post('status') : 0;
                        if ($this->md->update($this->table_prefix . $class, $insert_data, array($class . '_id' => $id))) {
                            $this->session->set_flashdata('success', 'Yeah, ' . $class . ' updated successfully!');
                            redirect('manage-' . $class . "/show");
                        }
                    endif;
                endif;
            }   // Edit Data
            $this->load->view('admin/common/master', $data);
        endif;
    }

    /*
      A J A X  -  C A L L
     */

    // get title to slug AJAX Call
    public function getSlug()
    {
        $val = $this->input->post('str');
        echo $this->md->generateSeoURL($val);
    }

    // get sub category AJAX Call
    public function get_category()
    {
        $val = $this->input->post('val');
        echo '<option value="">Select Sub Category</option>';
        $subcategory = $this->md->select_where('tbl_subcategory', array('parent' => $val));
        if (!empty($subcategory)) {
            foreach ($subcategory as $subcategory_data) {
                echo '<option value="' . $subcategory_data->subcategory_id . '">' . $subcategory_data->title . '</option>';
            }
        }
    }

    // get Submenu Position AJAX Call
    public function get_submenuPosition()
    {
        $mainMenuId = $this->input->post('mainMenuId');   // get mainmenu id
        $submenu = $this->md->select_where('tbl_submenu', array('mainmenu_id' => $mainMenuId));
        $submenu = count($submenu);   // Count data of submenu table
        if ($submenu != 0):
            for ($i = ($submenu + 1); $i >= 1; $i--):
                echo '<option value="' . $i . '">' . $i . '</option>';
            endfor;
        else:
            echo '<option value="1">1</option>';
        endif;
    }

    // get Role's Granted permission AJAX Call
    public function get_permission_table()
    {
        $roleid = $this->input->post('roleid');   // get role id
        $permission = $this->md->select('tbl_permission');  // get all permission
        $existing_permission = $this->md->select_where('tbl_permission_assign', array('role_id' => $roleid));   // get current role's permisison
        $per_id = array();
        foreach ($existing_permission as $ext_per) {
            $per = json_decode($ext_per->permission_id);
            foreach ($per as $per_val):
                $per_id[] = array('per_id' => $per_val, 'assign_id' => $ext_per->permission_assigned_id);
            endforeach;
        }
        echo '<input type="hidden" value="' . $roleid . '" name="role_id" />';
        echo '<table class="table table-bordered table-hover table-sm permission_table">';
        echo '<tr class="bg-secondary text-000">
                <th>Features/Pages</th> 
                <th>All</th>
                <th>Read</th>
                <th>Write</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Status</th>
            </tr>';
        foreach ($permission as $fields_val):
            $exist = false;
            $exist_prmsn = array();
            if (!empty($per_id)):
                $key = array_search($fields_val->permission_id, array_column($per_id, 'per_id'));
                if (!empty($key) || $key === 0) {
                    $exist = true;
                    $exist_prmsn = $this->md->select_where('tbl_permission_assign', array('permission_assigned_id' => $per_id[$key]['assign_id']));
                }
            endif;
            $all = $read = $write = $edit = $delete = $status = 0;
            if ($exist):
                if (!empty($exist_prmsn)):
                    $all = $exist_prmsn[0]->all;
                    $read = $exist_prmsn[0]->read;
                    $write = $exist_prmsn[0]->write;
                    $edit = $exist_prmsn[0]->edit;
                    $delete = $exist_prmsn[0]->delete;
                    $status = $exist_prmsn[0]->status;
                endif;
            endif;
            if ($fields_val->type == 'page'):
                echo '<tr>';
                ?>
                <td class="font-weight-bold"><span
                        class="badge badge-dark zoom-08">Page</span> <?php echo ucfirst($fields_val->feature); ?>
                </td> <!-- Feature Name -->
                <td><?php if ($fields_val->all): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($all) ? 'checked' : ''; ?>
                               onchange="checkAllPermission(this)" value="1"
                               name="all_per[<?php echo $fields_val->permission_id; ?>]" class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << All Permission  -->
                <td><?php if ($fields_val->read): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($read) ? 'checked' : ''; ?>
                               name="read_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Read Permission  -->
                <td><?php if ($fields_val->write): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($write) ? 'checked' : ''; ?>
                               name="write_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Write Permission  -->
                <td><?php if ($fields_val->edit): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($edit) ? 'checked' : ''; ?>
                               name="edit_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Edit Permission  -->
                <td><?php if ($fields_val->delete): ?><label class="custom-switch zoom-08 cursor-pointer ">
                        <input type="checkbox" <?php echo ($delete) ? 'checked' : ''; ?>
                               name="delete_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label><?php endif; ?>
                </td> <!-- << Delete Permission  -->
                <td><?php if ($fields_val->status): ?><label class="custom-switch zoom-08 cursor-pointer ">
                        <input type="checkbox" <?php echo ($status) ? 'checked' : ''; ?>
                               name="status_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label><?php endif; ?>
                </td> <!-- << Status Permission  -->
                <?php
                echo '</tr>';
            elseif ($fields_val->type == 'feature'):
                echo '<tr>';
                ?>
                <td class="font-weight-bold">
                    <span class="badge badge-info zoom-08">Feature</span> <?php echo ucfirst($fields_val->feature); ?>
                </td> <!-- Feature Name -->
                <td><?php if ($fields_val->all): ?> <label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($all) ? 'checked' : ''; ?>
                               onchange="checkAllPermission(this)" value="1"
                               name="feature_all_per[<?php echo $fields_val->permission_id; ?>]"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << All Permission  -->
                <td><?php if ($fields_val->read): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($read) ? 'checked' : ''; ?>
                               name="feature_read_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Read Permission  -->
                <td>
                    <!-- Write Permission Not Require in Feature Module -->
                </td> <!-- << Write Permission  -->
                <td><?php if ($fields_val->edit): ?><label class="custom-switch zoom-08 cursor-pointer">
                        <input type="checkbox" <?php echo ($edit) ? 'checked' : ''; ?>
                               name="feature_edit_per[<?php echo $fields_val->permission_id; ?>]" value="1"
                               class="custom-switch-input">
                        <span class="custom-switch-indicator"></span></label> <?php endif; ?>
                </td> <!-- << Edit Permission  -->
                <td>
                    <!-- Delete Permission Not Require in Feature Module -->
                </td> <!-- << Delete Permission  -->
                <?php
                echo '</tr>';
            endif;
        endforeach;
        echo '</table>';
    }

    /*
      C O M M O N  -  D E L E T E
     */


    // Remove photo Data
    public function removePhoto()
    {
        $table = $this->uri->segment(4);
        $id = $this->uri->segment(5);
        $path = $this->uri->segment(6);
        if ($this->db->table_exists($this->table_prefix . $table)) :
            $data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id))[0]->photos;
            $photos = explode(",", $data);
            unlink($photos[$path]);
            unset($photos[$path]);
            $this->md->update($this->table_prefix . $table, array('photos' => implode(",", $photos)), array($table . '_id' => $id));
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

    // Common Remove Data
    public function delete()
    {
        if (web_status()):
            $table = $this->input->post('tbl'); // table name
            $id = $this->input->post('id'); // item id
            $path = $this->input->post('path') ? $this->input->post('path') : ''; // item photo field
            if ($this->db->table_exists($this->table_prefix . $table)):
                if ($path):
                    $data = $this->md->select_where($this->table_prefix . $table, array($table . '_id' => $id));
                    if (!empty($data)):
                        $photos = explode(",", $data[0]->$path);
                        foreach ($photos as $pic):
                            unlink("./" . $pic);
                        endforeach;
                    endif;
                endif;
                $this->md->delete($this->table_prefix . $table, array($table . '_id' => $id));
            endif;
            echo "true";
        else:
            echo "false";
        endif;
    }

}
