<?php

/* * *
 * Project:    Enzo Admin Pro
 * Name:       Installer
 * Package:    Install.php
 * About:      A controller that handle installation of /ci project
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {

    // Global Variables
    public $website, $developer, $web_link, $table_prefix;

    public function __construct() {
        parent::__construct();
        //$this->load->model(array(''));
        $this->load->helper(array('url', 'form_helper', 'website'));
        $this->load->library(array('form_validation', 'session'));
        $this->developer = DEVELOPER;
        $this->web_link = WEBLINK;
        $this->table_prefix = TABLE_PREFIX;     // SET TABLE PREFIX (TBL_) 
    }

    // Page not found 404
    public function page_not_found() {
        $data = [];
        $data['page_title'] = ucwords("404 - Page Not Found");  // set page title  
        $data['page'] = 'admin/errors/404';   //  open page from admin folder  
        $data['web_data'] = ($this->db->table_exists('tbl_web_data')) ? $this->md->select('tbl_web_data') : '';    // SELECT WEBSITE DATA    // get all Website data
        $data['admin_data'] = []; // SELECT ADMIN DATA
        $data['header'] = false;
        $data['sidebar'] = false;
        $data['breadcrumb'] = false;
        $data['breadcrumb_icon'] = false;
        $data['datatables'] = false;
        $this->load->view('admin/common/master', $data);
    }

    public function next() {
        $this->load->database();
        $queries = array(
            "CREATE TABLE `tbl_aboutus` (
                `aboutus_id` int(12) NOT NULL,
                `about` text NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
                ",
            "CREATE TABLE `tbl_admin` (
                `admin_id` int(12) NOT NULL,
                `user_role_id` int(11) NOT NULL,
                `admin_name` varchar(50) NOT NULL,
                `email` varchar(100) NOT NULL,
                `phone` varchar(20) NOT NULL,
                `password` varchar(500) NOT NULL,
                `path` varchar(500) NOT NULL COMMENT 'Profile Photo',
                `address` varchar(200) NOT NULL,
                `bio` varchar(1000) NOT NULL,
                `theme` varchar(10) NOT NULL,
                `lastseen` datetime NOT NULL,
                `register_date` date NOT NULL,
                `status` int(11) NOT NULL COMMENT 'Profile Status'
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
              ",
            "CREATE TABLE `tbl_banner` (
                `banner_id` int(12) NOT NULL,
                `title` varchar(500) NOT NULL,
                `subtitle` varchar(500) NOT NULL,
                `path` varchar(1000) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
                ",
            "CREATE TABLE `tbl_blog` (
                `blog_id` int(10) NOT NULL,
                `title` varchar(1000) NOT NULL,
                `path` varchar(300) NOT NULL,
                `description` text NOT NULL,
                `meta_title` varchar(250) NOT NULL,
                `meta_keyword` varchar(500) NOT NULL,
                `meta_desc` varchar(500) NOT NULL,
                `blogdate` date NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_category` (
                `category_id` int(11) NOT NULL,
                `title` varchar(50) NOT NULL,
                `path` varchar(500) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_client` (
                `client_id` int(11) NOT NULL,
                `title` varchar(100) NOT NULL,
                `path` varchar(500) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_contact` (
                `contact_id` int(12) NOT NULL,
                `name` varchar(200) NOT NULL,
                `email` varchar(200) NOT NULL,
                `phone` varchar(200) NOT NULL,
                `subject` varchar(2000) NOT NULL,
                `message` varchar(2000) NOT NULL,
                `datetime` varchar(50) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
              ",
            "CREATE TABLE `tbl_control` (
                `control_id` int(11) NOT NULL,
                `aboutus` enum('1','0') NOT NULL,
                `banner` enum('1','0') NOT NULL,
                `blog` enum('1','0') NOT NULL,
                `category` enum('1','0') NOT NULL,
                `client` enum('1','0') NOT NULL,
                `email_subscriber` enum('1','0') NOT NULL,
                `contact` enum('1','0') NOT NULL,
                `feedback` enum('1','0') NOT NULL,
                `gallery` enum('1','0') NOT NULL,
                `inquiry` enum('1','0') NOT NULL,
                `mainmenu` enum('1','0') NOT NULL,
                `permission` enum('1','0') NOT NULL,
                `policy` enum('1','0') NOT NULL,
                `review` enum('1','0') NOT NULL,
                `role` enum('1','0') NOT NULL,
                `seo` enum('1','0') NOT NULL,
                `subcategory` enum('1','0') NOT NULL,
                `submenu` enum('1','0') NOT NULL,
                `terms` enum('1','0') NOT NULL,
                `team` enum('1','0') NOT NULL,
                `user` enum('1','0') NOT NULL,
                `video` enum('1','0') NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_control_feature` (
                `control_feature_id` int(11) NOT NULL,
                `general` varchar(200) NOT NULL,
                `social` varchar(200) NOT NULL,
                `email` varchar(200) NOT NULL,
                `security` varchar(200) NOT NULL,
                `system` varchar(200) NOT NULL,
                `captcha` varchar(200) NOT NULL,
                `theme` varchar(200) NOT NULL,
                `payment` varchar(200) NOT NULL,
                `language` varchar(200) NOT NULL,
                `backup` varchar(200) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_email_subscriber` (
                `email_subscriber_id` int(12) NOT NULL,
                `email` varchar(500) NOT NULL,
                `datetime` datetime NOT NULL DEFAULT current_timestamp()
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_feedback` (
                `feedback_id` int(11) NOT NULL,
                `name` varchar(100) NOT NULL,
                `email` varchar(100) NOT NULL,
                `phone` varchar(50) NOT NULL,
                `message` varchar(1500) NOT NULL,
                `datetime` datetime NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_gallery` (
                `gallery_id` int(11) NOT NULL,
                `category` varchar(50) NOT NULL,
                `title` varchar(1000) NOT NULL,
                `path` varchar(2000) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_inquiry` (
                `inquiry_id` int(12) NOT NULL,
                `name` varchar(500) NOT NULL,
                `email` varchar(500) NOT NULL,
                `phone` varchar(500) NOT NULL,
                `customer_id` int(11) NOT NULL,
                `subject` varchar(500) NOT NULL,
                `message` varchar(2000) NOT NULL,
                `datetime` datetime NOT NULL,
                `status` int(11) NOT NULL COMMENT '0-pending | 1- approve'
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_mainmenu` (
                `mainmenu_id` int(11) NOT NULL,
                `title` varchar(100) NOT NULL,
                `slug` varchar(50) NOT NULL,
                `position` int(11) NOT NULL,
                `icon` varchar(50) NOT NULL,
                `controller` varchar(50) NOT NULL,
                `url` varchar(200) NOT NULL,
                `status` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_menu` (
                `menu_id` int(11) NOT NULL,
                `text` varchar(100) NOT NULL,
                `icon` varchar(30) NOT NULL,
                `href` varchar(200) NOT NULL,
                `title` varchar(100) NOT NULL COMMENT 'slug',
                `target` varchar(10) NOT NULL,
                `parent` int(11) NOT NULL,
                `position` int(11) NOT NULL,
                `status` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_permission` (
                `permission_id` int(11) NOT NULL,
                `menu` char(50) NOT NULL,
                `type` varchar(15) NOT NULL,
                `feature` varchar(30) NOT NULL,
                `all` enum('0','1') NOT NULL,
                `read` enum('0','1') NOT NULL,
                `write` enum('0','1') NOT NULL,
                `edit` enum('0','1') NOT NULL,
                `delete` enum('0','1') NOT NULL,
                `status` enum('0','1') NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_permission_assign` (
                `permission_assigned_id` int(11) NOT NULL,
                `role_id` int(11) NOT NULL,
                `permission_id` varchar(100) NOT NULL,
                `type` varchar(15) NOT NULL,
                `all` enum('1','0') NOT NULL,
                `read` enum('1','0') NOT NULL,
                `write` enum('1','0') NOT NULL,
                `edit` enum('1','0') NOT NULL,
                `delete` enum('1','0') NOT NULL,
                `status` enum('1','0') NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_policy` (
                `policy_id` int(11) NOT NULL,
                `policy` text NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_review` (
                `review_id` int(11) NOT NULL,
                `username` varchar(100) NOT NULL,
                `review` varchar(2000) NOT NULL,
                `datetime` datetime NOT NULL,
                `path` varchar(100) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_role` (
                `role_id` int(11) NOT NULL,
                `title` varchar(50) NOT NULL,
                `remark` varchar(200) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            ",
            "CREATE TABLE `tbl_seo` (
                `seo_id` int(12) NOT NULL,
                `page` varchar(500) NOT NULL,
                `title` varchar(2000) NOT NULL,
                `description` text NOT NULL,
                `keyword` text NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_subcategory` (
                `subcategory_id` int(11) NOT NULL,
                `title` varchar(50) NOT NULL,
                `parent` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_submenu` (
                `submenu_id` int(11) NOT NULL,
                `mainmenu_id` int(11) NOT NULL,
                `title` varchar(100) NOT NULL,
                `slug` varchar(50) NOT NULL,
                `position` int(11) NOT NULL,
                `icon` varchar(50) NOT NULL,
                `url` varchar(200) NOT NULL,
                `status` int(11) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_team` (
                `team_id` int(11) NOT NULL,
                `name` varchar(1000) NOT NULL,
                `path` varchar(1000) NOT NULL,
                `facebook` varchar(1000) NOT NULL,
                `instagram` varchar(1000) NOT NULL,
                `email` varchar(1000) NOT NULL,
                `phone` varchar(1000) NOT NULL,
                `linkedin` varchar(1000) NOT NULL,
                `twitter` text NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_terms` (
                `terms_id` int(12) NOT NULL,
                `terms` text NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_timezone` (
                `timezone_id` int(11) NOT NULL,
                `identifier` varchar(50) NOT NULL,
                `timezone` varchar(100) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_video` (
                `video_id` int(11) NOT NULL,
                `video` varchar(150) NOT NULL,
                `title` varchar(150) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "CREATE TABLE `tbl_web_data` (
                `web_data_id` int(11) NOT NULL,
                `project_name` varchar(100) NOT NULL,
                `email_address` varchar(500) NOT NULL,
                `phone` varchar(100) NOT NULL,
                `facebook` varchar(100) NOT NULL,
                `instagram` varchar(100) NOT NULL,
                `twitter` varchar(100) NOT NULL,
                `linkedin` varchar(100) NOT NULL,
                `youtube` varchar(100) NOT NULL,
                `whatsapp` varchar(50) NOT NULL,
                `office` varchar(20) NOT NULL,
                `address` varchar(200) NOT NULL,
                `map` varchar(1000) NOT NULL,
                `logo` varchar(300) NOT NULL,
                `favicon` varchar(300) NOT NULL,
                `footer` varchar(500) NOT NULL,
                `mail_smtp_host` varchar(200) NOT NULL,
                `mail_protocol` varchar(100) NOT NULL,
                `mail_port` int(11) NOT NULL,
                `mail_email` varchar(100) NOT NULL,
                `mail_password` varchar(50) NOT NULL,
                `forgot_mail_subject` varchar(200) NOT NULL,
                `forgot_mail_message` varchar(2000) NOT NULL,
                `welcome_mail_subject` varchar(200) NOT NULL,
                `welcome_mail_message` varchar(2000) NOT NULL,
                `inquiry_mail_subject` varchar(200) NOT NULL,
                `inquiry_mail_message` varchar(2000) NOT NULL,
                `active_mail_subject` varchar(200) NOT NULL,
                `active_mail_message` varchar(2000) NOT NULL,
                `inactive_mail_subject` varchar(200) NOT NULL,
                `inactive_mail_message` varchar(2000) NOT NULL,
                `captcha_site_key` varchar(100) NOT NULL,
                `captcha_secret_key` varchar(100) NOT NULL,
                `captcha_visibility` int(11) NOT NULL,
                `date_format` varchar(50) NOT NULL,
                `time_format` varchar(50) NOT NULL,
                `timezone` varchar(50) NOT NULL,
                `meta_keyword` varchar(200) NOT NULL,
                `meta_desc` varchar(200) NOT NULL
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
              ",
            "ALTER TABLE `tbl_aboutus`
                ADD PRIMARY KEY (`aboutus_id`);",
            "ALTER TABLE `tbl_admin`
                ADD PRIMARY KEY (`admin_id`);",
            "ALTER TABLE `tbl_banner`
                ADD PRIMARY KEY (`banner_id`); ",
            "ALTER TABLE `tbl_blog`
                ADD PRIMARY KEY (`blog_id`);",
            "ALTER TABLE `tbl_category`
                ADD PRIMARY KEY (`category_id`); ",
            "ALTER TABLE `tbl_client`
                ADD PRIMARY KEY (`client_id`);",
            "ALTER TABLE `tbl_contact`
                ADD PRIMARY KEY (`contact_id`); ",
            "ALTER TABLE `tbl_control`
                ADD PRIMARY KEY (`control_id`);",
            "ALTER TABLE `tbl_control_feature`
                ADD PRIMARY KEY (`control_feature_id`);",
            "ALTER TABLE `tbl_email_subscriber`
                ADD PRIMARY KEY (`email_subscriber_id`);",
            "ALTER TABLE `tbl_feedback`
                ADD PRIMARY KEY (`feedback_id`);",
            "ALTER TABLE `tbl_gallery`
                ADD PRIMARY KEY (`gallery_id`);",
            "ALTER TABLE `tbl_inquiry`
                ADD PRIMARY KEY (`inquiry_id`);",
            "ALTER TABLE `tbl_mainmenu`
                ADD PRIMARY KEY (`mainmenu_id`);",
            "ALTER TABLE `tbl_menu`
                ADD PRIMARY KEY (`menu_id`);",
            "ALTER TABLE `tbl_permission`
                ADD PRIMARY KEY (`permission_id`);",
            "ALTER TABLE `tbl_permission_assign`
                ADD PRIMARY KEY (`permission_assigned_id`),
                ADD KEY `Property ID` (`permission_id`);",
            "ALTER TABLE `tbl_policy`
                ADD PRIMARY KEY (`policy_id`);",
            "ALTER TABLE `tbl_review`
                ADD PRIMARY KEY (`review_id`);",
            "ALTER TABLE `tbl_role`
                ADD PRIMARY KEY (`role_id`);",
            "ALTER TABLE `tbl_seo`
                ADD PRIMARY KEY (`seo_id`);",
            "ALTER TABLE `tbl_subcategory`
                ADD PRIMARY KEY (`subcategory_id`),
                ADD KEY `parent` (`parent`);",
            "ALTER TABLE `tbl_submenu`
                ADD PRIMARY KEY (`submenu_id`),
                ADD KEY `Main Menu ID` (`mainmenu_id`);",
            "ALTER TABLE `tbl_team`
                ADD PRIMARY KEY (`team_id`);",
            "ALTER TABLE `tbl_terms`
                ADD PRIMARY KEY (`terms_id`);",
            "ALTER TABLE `tbl_timezone`
                ADD PRIMARY KEY (`timezone_id`);",
            "ALTER TABLE `tbl_video`
                ADD PRIMARY KEY (`video_id`);",
            "ALTER TABLE `tbl_web_data`
                ADD PRIMARY KEY (`web_data_id`);",
            "ALTER TABLE `tbl_aboutus`
                MODIFY `aboutus_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;",
            "ALTER TABLE `tbl_admin`
                MODIFY `admin_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7; ",
            "ALTER TABLE `tbl_banner`
                MODIFY `banner_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40; ",
            "ALTER TABLE `tbl_blog`
                MODIFY `blog_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6; ",
            "ALTER TABLE `tbl_category`
                MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12; ",
            "ALTER TABLE `tbl_client`
                MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10; ",
            "ALTER TABLE `tbl_contact`
                MODIFY `contact_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26; ",
            "ALTER TABLE `tbl_control`
                MODIFY `control_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2; ",
            "ALTER TABLE `tbl_control_feature`
                MODIFY `control_feature_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4; ",
            "ALTER TABLE `tbl_email_subscriber`
                MODIFY `email_subscriber_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9; ",
            "ALTER TABLE `tbl_feedback`
                MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2; ",
            "ALTER TABLE `tbl_gallery`
                MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18; ",
            "ALTER TABLE `tbl_inquiry`
                MODIFY `inquiry_id` int(12) NOT NULL AUTO_INCREMENT; ",
            "ALTER TABLE `tbl_mainmenu`
                MODIFY `mainmenu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11; ",
            "ALTER TABLE `tbl_menu`
                MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20; ",
            "ALTER TABLE `tbl_permission`
                MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32; ",
            "ALTER TABLE `tbl_permission_assign`
                MODIFY `permission_assigned_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284; ",
            "ALTER TABLE `tbl_policy`
                MODIFY `policy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3; ",
            "ALTER TABLE `tbl_review`
                MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18; ",
            "ALTER TABLE `tbl_role`
                MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10; ",
            "ALTER TABLE `tbl_seo`
                MODIFY `seo_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33; ",
            "ALTER TABLE `tbl_subcategory`
                MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT; ",
            "ALTER TABLE `tbl_submenu`
                MODIFY `submenu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6; ",
            "ALTER TABLE `tbl_team`
                MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9; ",
            "ALTER TABLE `tbl_terms`
                MODIFY `terms_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4; ",
            "ALTER TABLE `tbl_timezone`
                MODIFY `timezone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12; ",
            "ALTER TABLE `tbl_video`
                MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9; ",
            "ALTER TABLE `tbl_web_data`
                MODIFY `web_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2; ",
            "ALTER TABLE `tbl_subcategory`
                ADD CONSTRAINT `maincategory` FOREIGN KEY (`parent`) REFERENCES `tbl_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE; ",
            "ALTER TABLE `tbl_submenu`
                ADD CONSTRAINT `Main Menu ID` FOREIGN KEY (`mainmenu_id`) REFERENCES `tbl_mainmenu` (`mainmenu_id`) ON DELETE CASCADE ON UPDATE CASCADE;",
            "INSERT INTO `tbl_admin` (`admin_id`, `user_role_id`, `admin_name`, `email`, `phone`, `password`, `path`, `address`, `bio`, `theme`, `lastseen`, `register_date`, `status`) VALUES
                (1, 1, 'super admin', 'nishantthummar005@gmail.com', '+91123456789', '7eafa30109230865c2bbf7139d086a9879ff2668230c679cfaa1eb0a4f95b6420509d0a06fc64767cdfc6a950dfd1ce951666bd1d388fd3ddde04df59222d2cbHm9HETnADcFlumuWTBTrSP/NOgC1b3fR64+N5kkcGwE=', '', 'Demo address.', '<p><span style=\"color: rgb(108, 117, 125); font-family: Nunito, \" segoe=\"\" ui\",=\"\" arial;\"=\"\">Hello there, ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo<b>re et dolore m</b>agna aliqua. Ut enim ad minim veniam, quis nostr</span><span style=\"color: rgb(108, 117, 125); font-family: Nunito, \" segoe=\"\" ui\",=\"\" arial;=\"\" background-color:=\"\" transparent;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua<b>. Ut enim ad</b> minim veniam, quis nostr</span><br></p>', 'theme-1', '2022-10-01 10:23:58', '2022-09-19', 1),
                (2, 2, 'admin', 'admin@gmail.com', '8812456780', '7eafa30109230865c2bbf7139d086a9879ff2668230c679cfaa1eb0a4f95b6420509d0a06fc64767cdfc6a950dfd1ce951666bd1d388fd3ddde04df59222d2cbHm9HETnADcFlumuWTBTrSP/NOgC1b3fR64+N5kkcGwE=', '', 'Demo Address.', '<p>Hello I am admin</p>', 'theme-3', '2022-09-30 06:31:10', '2022-09-19', 1);",
            "INSERT INTO `tbl_web_data` (`web_data_id`, `project_name`, `email_address`, `phone`, `facebook`, `instagram`, `twitter`, `linkedin`, `youtube`, `whatsapp`, `office`, `address`, `map`, `logo`, `favicon`, `footer`, `mail_smtp_host`, `mail_protocol`, `mail_port`, `mail_email`, `mail_password`, `forgot_mail_subject`, `forgot_mail_message`, `welcome_mail_subject`, `welcome_mail_message`, `inquiry_mail_subject`, `inquiry_mail_message`, `active_mail_subject`, `active_mail_message`, `inactive_mail_subject`, `inactive_mail_message`, `captcha_site_key`, `captcha_secret_key`, `captcha_visibility`, `date_format`, `time_format`, `timezone`, `meta_keyword`, `meta_desc`) VALUES
                (1, 'Enzo Admin Pro', 'nishantthummar005@gmail.com,admin@gmail.com', '88888888123,7894561230', 'www.fb.com', 'www.ins.com', 'www.tw.com', 'www.link.com', 'www.yt.com', '91234657980', '789456130', 'demo address', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d238132.63727384948!2d72.68220738859046!3d21.15946270544973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04e59411d1563%3A0xfe4558290938b042!2sSurat%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1664542208828!5m2!1sen!2sin\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>', './admin_asset/logo/e50ca5c8370bd95c00f29581b67c7412.png', './admin_asset/favicon/5acb6d8844e5e844932e09af8c02508f.png', '', 'smtp', 'ssmtp', 465, 'demo@gmail.com', 'demo123', 'Recover your password - Enzo Admin', '<p>Hello user,</p><p>Click on below link to reset your current password.</p><p><br></p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Register Success - Welcome to Enzo Admin ', '<p>Hello user,</p><p>Firstly Thank you for register with us. You have successfully created account in our admin. You can access all the granted functionality in your account. If you want any further help regarding account, drop your issues on admin mail</p><p>Thanks & Regards,</p><p><b><i>Enzo Admin.</i></b></p>', 'Inquiry Submitted - Enzo Admin ', '<p>Hello user,</p><p>We have received your inquiry and we look into it very soon.Â </p><p><br></p><p>Thanks & Regards.</p><p><b><i>Enzo Admin.</i></b></p>', 'Profile Activated Successfully - Enzo Admin', '<p>Hello user,</p><p>Congratulations, Your Enzo admin <b>profile is now activated successfully</b>. You are eligible to access our fully functional admin panel.</p><p><br></p><p>Thanks & Regards</p><p><b><i>Enzo Admin.</i></b></p>', 'Your Profile Inactivated - Enzo Admin', '<p>Hello user,</p><p>Your Enzo admin&nbsp;<span style=\"font-weight: bolder;\">profile is now inactivated due to some reason</span>. You are not eligible to access our fully functional admin panel. For more information contact to our admin or team.</p><p><br></p><p>Thanks &amp; Regards</p><p><span style=\"font-weight: bolder;\"><i>Enzo Admin.</i></span></p>', '6Lf98OYiAAAAAMt0C9ugKrgT7HH5dqMGwWGVrpHo', '6Lf98OYiAAAAABxfCbRLcbYaq3a_GSB938iXaiQg', 1, 'd-M-Y', 'h', '8', 'enzo admin, php admin panel, codeigniter admin, user role permission admin panel, fully function admin, react admin, best admin panel, free admin panel, enzo admin pro panel', 'You are accessing The Enzo Pro Admin Panel, which give you best admin experience with advanced functionality and features; developed by Nishant Thummar.');",
            "INSERT INTO `tbl_timezone` (`timezone_id`, `identifier`, `timezone`) VALUES
                (1, 'Africa', 'Africa/Abidjan'),
                (2, 'Africa', 'Africa/Accra'),
                (3, 'Asia', 'Asia/Aden'),
                (4, 'Asia', 'Asia/Dhaka'),
                (5, 'Asia', 'Asia/Almaty'),
                (6, 'Asia', 'Asia/Karachi'),
                (7, 'Asia', 'Asia/Kathmandu'),
                (8, 'Asia', 'Asia/Kolkata'),
                (9, 'Asia', 'Asia/Shanghai'),
                (10, 'Asia', 'Asia/Tokyo'),
                (11, 'Asia', 'Asia/Bangkok');",
            "INSERT INTO `tbl_control_feature` (`control_feature_id`, `general`, `social`, `email`, `security`, `system`, `captcha`, `theme`, `payment`, `language`, `backup`) VALUES
                (1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
                (2, 'fas fa-cog', 'fas fa-search', 'fas fa-envelope', 'fas fa-lock', 'fas fa-power-off', 'fas fa-sync-alt', 'fas fa-palette', 'far fa-credit-card', 'fas fa-language', 'fas fa-database'),
                (3, 'General settings such as, site title, site description, address and so on.', 'Search engine optimization settings, such as meta tags and social media.', 'Email SMTP settings, notifications and others related to email.', 'Security settings such as change admin login password and others.', 'PHP version settings, time zones and other environments.', 'Google Captcha Security settings for contact, inquiry and other form.', 'Change admin theme and other appearance changes as per your choice.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Change payment gateway setting such as razorpay, paypal, paytm, stripe, etc.', 'Set the backup settings below');",
            "INSERT INTO `tbl_control` (`control_id`, `aboutus`, `banner`, `blog`, `category`, `client`, `email_subscriber`, `contact`, `feedback`, `gallery`, `inquiry`, `mainmenu`, `permission`, `policy`, `review`, `role`, `seo`, `subcategory`, `submenu`, `terms`, `team`, `user`, `video`) VALUES
                (1, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');",
            "INSERT INTO `tbl_role` (`role_id`, `title`, `remark`) VALUES
                (1, 'super admin', 'Super admin has all rights.'),
                (2, 'admin', 'admin role'),
                (3, 'user', 'user role');",
            "INSERT INTO `tbl_permission` (`permission_id`,`menu` ,`type`, `feature`, `all`, `read`, `write`, `edit`, `delete`,`status`) VALUES
                (1, 'site pages', 'page', 'contact', '0', '1', '0', '0', '1','0'),
                (2, 'site pages', 'page', 'aboutus', '1', '1', '1', '1', '1','0'),
                (3, 'site pages', 'page', 'banner', '1', '1', '1', '1', '1','0'),
                (4, 'site pages', 'page', 'gallery', '1', '1', '1', '1', '1','0'),
                (5, 'site pages', 'page', 'client', '1', '1', '1', '1', '1','0'),
                (6, 'site pages', 'page', 'terms', '1', '1', '1', '1', '1','0'),
                (7, 'site pages', 'page', 'policy', '1', '1', '1', '1', '1','0'),
                (8, 'site pages', 'page', 'review', '1', '1', '1', '1', '1','0'),
                (9, 'site pages', 'page', 'blog', '1', '1', '1', '1', '1','0'),
                (10, 'site pages', 'page', 'feedback', '0', '1', '0', '0', '1','0'),
                (11, 'site pages', 'page', 'team', '1', '1', '1', '1', '1','0'),
                (12, 'site pages', 'page', 'video', '1', '1', '1', '1', '1','0'),
                (13, 'site pages', 'page', 'inquiry', '0', '1', '0', '0', '1','0'),
                (14, 'site pages', 'page', 'seo', '1', '1', '1', '1', '1','0'),
                (15, 'site pages', 'page', 'category', '1', '1', '1', '1', '1','0'),
                (16, 'site pages', 'page', 'subcategory', '1', '1', '1', '1', '1','0'),
                (17, 'site pages', 'page', 'user', '1', '1', '1', '1', '1','1'),
                (18, 'site pages', 'page', 'role', '1', '1', '1', '1', '1','0'),
                (19, 'site pages', 'page', 'permission', '1', '1', '1', '1', '1','0'),
                (20, 'site pages', 'page', 'mainmenu', '1', '1', '1', '1', '1','0'),
                (21, 'site pages', 'page', 'submenu', '1', '1', '1', '1', '1','0'),
                (22, '','feature', 'general', '1', '1', '0', '1', '0','0'),
                (23, '','feature', 'social', '1', '1', '0', '1', '0','0'),
                (24, '','feature', 'email', '1', '1', '0', '1', '0','0'),
                (25, '','feature', 'security', '1', '1', '0', '1', '0','0'),
                (26, '','feature', 'system', '1', '1', '0', '1', '0','0'),
                (27, '','feature', 'captcha', '1', '1', '0', '1', '0','0'),
                (28, '','feature', 'theme', '1', '1', '0', '1', '0','0'),
                (29, '','feature', 'payment', '1', '1', '0', '1', '0','0'),
                (30, '','feature', 'language', '1', '1', '0', '1', '0','0'),
                (31, '','feature', 'backup', '1', '1', '0', '1', '0','0');",
            "INSERT INTO `tbl_permission_assign` (`permission_assigned_id`, `role_id`, `permission_id`, `type`, `all`, `read`, `write`, `edit`, `delete`) VALUES
                (139, 1, '[31,30,29,28,27,26,25,24,23,22]', 'feature', '1', '1', '1', '1', '1'),
                (140, 1, '[31,30,29,28,27,26,25,24,23,22]', 'feature', '0', '1', '0', '0', '0'),
                (141, 1, '[31,30,29,28,27,26,25,24,23,22]', 'feature', '0', '0', '0', '1', '0'),
                (142, 1, '[19,18,17,16,15,14,12,11,9,8,7,6,5,4,3,2]', 'page', '1', '1', '1', '1', '1'),
                (143, 1, '[19,18,17,16,15,14,12,11,9,8,7,6,5,4,3,2,1]', 'page', '0', '1', '0', '0', '0'),
                (144, 1, '[19,18,17,16,15,14,12,11,9,8,7,6,5,4,3,2]', 'page', '0', '0', '1', '0', '0'),
                (145, 1, '[19,18,17,16,15,14,12,11,9,8,7,6,5,4,3,2]', 'page', '0', '0', '0', '1', '0'),
                (146, 1, '[19,18,17,16,15,14,12,11,9,8,7,6,5,4,3,2]', 'page', '0', '0', '0', '0', '1'),
                (244, 2, '[28,27,26,25,24]', 'feature', '1', '1', '1', '1', '1'),
                (245, 2, '[28,27,26,25]', 'feature', '0', '1', '0', '0', '0'),
                (246, 2, '[28,27,26,25]', 'feature', '0', '0', '0', '1', '0'),
                (247, 2, '[4,2]', 'page', '1', '1', '1', '1', '1'),
                (248, 2, '[21,20,17,9,8,6,4,3,2,1]', 'page', '0', '1', '0', '0', '0'),
                (249, 2, '[4,2]', 'page', '0', '0', '1', '0', '0'),
                (250, 2, '[4,2]', 'page', '0', '0', '0', '1', '0'),
                (251, 2, '[4,2]', 'page', '0', '0', '0', '0', '1');",
            "INSERT INTO `tbl_aboutus` (`aboutus_id`, `about`) VALUES
                (2, '<p style=\"margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: Swansea, sans-serif; font-size: 18px;\">Enzo Admin Pro,<span style=\"font-weight: normal;\"> we develop innovative and creative products and services.php that provide total communication and information solutions. Among a plethora of services.php, web design and development, tailor made applications, ERPs, CRMs, e-commerce solutions, business-to-business applications, business-to-client applications, managed hosting and internet portal management are few that we offer. Satisfied clients around the globe bear testimony to the quality of our work.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Swansea, sans-serif; font-size: 18px;\">As a leader in technology exploring Enzo Admin Pro, is committed to exporting quality software worldwide.</p>');",
            "INSERT INTO `tbl_banner` (`banner_id`, `title`, `subtitle`, `path`) VALUES
                (40, 'First Banner', 'This is demo banner which shows on home page slider', './admin_asset/banner/a89ba2f7b7ebeef93cae77e751da9aa7.jpg');",
            "INSERT INTO `tbl_blog` (`blog_id`, `title`, `path`, `description`, `meta_title`, `meta_keyword`, `meta_desc`, `blogdate`) VALUES
                (6, 'Application Design & Development', './admin_asset/blog/a36106103c6b0f62754e0d840d523386.jpg', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Swansea, sans-serif; font-size: 18px;\">Made wherein kind. Fruit dominion for fowl and together bring man created abundantly were likeness be days and winged darkness creepeth divide firmament subdue yielding itself seed fruit he Blessed.&nbsp;Darkness they\'re, moved whales great or void great man one man from blessed so our good face was fruitful sixth male kind.</p><div class=\"vlt-gap-40\" style=\"height: 40px; color: rgb(82, 82, 82); font-family: Swansea, sans-serif; font-size: 18px;\"></div><div class=\"has-black-color\" style=\"color: rgb(16, 16, 16); font-family: Swansea, sans-serif; font-size: 18px;\"><ul class=\"vlt-styled-list vlt-styled-list--style-3\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style-type: none;\"><li style=\"font-size: 13px; font-weight: 700; line-height: 1.2; letter-spacing: 2px; text-transform: uppercase; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; flex-direction: row;\">PRODUCT DESIGN</li><li style=\"font-size: 13px; font-weight: 700; line-height: 1.2; letter-spacing: 2px; text-transform: uppercase; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; flex-direction: row; margin-top: 22px;\">BRAND DESIGN</li><li style=\"font-size: 13px; font-weight: 700; line-height: 1.2; letter-spacing: 2px; text-transform: uppercase; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; flex-direction: row; margin-top: 22px;\">WEB &amp; MOBILE DESIGN</li><li style=\"font-size: 13px; font-weight: 700; line-height: 1.2; letter-spacing: 2px; text-transform: uppercase; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; flex-direction: row; margin-top: 22px;\"><br></li></ul></div>', 'Application Design & Development', 'application design, website development, website design', 'Made wherein kind. Fruit dominion for fowl and together bring man created abundantly were likeness be days and winged darkness creepeth divide firmament subdue yielding itself seed fruit he Blessed. ', '2022-10-06');",
            "INSERT INTO `tbl_category` (`category_id`, `title`, `path`) VALUES
                (13, 'Electronic', './admin_asset/category/6e72b239445df31c1e33184617b22fc5.png'),
                (14, 'Garments', './admin_asset/category/18dbe74ac8fb95356332d7b341d024e3.jpg');",
            "INSERT INTO `tbl_client` (`client_id`, `title`, `path`) VALUES
                (10, 'Demo Client', './admin_asset/client/46fa3ab5f0eb5af7f7af9bafbf83c0fb.png');",
            "INSERT INTO `tbl_email_subscriber` (`email_subscriber_id`, `email`, `datetime`) VALUES
                (9, 'example1@mail.com', '2022-10-06 12:50:35'),
                (10, 'example2@mail.com', '2022-10-06 12:50:35');",
            "INSERT INTO `tbl_gallery` (`gallery_id`, `category`, `title`, `path`) VALUES
                (18, '', 'Demo Photo', './admin_asset/gallery/15888a24ed7e8a6530630de256369832.jpg');",
            "INSERT INTO `tbl_mainmenu` (`mainmenu_id`, `title`, `slug`, `position`, `icon`,`controller`, `url`, `status`) VALUES
                (11, 'Demo Main Menu', 'admin-demomain', 1, 'fab fa-pagelines', 'demo','admin-demomain', 1);",
            "INSERT INTO `tbl_review` (`review_id`, `username`, `review`, `datetime`, `path`) VALUES
                (18, 'Demo User', 'â€œ A review is an evaluation of a publication, service, or company such as a movie, video game, musical composition, book; a piece of hardware like a car, home appliance, or computer. â€�', '2022-10-06 00:00:00', './admin_asset/review/1e3d062c91098246bc11bfb2f02a88fa.png');",
            "INSERT INTO `tbl_seo` (`seo_id`, `page`, `title`, `description`, `keyword`) VALUES
                (33, 'index', 'Enzo Admin Pro - Complete User Management System', 'You are accessing The Enzo Pro Admin Panel, which give you best admin experience with advanced functionality and features.', 'enzo admin, php admin panel, codeigniter admin, user role permission admin panel, fully function admin, react admin, best admin panel, free admin panel, enzo admin pro panel');",
            "INSERT INTO `tbl_subcategory` (`subcategory_id`, `title`, `parent`) VALUES
                (1, 'Female Outfit', 14),
                (2, 'Mobile', 13),
                (3, 'Television', 13);",
            "INSERT INTO `tbl_submenu` (`submenu_id`, `mainmenu_id`, `title`, `slug`, `position`, `icon`, `url`, `status`) VALUES
                (6, 11, 'Demo Sub Menu', 'admin-subdemo', 1, 'fas fa-air-freshener', 'admin-subdemo', 0),
                (7, 11, 'Sub Menu Demo', 'admin-subdemo2', 2, 'fab fa-affiliatetheme', 'admin-subdemo2', 1);",
            "INSERT INTO `tbl_team` (`team_id`, `name`, `path`, `facebook`, `instagram`, `email`, `phone`, `linkedin`, `twitter`) VALUES
                (9, 'Demo User', './admin_asset/team/fb1973a4c4376ac6528f6eb0d51ef998.png', 'https://www.fb.com', 'https://www.instagram.com', 'demo@mail.com', '7894561230', 'https://www.linkedin.com', 'https://www.twitter.com');",
            "INSERT INTO `tbl_video` (`video_id`, `video`, `title`) VALUES
                (9, 'https://youtu.be/JeVda9T8_JM', 'Demo Youtube Video');"
        );
        foreach ($queries as $table) {
            if (!$this->db->query($table)) {
                die("Error in your Queries");
            }
        }
        $this->load->view('installer/completed');
    }

    public function insertSetting($setting_array) {
        $file = __DIR__ . "/installer.json";
        return file_put_contents($file, json_encode($setting_array));    // store data in installer.json file
    }

    public function index() {
        /*
          $dirs = explode(DIRECTORY_SEPARATOR, __DIR__);
          var_dump($dirs);
          return 0; */
        $data['settings'] = INSTALLER_SETTING;

        // Check if database already connected or not [STATUS = PRE/POST] 
        if ($data['settings']['status'] != "pre") {
            redirect('/authorize');
        }

        $this->form_validation->set_rules("database_name", "Database Name", "required");
        //$this->form_validation->set_rules("database_password","Database Password","required");
        $this->form_validation->set_rules("database_host", "Database Host", "required");
        $this->form_validation->set_rules("database_username", "Database UserName", "required");
        $this->form_validation->set_rules("url", "Link to your root/Domain", "required");
        if (!$this->form_validation->run()) {
            $this->load->view('installer/index', $data);
        } else {
            $servername = $this->input->post('database_host');
            $username = $this->input->post('database_username');
            $password = $this->input->post('database_password');

            // Create connection
            $conn = new mysqli($servername, $username, $password);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            // Create database
            $sql = "CREATE DATABASE IF NOT EXISTS " . $this->input->post('database_name');
            if ($conn->query($sql) === TRUE) {
                //echo "Database created successfully";
                $data['settings']['status'] = "post";
                $data['settings']['base_url'] = $this->input->post('url');
                $data['settings']['password'] = $this->input->post('database_password');
                $data['settings']['hostname'] = $this->input->post('database_host');
                $data['settings']['username'] = $this->input->post('database_username');
                $data['settings']['database'] = $this->input->post('database_name');

                $this->insertSetting($data['settings']);    // store data in installer.json file
                redirect('/Install/next');
            } else {
                echo "Error creating database: " . $conn->error;
            }
            $conn->close();
        }
    }

}
