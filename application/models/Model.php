<?php

/* * *
 * Project:    Enzo Admin Pro
 * Name:       Model
 * Package:    Model.php
 * About:      A model that handle all CI_Model queries, send email, file upload and other stuff.
 * Copyright:  (C) 2022
 * Author:     Nishant Thummar
 * Website:    https://nishantthummar.in/
 * * */

class Model extends CI_Model
{

    // Global Variables
    public $savepath = './admin_asset/';

    public function insert($tbl, $dt)
    {
        return $this->db->insert($tbl, $dt);
    }

    public function select_field($tbl)
    {
        $result = $this->db->list_fields($tbl);
        foreach ($result as $field) {
            $data[] = $field;
        }
        return $data;
    }

    public function select($tbl)
    {
        $field = substr($tbl, 4) . "_id";
        if ($this->db->field_exists($field, $tbl)) {
            $this->db->order_by($field, "desc");
        }
        $data = $this->db->get($tbl);
        return $data->result();
    }

    public function select_where($tbl, $wh)
    {
        $field = substr($tbl, 4) . "_id";
        if ($this->db->field_exists($field, $tbl)) {
            $this->db->order_by($field, "desc");
        }
        $data = $this->db->get_where($tbl, $wh);
        return $data->result();
    }

    public function select_where_order($tbl, $wh, $order)
    {
        $data = $this->db->get_where($tbl, $wh);
        $this->db->order_by($order);
        return $data->result();
    }

    public function select_join($tbl, $join, $onjoin)
    {
        $joink = array_keys($join);
        $joinv = array_values($join);

        $count = count($join);
        $this->db->select('*');
        $this->db->from($tbl);

        for ($i = 0; $i < $count; $i++) {
            $this->db->join($joink[$i], $joink[$i] . "." . $joinv[$i] . "=" . $tbl . "." . $onjoin[$i]);
        }
        //$this->db->join('comments', 'comments.id = blogs.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function select_join_order($tbl, $join, $onjoin, $order)
    {
        $joink = array_keys($join);
        $joinv = array_values($join);

        $count = count($join);
        $this->db->select('*');
        $this->db->from($tbl);

        for ($i = 0; $i < $count; $i++) {
            $this->db->join($joink[$i], $joink[$i] . "." . $joinv[$i] . "=" . $tbl . "." . $onjoin[$i]);
        }
        //$this->db->join('comments', 'comments.id = blogs.id');
        $this->db->order_by($order);
        $query = $this->db->get();
        return $query->result();
    }

    public function select_max($tbl, $id)
    {
        $this->db->select_max($id, 'id');
        $this->db->from($tbl);
        $query = $this->db->get();
        return $query->row();
    }

    public function select_max_where($tbl, $id, $wh)
    {
        $this->db->select_max($id, 'id');
        $query = $this->db->get_where($tbl, $wh);
        return $query->row();
    }

    public function select_sum($tbl, $id)
    {
        $this->db->select_sum($id, 'id');
        $this->db->from($tbl);
        $query = $this->db->get();
        return $query->row();
    }

    public function select_sum_where($tbl, $id, $wh)
    {
        $this->db->select_sum($id, 'id');
        $query = $this->db->get_where($tbl, $wh);
        return $query->row();
    }

    public function select_limit_order($tbl, $limit, $id = null, $order = null, $wh = null)
    {
        $this->db->order_by($id, $order);
        $this->db->limit($limit);
        if ($wh) {
            $query = $this->db->get_where($tbl, $wh);
        } else {
            $query = $this->db->get($tbl);
        }

        return $query->result();
    }

    public function delete($tbl, $wh)
    {
        return $this->db->delete($tbl, $wh);
    }

    public function update($tbl, $data, $wh)
    {
        return $this->db->update($tbl, $data, $wh);
    }

    public function my_query($q)
    {
        return $this->db->query($q);
    }


    // SEL Friendly URL
    public function generateSeoURL($string, $wordLimit = 0)
    {
        $separator = '-';

        if ($wordLimit != 0) {
            $wordArr = explode(' ', $string);
            $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
        }

        $quoteSeparator = preg_quote($separator, '#');

        $trans = array(
            '&.+?;' => '',
            '[^\w\d _-]' => '',
            '\s+' => $separator,
            '(' . $quoteSeparator . ')+' => $separator
        );

        $string = strip_tags($string);
        foreach ($trans as $key => $val) {
            $string = preg_replace('#' . $key . '#iu', $val, $string);
        }

        $string = strtolower($string);

        return trim(trim($string, $separator));
    }

    // Import Data
    public function insert_import($tbl, $data)
    {
        $res = $this->db->insert_batch($tbl, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // Validate Google Captcha
    public function validateCaptcha($captcha_response, $keySecret)
    {
        $response = false;
        $captcha_response = $captcha_response;
        if ($captcha_response != '') {
            $check = array(
                'secret' => $keySecret,
                'response' => $captcha_response
            );
            $startProcess = curl_init();
            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($startProcess, CURLOPT_POST, true);
            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
            $receiveData = curl_exec($startProcess);
            $finalResponse = json_decode($receiveData, true);
            if ($finalResponse['success']) {
                $response = true;
            } else {
                $response = false;
            }
        } else {
            $response = false;
        }
        return $response;
    }

    public function verify_recaptcha($res)
    {
        $insert_status = false;
        if ($this->web_data[0]->captcha_visibility):
            $captcha_response = $this->validateCaptcha($res, $this->web_data[0]->captcha_secret_key);
            if ($captcha_response):
                $insert_status = true;
            else:
                $insert_status = false;
            endif;
        else:
            $insert_status = true;
        endif;
        return $insert_status;
    }

    // Send mail
    public function sendMail($protocol, $host, $sender, $password, $port, $subject, $body, $receiver)
    {
        $config = array(
            'protocol' => $protocol,
            'smtp_host' => $host,
            'smtp_user' => $sender,
            'smtp_pass' => $password,
            'smtp_port' => $port,
            'mailtype' => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email', $config);
        $this->email->from($sender, WEBSITE);
        $this->email->to($receiver);
        $this->email->subject($subject);
        $this->email->message($body);
        return ($this->email->send()) ? true : false;
    }

    // common file upload 
    public function uploadFile($type, $file_type = null)
    {
        $config['upload_path'] = $this->savepath . $type . '/';
        if (!is_dir($config['upload_path']))
            mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists
        $config['allowed_types'] = (!$file_type ? 'jpg|png|jpeg|gif|pdf|webp' : 'pdf');
        $config['file_name'] = time();
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 1024 * 8;
        /* $config['max_width'] = 1920;
          $config['max_height'] = 1080;
          $config['min_width'] = 1920;
          $config['min_height'] = 1080; */
        $this->upload->initialize($config);
        if ($this->upload->do_upload($type)) {
            return $this->savepath . $type . '/' . $this->upload->data('file_name');
        } else {
            return FILENOTFOUND;
        }
    }

    // common Multi file upload 
    public function uploadMultiFile($type, $count, $file_type = null)
    {
        $upload_status = false;
        for ($i = 0; $i < $count; $i++) {
            $_FILES['single']['name'] = $_FILES[$type]['name'][$i];
            $_FILES['single']['type'] = $_FILES[$type]['type'][$i];
            $_FILES['single']['size'] = $_FILES[$type]['size'][$i];
            $_FILES['single']['error'] = $_FILES[$type]['error'][$i];
            $_FILES['single']['tmp_name'] = $_FILES[$type]['tmp_name'][$i];

            $config['upload_path'] = $this->savepath . $type . '/';
            if (!is_dir($config['upload_path']))
                mkdir($config['upload_path'], 0777, TRUE); // Make a folder if not exists 
            $config['allowed_types'] = (!$file_type ? 'jpg|png|jpeg|gif|webp' : 'pdf');
            $config['file_name'] = time();
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = 1024 * 12;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('single'))
                $upload_status = true;
            else
                $data['error'] = 'Oops,' . $this->upload->display_errors();
            $path[] = $this->savepath . $type . '/' . $this->upload->data('file_name');
        }
        return $upload_status ? implode(",", $path) : FILENOTFOUND;
    }

    // Get Website Data
    public function getWebData()
    {
        return $this->md->select('tbl_web_data');
    }

    // Get Item Name from Item ID - Common function for all the table
    public function getItemName($tbl, $whField, $field, $id)
    {
        if ($id):
            $get_name = $this->db->query("SELECT `$field` as name FROM `$tbl` WHERE `$whField` = $id")->result();
            return ($get_name) ? ucfirst($get_name[0]->name) : '<mark>Data not found!</mark>';
        else:
            return '<mark>Data not found!</mark>';
        endif;
    }

}
