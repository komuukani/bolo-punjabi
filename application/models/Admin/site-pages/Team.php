<?php

class Team extends CI_Model {

    // Global Variables
    public $tbl = 'tbl_team';

    // get data from table set in datatable
    function getData($postData = null) {

        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (name like '%" . $searchValue . "%' or phone like '%" . $searchValue . "%' or email like '%" . $searchValue . "%') ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("team_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();

        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "name" => $record->name,
                "email" => $record->email,
                "phone" => $record->phone,
                "facebook" => '<a href="' . $record->facebook . '" target="_blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Visit Link" > <i class="far fa-paper-plane"></i></a>',
                "instagram" => '<a href="' . $record->instagram . '" target="_blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Visit Link" > <i class="far fa-paper-plane"></i> </a>',
                "linkedin" => '<a href="' . $record->linkedin . '" target="_blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Visit Link" > <i class="far fa-paper-plane"></i></a>',
                "twitter" => '<a href="' . $record->twitter . '" target="_blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Visit Link" > <i class="far fa-paper-plane"></i></a>',
                "path" => '<a href="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="" data-original-title="Show Full Category"><img src="' . base_url($record->path ? $record->path : FILENOTFOUND) . '" width="40" height="40" style="object-fit:contain" /></a>',
                "edit" => '<a href="' . base_url('manage-team/edit/' . $record->team_id) . '"style="color: #003eff" data-toggle="tooltip" data-placement="bottom" data-original-title="Update Data"><i class="far fa-edit" ></i></a> &nbsp; &nbsp; ',
                "delete" => '<a id="deleteItem" data-itemid="' . $record->team_id . '"  class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom"  data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
