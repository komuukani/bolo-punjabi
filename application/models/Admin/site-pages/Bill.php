<?php

class Bill extends CI_Model
{

    // Global Variables
    public $tbl = 'tbl_bill';

    // get data from table set in datatable
    function getData($postData = null)
    {

        $webData = $this->md->getWebData(); // Get Website Data
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (name like '%" . $searchValue . "%' or email like '%" . $searchValue . "%' or phone like'%" . $searchValue . "%' or message like'%" . $searchValue . "%' or subject like'%" . $searchValue . "%' ) ";
        }

        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->tbl)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->tbl)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('*');
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by("bill_id", "desc");
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->tbl)->result();
        $data = array();
        foreach ($records as $record) {
            $data[] = array(
                "order_id" => $record->order_id,
                "transaction_id" => $record->transaction_id,
                "type" => $record->user_type,
                "name" => $record->fname,
                "email" => $record->email,
                "phone" => $record->phone,
                "netprice" => $record->netprice,
                "country" => $record->country,
                "state" => $record->state,
                "city" => $record->city,
                "address" => $record->address,
                "notes" => $record->notes,
                "invoice" => '<a href="' . base_url('generate-report/view/' . $record->transaction_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Show Invoice" data-original-title="Show Invoice" class="btn btn-primary btn-sm">Show Invoice</a>',
                "download" => '<a href="' . base_url('generate-report/download/' . $record->transaction_id) . '" target="_blank" data-toggle="tooltip" data-placement="right" title="Download Invoice" data-original-title="Download Invoice" class="btn btn-info btn-sm">Download</a>',
                "datetime" => date(($webData ? $webData[0]->date_format : 'd-M-Y'), strtotime($record->entry_date)),
                "delete" => '<a id="deleteItem" data-itemid="' . $record->bill_id . '" class="text-danger cursor-pointer" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove Data"><i class="far fa-trash-alt" ></i></a>'
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

}
