<?php
echo $page_head;
?>
<body>
<div class="main-wrapper">
<?php echo $page_header; ?>
<?php echo $page_breadcumb; ?>
<div class=" eduvibe-home-four-about edu-about-area about-style-2 edu-section-gap bg-color-white">
    <div class="container eduvibe-animated-shape">
        <div class="row row--50">
            <div class="col-lg-6">
                <div class="about-image-gallery">
                    <div class="eduvibe-about-1-img-wrapper">
                        <img class="image-1" src="assets/images/about/about-07/about-image-01.png" alt="About Images" />
                        <span class="eduvibe-about-blur"><img src="assets/images/about/about-07/about-blur.png" alt="About Blur" /></span>
                    </div>
                    <div class="circle-image-wrapper">
                        <img class="image-2" src="assets/images/about/about-07/about-image-02.png" alt="About Images" />
                        <div class="circle-image"><span></span></div>
                    </div>
                    <div class="finished-session">
                        <div class="inner">
                            <div class="text">2.98</div>
                            <span class="finished-title">
                                Finished <br />
                                Sessions
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="inner mt_md--40 mt_sm--40">
                    <div class="section-title" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <span class="pre-title">About Us</span>
                        <h3 class="title">Knowledge is power, Information is liberating.</h3>
                    </div>
                    <p class="description mt--40 mt_md--20 mt_sm--20" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet, venenatis dictum et nec. Fringilla dictum tristique cras pellentesque consequat.</p>
                    <h6 class="subtitle mb--20" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">People Love To Learn With Us</h6>
                    <div class="about-feature-list">
                        <div class="row g-5">
                            <!-- Start Single Feature  -->
                            <div class="col-lg-6" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="feature-style-3">
                                    <div class="feature-content">
                                        <h6 class="feature-title">90%</h6>
                                        <p class="feature-description">90% of students see their course through to completion.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Feature  -->

                            <!-- Start Single Feature  -->
                            <div class="col-lg-6" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="feature-style-3">
                                    <div class="feature-content">
                                        <h6 class="feature-title">9/10</h6>
                                        <p class="feature-description">9/10 users reported better learning outcomes.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Feature  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-11-05.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-08-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-3">
                <img src="assets/images/shapes/shape-30.png" alt="Shape Thumb" />
            </div>
            <div class="shape shape-1"><span class="shape-dot"></span></div>
        </div>
    </div>
</div>

<div class="eduvibe-about-us-one-service edu-service-area mb-30 bg-color-white service-bg-position">
    <div class="container eduvibe-animated-shape">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <span class="pre-title">What We Offer</span>
                    <h3 class="title">Learn New Skills When And <br /> Where You Like</h3>
                </div>
            </div>
        </div>
        <div class="row g-5 mt--20">

            <!-- Start Service Grid  -->
            <div class="col-lg-3 col-md-6 col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                <div class="service-card service-card-2 card-bg-1">
                    <div class="inner">
                        <div class="icon">
                            <a href="#">
                                <img src="assets/images/icons/offer-icon-01.png" alt="Service Images">
                            </a>
                            <div class="shape-list">
                                <img class="shape shape-1" src="assets/images/icons/service-icon-01.png" alt="Shape Images">
                                <img class="shape shape-2" src="assets/images/icons/service-icon-02.png" alt="Shape Images">
                                <img class="shape shape-3" src="assets/images/icons/service-icon-03.png" alt="Shape Images">
                            </div>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#">Remote Learning</a></h6>
                            <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Service Grid  -->

            <!-- Start Service Grid  -->
            <div class="col-lg-3 col-md-6 col-12" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
                <div class="service-card service-card-2 card-bg-2">
                    <div class="inner">
                        <div class="icon">
                            <a href="#">
                                <img src="assets/images/icons/offer-icon-02.png" alt="Service Images">
                            </a>
                            <div class="shape-list">
                                <img class="shape shape-1" src="assets/images/icons/service-icon-01.png" alt="Shape Images">
                                <img class="shape shape-2" src="assets/images/icons/service-icon-02.png" alt="Shape Images">
                                <img class="shape shape-3" src="assets/images/icons/service-icon-03.png" alt="Shape Images">
                            </div>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#">Awesome Tutors</a></h6>
                            <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Service Grid  -->

            <!-- Start Service Grid  -->
            <div class="col-lg-3 col-md-6 col-12" data-sal-delay="250" data-sal="slide-up" data-sal-duration="800">
                <div class="service-card service-card-2 card-bg-3">
                    <div class="inner">
                        <div class="icon">
                            <a href="#">
                                <img src="assets/images/icons/offer-icon-03.png" alt="Service Images">
                            </a>
                            <div class="shape-list">
                                <img class="shape shape-1" src="assets/images/icons/service-icon-01.png" alt="Shape Images">
                                <img class="shape shape-2" src="assets/images/icons/service-icon-02.png" alt="Shape Images">
                                <img class="shape shape-3" src="assets/images/icons/service-icon-03.png" alt="Shape Images">
                            </div>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#">Global Certificate</a></h6>
                            <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Service Grid  -->

            <!-- Start Service Grid  -->
            <div class="col-lg-3 col-md-6 col-12" data-sal-delay="300" data-sal="slide-up" data-sal-duration="800">
                <div class="service-card service-card-2 card-bg-4">
                    <div class="inner">
                        <div class="icon">
                            <a href="#">
                                <img src="assets/images/icons/offer-icon-04.png" alt="Service Images">
                            </a>
                            <div class="shape-list">
                                <img class="shape shape-1" src="assets/images/icons/service-icon-01.png" alt="Shape Images">
                                <img class="shape shape-2" src="assets/images/icons/service-icon-02.png" alt="Shape Images">
                                <img class="shape shape-3" src="assets/images/icons/service-icon-03.png" alt="Shape Images">
                            </div>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#">Carrier Guideline</a></h6>
                            <p class="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Service Grid  -->
        </div>

        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-04-03.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-02-07.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-3">
                <img src="assets/images/shapes/shape-15.png" alt="Shape Thumb" />
            </div>
        </div>
    </div>
</div>

<!-- Start Video Area  -->
<div class="edu-workshop-area eduvibe-home-three-video workshop-style-1 mb-30">
    <div class="container eduvibe-animated-shape">
        <div class="row gy-lg-0 gy-5 row--60 align-items-center">
            <div class="col-lg-12 order-2 order-lg-1">
                <?php
                if (empty($aboutus)) :
                    echo "Sorry, content not available";
                else :
                    foreach ($aboutus as $key => $about_data) {
                        echo $about_data->about;
                    }
                endif;
                ?>
            </div>
        </div>
        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-09-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-04-05.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-3">
                <img src="assets/images/shapes/shape-13-02.png" alt="Shape Thumb" />
            </div>
        </div>

    </div>
</div>
<!-- End Video Area  -->

<?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>