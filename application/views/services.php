<?php
echo $page_head;
?>
<body class="theme-color2 dark ltr">
<!-- template sections -->
<?php echo $page_header; ?>
<?php $this->load->view('mobileMenu'); ?>
<?php echo $page_breadcumb; ?>

<section class="bg-smoke space">
    <div class="container">
        <div class="row gy-30">
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_1.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Electrical
                                System</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_2.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Auto
                                Car Repair</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_3.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Engine
                                Diagnostics</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_4.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Car
                                & Engine Clean</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_5.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Full
                                Car Service</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="service-item-2">
                    <div class="service-item-2_img"><img src="assets/img/update_1/service/service_3_6.jpg"
                                                         alt="service image"></div>
                    <div class="service-item-2_content"><h3 class="service-item-2_title"><a href="service-details.html">Fresh
                                Oil Input</a></h3>
                        <p class="service-item-2_text">Unique core competen resource sucking methods of empowerment
                            disciplinary deliverables after cost effective</p><a href="service-details.html"
                                                                                 class="th-btn">View Details</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="space-top" data-bg-src="assets/img/update_1/bg/service_bg_1.jpg">
    <div class="container">
        <div class="quality-card" data-pos-space=".blog-sec" data-sec-space="margin-bottom" data-margin-bottom="155px">
            <div class="title-area mb-40 text-lg-start"><span class="sub-title">Top News Updates</span>
                <h2 class="sec-title">We Provide Best Service And Repair</h2></div>
            <div class="quality-feature-wrap">
                <div class="quality-feature">
                    <div class="quality-feature_icon"><img src="assets/img/update_1/icon/quality-feature_1.svg"
                                                           alt="icon"></div>
                    <h3 class="quality-feature_title">Quality Services</h3>
                    <p class="quality-feature_text">Purpose quality vectors with highly efficient incubate</p></div>
                <div class="quality-feature">
                    <div class="quality-feature_icon"><img src="assets/img/update_1/icon/quality-feature_2.svg"
                                                           alt="icon"></div>
                    <h3 class="quality-feature_title">Fast Delivery</h3>
                    <p class="quality-feature_text">Purpose quality vectors with highly efficient incubate</p></div>
            </div>
            <div class="skill-feature style4"><h3 class="skill-feature_title">Product Design</h3>
                <div class="progress-bar" data-percentage="70%">
                    <div class="progress-content-outter">
                        <div class="progress-content"></div>
                    </div>
                </div>
            </div>
            <div class="skill-feature style4"><h3 class="skill-feature_title">Car Mechanic Service</h3>
                <div class="progress-bar" data-percentage="93%">
                    <div class="progress-content-outter">
                        <div class="progress-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="space bg-smoke blog-sec5 overflow-hidden" id="blog-sec">
    <div class="container">
        <div class="row justify-content-lg-between justify-content-center align-items-center">
            <div class="col-lg-6">
                <div class="title-area text-lg-start"><span class="sub-title">News & Updates</span>
                    <h2 class="sec-title">Latest Blog Posts</h2></div>
            </div>
            <div class="col-auto">
                <div class="sec-btn"><a href="blog.html" class="th-btn white-btn">View All Posts</a></div>
            </div>
        </div>
        <div class="row slider-shadow th-carousel" data-slide-show="3" data-lg-slide-show="2" data-md-slide-show="2"
             data-sm-slide-show="1" data-arrows="true" data-xl-arrows="true" data-ml-arrows="true">
            <div class="col-md-6 col-xl-4">
                <div class="blog-block style2">
                    <div class="blog-img"><img src="assets/img/update_1/blog/blog_2_1.jpg" alt="blog image"></div>
                    <div class="blog-content">
                        <div class="blog-meta"><a href="blog.html"><i class="fas fa-calendar-alt"></i>October 15,
                                2023</a> <a href="blog.html"><i class="fas fa-tags"></i>Test Drive</a></div>
                        <h3 class="blog-title"><a href="blog-details.html">How to Make the Most of Your Test Drive</a>
                        </h3><a href="blog-details.html" class="link-btn">Read More<i
                                class="fas fa-arrow-right"></i></a></div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="blog-block style2">
                    <div class="blog-img"><img src="assets/img/update_1/blog/blog_2_2.jpg" alt="blog image"></div>
                    <div class="blog-content">
                        <div class="blog-meta"><a href="blog.html"><i class="fas fa-calendar-alt"></i>October 16,
                                2023</a> <a href="blog.html"><i class="fas fa-tags"></i>Oil Change</a></div>
                        <h3 class="blog-title"><a href="blog-details.html">How to Jump Start Your Car Maintenance?</a>
                        </h3><a href="blog-details.html" class="link-btn">Read More<i
                                class="fas fa-arrow-right"></i></a></div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="blog-block style2">
                    <div class="blog-img"><img src="assets/img/update_1/blog/blog_2_3.jpg" alt="blog image"></div>
                    <div class="blog-content">
                        <div class="blog-meta"><a href="blog.html"><i class="fas fa-calendar-alt"></i>October 17,
                                2023</a> <a href="blog.html"><i class="fas fa-tags"></i>Car Drive</a></div>
                        <h3 class="blog-title"><a href="blog-details.html">How to Decorate Your Car for Halloween</a>
                        </h3><a href="blog-details.html" class="link-btn">Read More<i
                                class="fas fa-arrow-right"></i></a></div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="blog-block style2">
                    <div class="blog-img"><img src="assets/img/update_1/blog/blog_2_4.jpg" alt="blog image"></div>
                    <div class="blog-content">
                        <div class="blog-meta"><a href="blog.html"><i class="fas fa-calendar-alt"></i>October 18,
                                2023</a> <a href="blog.html"><i class="fas fa-tags"></i>Oil Change</a></div>
                        <h3 class="blog-title"><a href="blog-details.html">How to Jump Start Your Car Maintenance?</a>
                        </h3><a href="blog-details.html" class="link-btn">Read More<i
                                class="fas fa-arrow-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="newsletter-area">
    <div class="container">
        <div class="row gx-0 align-items-center">
            <div class="col-lg-6 col-xl-6 col-xxl-5">
                <div class="newsletter-wrapper">
                    <div class="newsletter-image jump"><img src="assets/img/shape/mail.png" alt=""></div>
                    <h4 class="h4 newsletter-title text-white">Subscribe our newsletter To get updates</h4></div>
            </div>
            <div class="col-lg-6 col-xl-6 col-xxl-7">
                <div class="newsletter-form-wrapper">
                    <form class="newsletter-form"><input class="form-control" type="email"
                                                         placeholder="Email Address...." required="">
                        <button type="submit" class="th-btn style3"><span>Subscribe<span class="icon"><i
                                        class="fa-regular fa-paper-plane"></i></span></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $page_footer; ?>

<?php echo $page_footerscript; ?>
</body>