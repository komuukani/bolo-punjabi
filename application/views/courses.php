<?php
echo $page_head;
$category = $this->md->select('tbl_category');
?>
<body>
<div class="main-wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>
    <div class="edu-course-area edu-section-gap bg-color-white">
        <div class="container">
            <div class="row g-5">
                <div class="col-lg-3 order-2 oder-lg-1">
                    <aside class="edu-course-sidebar">
                        <!-- Start Widget Wrapper  -->
                        <div class="edu-course-widget widget-category">
                            <div class="inner">
                                <h5 class="widget-title">Categories</h5>
                                <div class="content">
                                    <div class="button-group isotop-filter filters-button-group d-flex justify-content-start justify-content-lg-end">

                                        <button data-filter="*" class="is-checked"><span class="filter-text">All</span>
                                        </button>
                                        <?php if (!empty($category)):
                                            foreach ($category as $category_data):
                                                ?>
                                                <button data-filter=".cat--<?php echo $category_data->category_id; ?>"><span
                                                            class="filter-text"><?php echo $category_data->title; ?></span>
                                                </button>
                                            <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Widget Wrapper  -->

                    </aside>
                </div>
                <div class="col-lg-9 order-1 oder-lg-2">
                    <div class="grid-metro3 mesonry-list">
                        <div class="resizer"></div>
                        <?php
                        if (!empty($courses)) {
                            foreach ($courses as $courses_data) {
                                $url = base_url('courses/' . ($courses_data->courses_id . "/") . $this->md->generateSeoURL($courses_data->title));
                                ?>
                                <div class="grid-metro-item cat--<?php echo $courses_data->category_id; ?>">
                                    <div class="edu-card card-type-1 radius-small">
                                        <div class="inner">
                                            <div class="thumbnail">
                                                <a href="<?php echo $url; ?>">
                                                    <img class="w-100"
                                                         src="<?php echo base_url($courses_data->photo); ?>"
                                                         alt="<?php echo $courses_data->title; ?>">
                                                </a>
                                                <div class="top-position status-group left-top">
                                                    <span class="eduvibe-status status-03"><?php echo $this->md->getItemName('tbl_category', 'category_id', 'title', $courses_data->category_id); ?></span>
                                                </div>
                                            </div>
                                            <div class="content p-3">
                                                <h6 class="title font-16"><a
                                                            href="<?php echo $url; ?>"><?php echo $courses_data->title; ?></a>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            echo "<div class='alert alert-warning'>Sorry, courses not found!</div>";
                        }
                        ?>
                        <!-- End Single Card  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>
