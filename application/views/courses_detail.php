<?php
echo $page_head;
?>
<body>
<div class="main-wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>
    <div class="edu-course-details-area edu-section-gap bg-color-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="">
                        <img class="img-thumbnail"
                             style="width:100%"
                             src="<?php echo base_url($courses->photo); ?>"
                             alt="<?php echo $courses->title; ?>">
                    </div>
                    <div class="mt-10">
                        <b>Share Course: </b>
                        <div class="text-left sharethis-inline-share-buttons"></div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="course-details-content">
                        <span
                            class="text-FFF border-radius-5 p-2 pl-3 pr-3 bg-primary"><?php echo $this->md->getItemName('tbl_category', 'category_id', 'title', $courses->category_id); ?></span>
                        <h3 class="title pb-0 mb-0"><?php echo $courses->title; ?></h3>
                        <p><?php echo $courses->description; ?></p>
                        <a class="rn-btn edu-btn" href="##">
                            <span>Book Demo Session</span><i class="icon-arrow-right-line-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
</body>
