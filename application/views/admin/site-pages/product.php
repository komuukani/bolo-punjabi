<?php
echo $page_head;  //  Load Head Link and Scripts
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$fields = $this->md->select('tbl_category');    // get all category
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <?php $this->load->view('admin/common/page_header'); // Load Page Header (Title / Navigation)  ?>
                    <div class="row">
                        <?php
                        if ($page_type == "add" || $page_type == "edit"):
                            ?>
                            <!-- >> ADD/EDIT Data Start
                            ================================================== -->
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <?php
                                        if (isset($updata)):
                                            if ($permission['all'] || $permission['edit']):  // If user has edit/update permission
                                                ?>
                                                <!-- >> Edit Form Start
                                                ================================================== -->
                                                <form method="post" name="edit" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            } else {
                                                                echo $updata[0]->title;
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Slug <span
                                                                    class="text-danger">*Every product must have unique slug.</span></label>
                                                            <input type="text" name="slug" id="productSlug"
                                                                   value="<?php
                                                                   if (set_value('slug') && !isset($success)) {
                                                                       echo set_value('slug');
                                                                   } else {
                                                                       echo $updata[0]->slug;
                                                                   }
                                                                   ?>" placeholder="Enter Unique Product Slug"
                                                                   class="form-control <?php if (form_error('slug')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('slug')) {
                                                                    echo form_error('slug');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   } else {
                                                                       echo $updata[0]->sku;
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Category</label>
                                                                <select name="category_id"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                <?php echo $fields_val->category_id == $updata[0]->category_id ? 'selected' : ''; ?>
                                                                                value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('category_id')) {
                                                                        echo form_error('category_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Standard Price</label>
                                                            <input type="text" name="standard_price"
                                                                   value="<?php
                                                                   if (set_value('standard_price') && !isset($success)) {
                                                                       echo set_value('standard_price');
                                                                   } else {
                                                                       echo $updata[0]->standard_price;
                                                                   }
                                                                   ?>" placeholder="Enter Price"
                                                                   class="form-control <?php if (form_error('standard_price')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('standard_price')) {
                                                                    echo form_error('standard_price');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Short
                                                                Description</label>
                                                            <textarea style="height: 100px" name="short_description"
                                                                      class="form-control <?php if (form_error('short_description')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Short Description"><?php
                                                                if (set_value('short_description') && !isset($success)) {
                                                                    echo set_value('short_description');
                                                                } else {
                                                                    echo $updata[0]->short_description;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('short_description')) {
                                                                    echo form_error('short_description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Tags (Exa, tag1, tag2,
                                                                tag3,...)</label>
                                                            <textarea style="height: 100px" name="tags"
                                                                      class="form-control <?php if (form_error('tags')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Tags"><?php
                                                                if (set_value('tags') && !isset($success)) {
                                                                    echo set_value('tags');
                                                                } else {
                                                                    echo $updata[0]->tags;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('tags')) {
                                                                    echo form_error('tags');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                } else {
                                                                    echo $updata[0]->description;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                } else {
                                                                    echo $updata[0]->additional_info;
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                        class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" multiple="" id="file"
                                                                               onchange="imagesPreview(this, '#img_preview');$('#updateStatus').val('yes');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                        <input type="hidden" id="updateStatus"
                                                                               name="updateStatus"/>
                                                                        <input type="hidden"
                                                                               value="<?php echo $updata[0]->photos; ?>"
                                                                               name="oldPath"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <?php
                                                                    $allphotos = $updata[0]->photos ? explode(",", $updata[0]->photos) : [];
                                                                    if (!empty($allphotos)) :
                                                                        $pp = 0;    // photo index
                                                                        foreach ($allphotos as $photo) :
                                                                            ?>
                                                                            <div class="col-md-3">
                                                                                <img
                                                                                    src="<?php echo base_url($photo); ?>"
                                                                                    class="mt-20 center-block"
                                                                                    style="width: 100%;height: 100px;object-fit: contain"/>
                                                                                <a class="btn btn-danger btn-sm"
                                                                                   href="<?php echo base_url('Admin/Pages/removePhoto/product/' . $updata[0]->product_id . '/' . $pp); ?>">Remove</a>
                                                                            </div>
                                                                            <?php
                                                                            $pp++;
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Meta Title <span
                                                                        class="text-danger">*Meta title maximum
                                                                            length is 70 </span></label>
                                                                <input type="text" name="meta_title" value="<?php
                                                                if (set_value('meta_title') && !isset($success)) {
                                                                    echo set_value('meta_title');
                                                                } else {
                                                                    echo $updata[0]->meta_title;
                                                                }
                                                                ?>" placeholder="Enter Meta Title"
                                                                       class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_title')) {
                                                                        echo form_error('meta_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta keywords
                                                                    <span class="text-danger">*Meta keywords allow
                                                                            maximum 30 words </span></label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Keywords"
                                                                          style="height: 70px"
                                                                          name="meta_keyword"><?php
                                                                    if (set_value('meta_keyword') && !isset($success)) {
                                                                        echo set_value('meta_keyword');
                                                                    } else {
                                                                        echo $updata[0]->meta_keyword;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_keyword')) {
                                                                        echo form_error('meta_keyword');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta Description
                                                                    <span class="text-danger">*Meta description
                                                                            length is between 150 to 160 </span>
                                                                </label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Description"
                                                                          style="height: 70px"
                                                                          name="meta_desc"><?php
                                                                    if (set_value('meta_desc') && !isset($success)) {
                                                                        echo set_value('meta_desc');
                                                                    } else {
                                                                        echo $updata[0]->meta_desc;
                                                                    }
                                                                    ?></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_desc')) {
                                                                        echo form_error('meta_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                        <?php echo $updata[0]->featured == 1 ? 'checked' : ''; ?>
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="update">Update Product
                                                        </button>
                                                        <a href="<?php echo base_url($current_page . '/show'); ?>"
                                                           class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light">Cancel</a>
                                                    </footer>
                                                </form>
                                                <!-- << Edit Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        else:
                                            if ($permission['all'] || $permission['write']):  // If user has write/add permission
                                                ?>
                                                <!-- >> Add Form Start
                                                ================================================== -->
                                                <form method="post" name="add" enctype="multipart/form-data">
                                                    <div class="panel-body row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Title</label>
                                                            <input type="text" name="title" value="<?php
                                                            if (set_value('title') && !isset($success)) {
                                                                echo set_value('title');
                                                            }
                                                            ?>" placeholder="Enter Product Title"
                                                                   id="productTitle"
                                                                   class="form-control <?php if (form_error('title')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('title')) {
                                                                    echo form_error('title');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Slug <span
                                                                    class="text-danger">*Every product must have unique slug.</span></label>
                                                            <input type="text" name="slug" id="productSlug"
                                                                   value="<?php
                                                                   if (set_value('slug') && !isset($success)) {
                                                                       echo set_value('slug');
                                                                   }
                                                                   ?>" placeholder="Enter Unique Product Slug"
                                                                   class="form-control <?php if (form_error('slug')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('slug')) {
                                                                    echo form_error('slug');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Product SKU</label>
                                                            <input type="text" name="sku"
                                                                   value="<?php
                                                                   if (set_value('sku') && !isset($success)) {
                                                                       echo set_value('sku');
                                                                   }
                                                                   ?>" placeholder="Enter Product SKU"
                                                                   class="form-control <?php if (form_error('sku')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('sku')) {
                                                                    echo form_error('sku');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Category</label>
                                                                <select name="category_id"
                                                                        class="form-control select2 text-capitalize <?php if (form_error('category_id')) { ?> is-invalid <?php } ?>">
                                                                    <option value="">Select Category</option>
                                                                    <?php
                                                                    if (!empty($fields)):
                                                                        foreach ($fields as $fields_val):
                                                                            ?>
                                                                            <option
                                                                                value="<?php echo $fields_val->category_id; ?>"><?php echo $fields_val->title; ?></option>
                                                                        <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('category_id')) {
                                                                        echo form_error('category_id');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Price</label>
                                                            <input type="text" name="standard_price"
                                                                   value="<?php
                                                                   if (set_value('standard_price') && !isset($success)) {
                                                                       echo set_value('standard_price');
                                                                   }
                                                                   ?>" placeholder="Enter Price"
                                                                   class="form-control <?php if (form_error('standard_price')) { ?> is-invalid <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('standard_price')) {
                                                                    echo form_error('standard_price');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Short
                                                                Description</label>
                                                            <textarea style="height: 100px" name="short_description"
                                                                      class="form-control <?php if (form_error('short_description')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Short Description"><?php
                                                                if (set_value('short_description') && !isset($success)) {
                                                                    echo set_value('short_description');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('short_description')) {
                                                                    echo form_error('short_description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Tags (Exa, tag1, tag2,
                                                                tag3,...)</label>
                                                            <textarea style="height: 100px" name="tags"
                                                                      class="form-control <?php if (form_error('tags')) { ?> is-invalid <?php } ?>"
                                                                      placeholder="Enter Product Tags"><?php
                                                                if (set_value('tags') && !isset($success)) {
                                                                    echo set_value('tags');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('tags')) {
                                                                    echo form_error('tags');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Description</label>
                                                            <textarea name="description"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('description');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('description')) {
                                                                    echo form_error('description');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Product Additional Info</label>
                                                            <textarea name="additional_info"
                                                                      class="summernote"><?php
                                                                if (set_value('description') && !isset($success)) {
                                                                    echo set_value('additional_info');
                                                                }
                                                                ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('additional_info')) {
                                                                    echo form_error('additional_info');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Select Product Photos <span
                                                                        class="font-12 text-info">*(Upload only .jpg | .jpeg | .png files with less than 1MB size and dimension should be 1280*1350.)</span>
                                                                </label>
                                                                <div class="fileupload fileupload-new"
                                                                     data-provides="fileupload">
                                                                    <div class="input-append">
                                                                        <input type="file" id="file" multiple=""
                                                                               onchange="imagesPreview(this, '#img_preview');"
                                                                               name="product_photos[]" class=""
                                                                               accept="image/*">
                                                                    </div>
                                                                </div>
                                                                <div id="img_preview"></div>
                                                                <p class="error-text file-error" style="display: none">
                                                                    Select a valid file!</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Meta Title <span
                                                                        class="text-danger">*Meta title maximum
                                                                            length is 70 </span></label>
                                                                <input type="text" name="meta_title" value="<?php
                                                                if (set_value('meta_title') && !isset($success)) {
                                                                    echo set_value('meta_title');
                                                                }
                                                                ?>" placeholder="Enter Meta Title"
                                                                       class="form-control <?php if (form_error('meta_title')) { ?> is-invalid <?php } ?>">
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_title')) {
                                                                        echo form_error('meta_title');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta keywords
                                                                    <span class="text-danger">*Meta keywords allow
                                                                            maximum 30 words </span></label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Keywords"
                                                                          style="height: 70px"
                                                                          name="meta_keyword"></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_keyword')) {
                                                                        echo form_error('meta_keyword');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Enter Meta Description
                                                                    <span class="text-danger">*Meta description
                                                                            length is between 150 to 160 </span>
                                                                </label>
                                                                <textarea class="form-control"
                                                                          placeholder="Enter Meta Description"
                                                                          style="height: 70px"
                                                                          name="meta_desc"></textarea>
                                                                <div class="error-text">
                                                                    <?php
                                                                    if (form_error('meta_desc')) {
                                                                        echo form_error('meta_desc');
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    <input type="checkbox"
                                                                           name="featured"
                                                                           value="1"
                                                                           class="zoom-14 vertical-middle"/> Set to
                                                                    Featured Product
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <footer class="panel-footer">
                                                        <button type="submit"
                                                                class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                                value="send" name="add">Add Product
                                                        </button>
                                                        <button
                                                            class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-light"
                                                            type="reset">Reset Form
                                                        </button>
                                                    </footer>
                                                </form>
                                                <!-- << Add Form End
                                                ================================================== -->
                                            <?php
                                            else:
                                                $this->load->view('admin/common/access_denied');
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- << ADD/EDIT Data END
                            ================================================== -->
                        <?php
                        else:
                            if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                                ?>
                                <!-- >> Table Data Start
                                ================================================== -->
                                <div class="col-md-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table mb-none table-hover" id="productTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>SKU</th>
                                                        <th>Title</th>
                                                        <th>Standard Price</th>
                                                        <th class="none">Short Description</th>
                                                        <th class="none">Tags</th>
                                                        <th class="none">Entry Date</th>
                                                        <?php
                                                        if ($permission['all'] || $permission['status']):
                                                            echo '<th>Action</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['edit']):
                                                            echo '<th>Edit</th>';
                                                        endif;
                                                        if ($permission['all'] || $permission['delete']):
                                                            echo '<th>Delete</th>';
                                                        endif;
                                                        ?>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- << Table Data End
                                ================================================== -->
                            <?php
                            else:
                                $this->load->view('admin/common/access_denied');
                            endif;
                        endif;
                        ?>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer  ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'status'},
        {data: 'category_id'},
        {data: 'sku'},
        {data: 'title'},
        {data: 'standard_price'},
        {data: 'short_description'},
        {data: 'tags'},
        {data: 'datetime'},
        <?php
        if ($permission['all'] || $permission['status']):
            echo '{data: "action"},';
        endif;
        if ($permission['all'] || $permission['edit']):
            echo '{data: "edit"},';
        endif;
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, 'photos', true);

    // Product Title convert into slug (Replace space with dash)
    jQuery(document).on("keyup blur", "#productTitle", function () {
        let str = $('#productTitle').val();
        jQuery('#productSlug').val('Loading...');
        var data = {
            str: str
        };
        var url = "<?php echo base_url('Admin/Pages/getSlug'); ?>";
        jQuery.post(url, data, function (data) {
            jQuery('#productSlug').val(data);
        });
    });
</script>
</body>  