<?php echo $page_head; //  Load Head Link and Scripts       ?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar  ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <?php
                if ($permission['all'] || $permission['read']):  // If user has read/show data permission
                    ?>
                    <div class="section-body">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-none" id="billTable">
                                        <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Transaction ID</th>
                                            <th>User</th>
                                            <th>Name</th>
                                            <th class="none">Customer Email</th>
                                            <th>Phone</th>
                                            <th>Amount</th>
                                            <th class="none">Country</th>
                                            <th class="none">State</th>
                                            <th class="none">City</th>
                                            <th class="none">Address</th>
                                            <th class="none">Extra Order Notes</th>
                                            <th>Invoice</th>
                                            <th>Download</th>
                                            <th>Date</th>
                                            <?php
                                            if ($permission['all'] || $permission['delete']):
                                                echo '<th>Delete</th>';
                                            endif;
                                            ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                else:
                    $this->load->view('admin/common/access_denied');
                endif;
                ?>
            </section>
        </div>
        <!-- >> Main Content Start
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer   ?>
    </div>
</div>
<?php echo $page_footerscript;  //  Load Footer script  ?>
<script>
    // List column which will be display in the table
    const column = [
        {data: 'order_id'},
        {data: 'transaction_id'},
        {data: 'type'},
        {data: 'name'},
        {data: 'email'},
        {data: 'phone'},
        {data: 'netprice'},
        {data: 'country'},
        {data: 'state'},
        {data: 'city'},
        {data: 'address'},
        {data: 'notes'},
        {data: 'invoice'},
        {data: 'download'},
        {data: 'datetime'},
        <?php
        if ($permission['all'] || $permission['delete']):
            echo '{data: "delete"},';
        endif;
        ?>
    ];
    // Parameter --> [TABLE_NAME, GET_DATA_URL, DISPLAY_COLUMN, PHOTO_COLUMN_NAME_FOR_DELETE]
    getDataTable('<?php echo $active_page; ?>', '<?php echo base_url($current_page . '/getdata') ?>', column, '', true);
</script>
</body>
