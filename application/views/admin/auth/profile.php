<?php
echo $page_head;  //  Load Head Link and Scripts     
$admin_name = ($admin_data ? $admin_data[0]->admin_name : ''); // get admin full name
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
?>
<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <?php echo $page_header; //  Load Header  ?>
        <?php echo $page_sidebar; //  Load Sidebar    ?>
        <!-- >> Main Content Start
        ================================================== -->
        <div class="main-content">
            <section class="section">
                <?php echo $page_breadcrumb; // Load Breadcrumb ?>
                <div class="section-body">
                    <h2 class="section-title">Hi, <?php echo ucfirst($admin_name); ?>!</h2>
                    <p class="section-lead">
                        Change information about yourself on this page.
                    </p>
                    <div class="row mt-sm-4">
                        <div class="col-12 col-md-12 col-lg-5">
                            <div class="card profile-widget">
                                <div class="profile-widget-header">
                                    <img alt="<?php echo $admin_name; ?>"
                                         src="<?php echo base_url($admin_data[0]->path ? $admin_data[0]->path : FILENOTFOUND); ?>"
                                         class="rounded-circle profile-widget-picture">
                                    <div class="profile-widget-items">
                                        <div class="profile-widget-item">
                                            <div class="profile-widget-item-label">Profile Status</div>
                                            <div
                                                class="profile-widget-item-value"><?php echo ($admin_data[0]->status) ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>'; ?></div>
                                        </div>
                                        <div class="profile-widget-item">
                                            <div class="profile-widget-item-label">Register Date</div>
                                            <div
                                                class="profile-widget-item-value"><?php echo date(($web_data ? $web_data[0]->date_format : 'd-M-Y'), strtotime($admin_data[0]->register_date)) ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-widget-description">
                                    <div class="profile-widget-name"><?php echo ucfirst($admin_name); ?></div>
                                    <?php echo($admin_data ? $admin_data[0]->bio : ''); ?>
                                    <hr/>
                                    <a class="badge badge-primary"
                                       href="<?php echo base_url('admin-setting/security') ?>">Change Password <i
                                            class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-7">
                            <div class="card">
                                <form method="post" novalidate="" enctype="multipart/form-data">
                                    <div class="card-header">
                                        <h4>Edit Profile</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-12">
                                                <label class="control-label">Admin Name</label>
                                                <input type="text" name="admin_name"
                                                       value="<?php echo(($admin_data) ? $admin_data[0]->admin_name : ''); ?>"
                                                       placeholder="Enter Admin Name"
                                                       class="form-control <?php if (form_error('admin_name')) { ?>  is-invalid  <?php } ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('admin_name')) {
                                                        echo form_error('admin_name');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6 col-12">
                                                <label>Phone</label>
                                                <input type="tel" name="phone"
                                                       value="<?php echo($admin_data ? $admin_data[0]->phone : ''); ?>"
                                                       placeholder="Enter Phone"
                                                       class="form-control <?php if (form_error('phone')) { ?>  is-invalid  <?php } ?>">
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('phone')) {
                                                        echo form_error('phone');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12 col-12">
                                                <label>Email</label>
                                                <input type="email" class="form-control"
                                                       name="email"
                                                       value="<?php echo($admin_data ? $admin_data[0]->email : ''); ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-5 col-12">
                                                <label>Address</label>
                                                <textarea style="height: 100px" name="address"
                                                          placeholder="Enter Address"
                                                          class="form-control <?php if (form_error('address')) { ?>  is-invalid  <?php } ?>"><?php echo($admin_data ? $admin_data[0]->address : ''); ?></textarea>
                                                <div class="error-text">
                                                    <?php
                                                    if (form_error('address')) {
                                                        echo form_error('address');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-7 col-12">
                                                <label class="control-label">Select Profile <span
                                                        style="font-size: 12px" class="text-info">*(Upload only .png | .ico  files.)</span>
                                                </label>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="input-append">
                                                        <input type="file" id="file"
                                                               onchange="readURL(this, 'blah2');document.getElementById('updateProfileStatus').value = 'yes';"
                                                               name="profile" class="" accept="image/*">
                                                        <input type="hidden" id="updateProfileStatus"
                                                               name="updateProfileStatus"/>
                                                        <input type="hidden"
                                                               value="<?php echo(($admin_data) ? $admin_data[0]->path : ''); ?>"
                                                               name="oldPath"/>
                                                    </div>
                                                </div>
                                                <?php
                                                if ($admin_data ? $admin_data[0]->path : ''):
                                                    ?>
                                                    <img
                                                        src="<?php echo base_url(($admin_data) ? $admin_data[0]->path : FILENOTFOUND); ?>"
                                                        class="mt-10 center-block" width="50" id="blah2"/>
                                                <?php
                                                else:
                                                    ?>
                                                    <img class="mt-10 center-block" width="50" id="blah2"/>
                                                <?php
                                                endif;
                                                ?>
                                                <div class="error-text file-error" style="display: none">Select a valid
                                                    file!
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <label>Bio <span style="font-size: 12px" class="text-info">*(Maximum 500 Characters allow.)</span></label>
                                                <textarea name="bio" class="form-control summernote-simple"
                                                          maxlength="500"><?php echo($admin_data ? $admin_data[0]->bio : ''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-right">
                                        <button type="submit" class="mb-2 mr-2 btn-hover-shine btn btn-shadow btn-info"
                                                value="send" name="update">Save Changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- << Main Content End
        ================================================== -->
        <?php echo $page_footer;  //  Load Footer    ?>
    </div>
</div>
<?php
$alert_data['success'] = $success;
$alert_data['error'] = $error;
$this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
?>
</body> 