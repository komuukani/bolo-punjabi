<?php
echo $page_head;  //  Load Head Link and Scripts   
$type = ($this->uri->segment(2) ? $this->uri->segment(2) : 'general'); // Get page type [general/security/system/email/seo/...]
// Getting All field of tbl_control table and list it in a submenu
$control_data = $this->md->select('tbl_control_feature');
$fields = $this->db->list_fields('tbl_control_feature');
$email_address = explode(',', ($web_data) ? $web_data[0]->email_address : '');
$phoneno_arr = explode(',', ($web_data) ? $web_data[0]->phone : '');
$success = $this->session->flashdata('success');
$error = $this->session->flashdata('error');
$superAdmin = true;
if ($admin_data):
    if ($admin_data[0]->admin_id != 1):
        $superAdmin = false;
        $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $admin_data[0]->user_role_id, 'type' => 'feature'));   // get current user's permisison   
    endif;
endif;
?> 
<body> 
    <!-- Country Code Styles -->
    <link rel="stylesheet" href="admin_asset/modules/country_code/build/css/intlTelInput.css"> 
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header  ?>  
            <?php echo $page_sidebar; //  Load Sidebar  ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb   ?> 
                    <div class="section-body">
                        <h2 class="section-title">All About General Settings</h2>
                        <p class="section-lead">
                            You can adjust all general settings here
                        </p>

                        <div id="output-status"></div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card" style="position: sticky;top: 0px">
                                    <div class="card-header">
                                        <h4>Jump To</h4>
                                    </div>
                                    <div class="card-body">
                                        <ul class="nav nav-pills flex-column">
                                            <?php
                                            if ($superAdmin):
                                                foreach ($fields as $fields_val):
                                                    if ($fields_val != 'control_feature_id' && $control_data[2]->$fields_val):
                                                        ?>
                                                        <li class="nav-item"><a href="<?php echo base_url('admin-setting/' . strtolower($fields_val)); ?>" class="nav-link <?php echo ($type == $fields_val) ? 'active' : ''; ?>"> <i class="<?php echo $control_data[1]->$fields_val; ?>"></i> <?php echo ucfirst($fields_val); ?></a></li>
                                                        <?php
                                                    endif;
                                                endforeach;
                                            else:
                                                $per_id = array();
                                                foreach ($admin_menu as $ext_per) {
                                                    $per = json_decode($ext_per->permission_id);
                                                    foreach ($per as $per_val):
                                                        $per_id[] = $per_val;
                                                    endforeach;
                                                }
                                                $allowed_menu = array_unique($per_id);
                                                sort($allowed_menu);    // Sort Array  
                                                $allowed_feature = array();
                                                if (!empty($allowed_menu)):
                                                    foreach ($allowed_menu as $per_val):
                                                        $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                                                        $field_data = $this->md->my_query("SELECT `" . $exist_prmsn[0]->feature . "` FROM `tbl_control_feature` ")->result_array();
                                                        $allowed_feature[] = $exist_prmsn[0]->feature;
                                                        ?>
                                                        <li class="nav-item"><a href="<?php echo base_url('admin-setting/' . strtolower($exist_prmsn[0]->feature)); ?>" class="nav-link <?php echo ($type == $exist_prmsn[0]->feature) ? 'active' : ''; ?>"> <i class="<?php echo $field_data[1][$exist_prmsn[0]->feature] ?>"></i> <?php echo ucfirst($exist_prmsn ? $exist_prmsn[0]->feature : ''); ?></a></li>
                                                        <?php
                                                    endforeach;
                                                endif;
                                            endif;
                                            ?> 
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <?php
                                if ($type == 'general'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <div>
                                            <div class="card" id="settings-card">
                                                <form method="post" name="change_logo_favicon" novalidate="" enctype="multipart/form-data"> 
                                                    <div class="card-header">
                                                        <h4>Project Name, Logo & Favicon Icon Settings</h4>
                                                    </div>
                                                    <div class="card-body">   
                                                        <div class="form-group">
                                                            <label class="control-label">Project Name</label>
                                                            <input type="text" name="project_name" value="<?php echo (($web_data) ? $web_data[0]->project_name : ''); ?>" placeholder="Enter Project Name" class="form-control <?php if (form_error('project_name')) { ?>  is-invalid  <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('project_name')) {
                                                                    echo form_error('project_name');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>   
                                                        <div class="form-group">
                                                            <label class="control-label">Select Logo <span style="font-size: 12px" class="text-info">*(Upload only .png | .ico  files.)</span> </label>
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="input-append"> 
                                                                    <input type="file" id="file" onchange="readURL(this, 'blah');document.getElementById('updateStatus').value = 'yes';" name="logo" class="" accept="image/*">  
                                                                    <input type="hidden" id="updateStatus" name="updateStatus" />
                                                                    <input type="hidden" value="<?php echo (($web_data) ? $web_data[0]->logo : ''); ?>" name="oldPath" />
                                                                </div>
                                                            </div>  
                                                            <?php
                                                            if ((($web_data) ? $web_data[0]->logo : '')):
                                                                ?>
                                                                <img src="<?php echo base_url((($web_data) ? $web_data[0]->logo : '')); ?>" class="mt-10 center-block" width="50" id="blah" />
                                                            <?php else:
                                                                ?>
                                                                <img class="mt-10 center-block" width="50" id="blah" />
                                                            <?php
                                                            endif;
                                                            ?> 
                                                            <div class="error-text file-error" style="display: none">Select a valid file!</div> 
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="control-label">Select Favicon <span style="font-size: 12px" class="text-info">*(Upload only .png | .ico  files.)</span> </label>
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                <div class="input-append"> 
                                                                    <input type="file" id="file" onchange="readURL(this, 'blah1');document.getElementById('updateStatusfavicon').value = 'yes';" name="favicon" class="" accept="image/*">  
                                                                    <input type="hidden" id="updateStatusfavicon" name="updateStatusfavicon" />
                                                                    <input type="hidden" value="<?php echo (($web_data) ? $web_data[0]->favicon : ''); ?>" name="oldPathfavicon" />
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if ((($web_data) ? $web_data[0]->favicon : '')):
                                                                ?> 
                                                                <img src="<?php echo base_url((($web_data) ? $web_data[0]->favicon : FILENOTFOUND)); ?>" class="mt-10 center-block" width="50" id="blah1" />
                                                            <?php else:
                                                                ?>
                                                                <img class="mt-10 center-block" width="50" id="blah1" />
                                                            <?php
                                                            endif;
                                                            ?> 
                                                            <div class="error-text file-error" style="display: none">Select a valid file!</div> 
                                                        </div>  
                                                    </div>
                                                    <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                        <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="update">Save</button>
                                                        <button class="btn btn-secondary" type="reset">Reset</button>
                                                    </div>
                                                </form>
                                            </div> 
                                            <div class="card" id="settings-card">
                                                <form method="post" name="general" novalidate="" enctype="multipart/form-data">
                                                    <div class="card-header">
                                                        <h4>General Settings</h4>
                                                    </div>
                                                    <div class="card-body">  
                                                        <div class="form-group">
                                                            <label class="control-label">Office Number</label>
                                                            <input type="text" name="office" value="<?php echo (($web_data) ? $web_data[0]->office : ''); ?>" placeholder="Enter Office Number" class="form-control <?php if (form_error('office')) { ?>  is-invalid  <?php } ?>">
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('office')) {
                                                                    echo form_error('office');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group addContent">
                                                            <label class="control-label">Contact Number</label> 
                                                            <?php
                                                            foreach ($phoneno_arr as $key => $number) {
                                                                ?>
                                                                <div class="item">
                                                                    <input type="text" name="phone[]" value="<?php echo $number; ?>" placeholder="Enter Contact Number" class="form-control <?php if (form_error('phone')) { ?>  is-invalid  <?php } ?>">    
                                                                    <?php
                                                                    echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                                    ?>
                                                                </div> 
                                                                <?php
                                                            }
                                                            ?>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('phone')) {
                                                                    echo form_error('phone');
                                                                }
                                                                ?>
                                                            </div> 
                                                        </div> 
                                                        <div class="form-group addContent">
                                                            <label class="control-label">Email Address</label>
                                                            <?php
                                                            foreach ($email_address as $key => $email) {
                                                                ?>
                                                                <div class="item">
                                                                    <input type="text" name="email[]" value="<?php echo $email; ?>" placeholder="Enter Email Address" class="form-control <?php if (form_error('email')) { ?>  is-invalid  <?php } ?>">   
                                                                    <?php
                                                                    echo ($key > 0) ? '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>' : '<button title="Add Item" class="btn btn-md btn-primary addMore" type="button"><i class="fa fa-plus"></i></button>';
                                                                    ?>
                                                                </div> 
                                                                <?php
                                                            }
                                                            ?> 
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('email')) {
                                                                    echo form_error('email');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="control-label">Address</label>
                                                            <textarea name="address" placeholder="Enter Address" class="form-control <?php if (form_error('address')) { ?>  is-invalid  <?php } ?>"><?php echo (($web_data) ? $web_data[0]->address : ''); ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('address')) {
                                                                    echo form_error('address');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="control-label">Map Location <code>*Paste embed iframe of map location</code></label>
                                                            <textarea name="map" style="height: 80px" placeholder="Enter Map Location" class="form-control <?php if (form_error('map')) { ?>  is-invalid  <?php } ?>"><?php echo (($web_data) ? $web_data[0]->map : ''); ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('map')) {
                                                                    echo form_error('map');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="control-label">Footer Info</label>
                                                            <textarea readonly="" name="footer" placeholder="Enter Footer Info" class="form-control <?php if (form_error('footer')) { ?>  is-invalid  <?php } ?>"><?php echo (($web_data) ? $web_data[0]->footer : ''); ?></textarea>
                                                            <div class="error-text">
                                                                <?php
                                                                if (form_error('footer')) {
                                                                    echo form_error('footer');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                        <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="contact">Save</button>
                                                        <button class="btn btn-secondary" type="reset">Reset</button>
                                                    </div>
                                                </form>
                                            </div> 
                                        </div>
                                    <?php
                                    endif;
                                elseif ($type == 'social'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="social" novalidate="">
                                            <div class="card" id="settings-card">
                                                <div class="card-header">
                                                    <h4>SEO & Social Media Settings</h4>
                                                </div>
                                                <div class="card-body">   
                                                    <div class="form-group">
                                                        <label class="control-label">Enter Meta keywords <small class="text-danger">*Meta keywords allow maximum 30 words </small></label> 
                                                        <textarea class="form-control <?php if (form_error('meta_keyword')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Keywords" style="height: 70px" name="meta_keyword"><?php echo $web_data[0]->meta_keyword; ?></textarea>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('meta_keyword')) {
                                                                echo form_error('meta_keyword');
                                                            }
                                                            ?>
                                                        </div> 
                                                    </div>    
                                                    <div class="form-group">
                                                        <label class="control-label">Enter Meta Description <small class="text-danger">*Meta description length is between 150 to 160 </small> </label> 
                                                        <textarea class="form-control <?php if (form_error('meta_desc')) { ?> is-invalid <?php } ?>" placeholder="Enter Meta Description" style="height: 70px" name="meta_desc"><?php echo $web_data[0]->meta_desc; ?></textarea>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('meta_desc')) {
                                                                echo form_error('meta_desc');
                                                            }
                                                            ?>
                                                        </div> 
                                                    </div>    
                                                    <div class="form-group">
                                                        <label class="control-label">Facebook</label>
                                                        <input type="text" name="facebook" value="<?php echo $web_data[0]->facebook; ?>" placeholder="Paste Your Facebook Link URL" class="form-control <?php if (form_error('facebook')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('facebook')) {
                                                                echo form_error('facebook');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Instagram</label>
                                                        <input type="text" name="instagram" value="<?php echo $web_data[0]->instagram; ?>" placeholder="Paste Your Instagram Link URL" class="form-control <?php if (form_error('instagram')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('instagram')) {
                                                                echo form_error('instagram');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Twitter</label>
                                                        <input type="text" name="twitter" value="<?php echo $web_data[0]->twitter; ?>" placeholder="Paste Your Twitter Link URL" class="form-control <?php if (form_error('twitter')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('twitter')) {
                                                                echo form_error('twitter');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Linkedin</label>
                                                        <input type="text" name="linkedin" value="<?php echo $web_data[0]->linkedin; ?>" placeholder="Paste Your Linkedin Link URL" class="form-control <?php if (form_error('linkedin')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('linkedin')) {
                                                                echo form_error('linkedin');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Youtube</label>
                                                        <input type="text" name="youtube" value="<?php echo $web_data[0]->youtube; ?>" placeholder="Paste Your Youtube Link URL" class="form-control <?php if (form_error('youtube')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('youtube')) {
                                                                echo form_error('youtube');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Whatsapp</label>
                                                        <input type="tel" id="whatsapp_country_code" style="padding-left: 45px" maxlength="12" name="whatsapp" value="<?php echo $web_data[0]->whatsapp; ?>" placeholder="Exa. 9874561231"  class="phone-number form-control <?php if (form_error('whatsapp')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('whatsapp')) {
                                                                echo form_error('whatsapp');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="social">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'email'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="email" novalidate="">
                                            <div class="card" id="settings-card">
                                                <div class="card-header">
                                                    <h4>Email Settings</h4>
                                                </div>
                                                <div class="card-body row">  
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Enter SMTP Host</label>
                                                        <input type="text" name="smtp_host" value="<?php
                                                        if (set_value('smtp_host') && !isset($success)) {
                                                            echo set_value('smtp_host');
                                                        } else {
                                                            echo $web_data[0]->mail_smtp_host;
                                                        }
                                                        ?>" placeholder="Exa. ssl://smtp.googlemail.com" class="form-control <?php if (form_error('smtp_host')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('smtp_host')) {
                                                                echo form_error('smtp_host');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Enter Protocol</label>
                                                        <input type="text" name="mail_protocol" value="<?php
                                                        if (set_value('mail_protocol') && !isset($success)) {
                                                            echo set_value('mail_protocol');
                                                        } else {
                                                            echo $web_data[0]->mail_protocol;
                                                        }
                                                        ?>" placeholder="Exa. ssmtp" class="form-control <?php if (form_error('mail_protocol')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('mail_protocol')) {
                                                                echo form_error('mail_protocol');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Enter Mail Port</label>
                                                        <input type="number" name="mail_port" value="<?php
                                                        if (set_value('mail_port') && !isset($success)) {
                                                            echo set_value('mail_port');
                                                        } else {
                                                            echo $web_data[0]->mail_port;
                                                        }
                                                        ?>" placeholder="Exa. 465" class="form-control <?php if (form_error('mail_port')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('mail_port')) {
                                                                echo form_error('mail_port');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group col-md-6">
                                                        <label class="control-label">Enter Email Address</label>
                                                        <input type="email" name="email_address" value="<?php
                                                        if (set_value('email_address') && !isset($success)) {
                                                            echo set_value('email_address');
                                                        } else {
                                                            echo $web_data[0]->mail_email;
                                                        }
                                                        ?>" placeholder="Exa. demo@gmail.com" class="form-control <?php if (form_error('email_address')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('email_address')) {
                                                                echo form_error('email_address');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group col-md-6 pass_hide_show">
                                                        <label class="control-label">Enter Password</label>
                                                        <input type="password" name="password" value="<?php
                                                        if (set_value('password') && !isset($success)) {
                                                            echo set_value('password');
                                                        } else {
                                                            echo $web_data[0]->mail_password;
                                                        }
                                                        ?>" placeholder="**********" class="form-control <?php if (form_error('password')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="action-btn" style="width: auto">
                                                            <i class="fa fa-eye" title="Show Password"></i>
                                                        </div>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('password')) {
                                                                echo form_error('password');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-12">
                                                        <h5 class="font-16">Set up Email Templates</h5>
                                                        <ul class="nav nav-fill nav-pills" id="myTab" role="tablist">
                                                            <li class="nav-item bg-whitesmoke">
                                                                <a class="nav-link active btn-sm" id="forgot-tab" data-toggle="tab" href="#forgot" role="tab" aria-controls="Forgot" aria-selected="true">Forgot Password</a>
                                                            </li>
                                                            <li class="nav-item bg-whitesmoke">
                                                                <a class="nav-link btn-sm" id="welcome-tab" data-toggle="tab" href="#welcome" role="tab" aria-controls="Welcome" aria-selected="false">Welcome</a>
                                                            </li>
                                                            <li class="nav-item bg-whitesmoke">
                                                                <a class="nav-link btn-sm" id="inquiry-tab" data-toggle="tab" href="#inquiry" role="tab" aria-controls="Inquiry" aria-selected="false">Inquiry</a>
                                                            </li>
                                                            <li class="nav-item bg-whitesmoke">
                                                                <a class="nav-link btn-sm" id="active-tab" data-toggle="tab" href="#active" role="tab" aria-controls="Active" aria-selected="false">Active Profile</a>
                                                            </li>
                                                            <li class="nav-item bg-whitesmoke">
                                                                <a class="nav-link btn-sm" id="inactive-tab" data-toggle="tab" href="#inactive" role="tab" aria-controls="Inactive" aria-selected="false">Inactive Profile</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content bg-whitesmoke tab-bordered" id="myTabContent">
                                                            <div class="tab-pane fade show active" id="forgot" role="tabpanel" aria-labelledby="forgot-tab">
                                                                <!-- Forgot --> 
                                                                <div class="form-group">
                                                                    <label class="control-label">Forgot Mail Subject</label>
                                                                    <input type="text" name="forgot_mail_subject" value="<?php
                                                                    if (set_value('forgot_mail_subject') && !isset($success)) {
                                                                        echo set_value('forgot_mail_subject');
                                                                    } else {
                                                                        echo $web_data[0]->forgot_mail_subject;
                                                                    }
                                                                    ?>" placeholder="Enter Forgot Mail Subject" class="form-control <?php if (form_error('forgot_mail_subject')) { ?>  is-invalid  <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('forgot_mail_subject')) {
                                                                            echo form_error('forgot_mail_subject');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Forgot Message of mail in your format</label>
                                                                    <textarea name="forgot_mail_message" class="summernote"><?php
                                                                        if (set_value('forgot_mail_message') && !isset($success)) {
                                                                            echo set_value('forgot_mail_message');
                                                                        } else {
                                                                            echo $web_data[0]->forgot_mail_message;
                                                                        }
                                                                        ?></textarea> 
                                                                    <div class="error-text">
                                                                        <?php
                                                                        echo form_error('forgot_mail_message');
                                                                        ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <div class="tab-pane fade" id="welcome" role="tabpanel" aria-labelledby="welcome-tab">
                                                                <!-- Welcome -->
                                                                <div class="form-group">
                                                                    <label class="control-label">Welcome Mail Subject</label>
                                                                    <input type="text" name="welcome_mail_subject" value="<?php
                                                                    if (set_value('welcome_mail_subject') && !isset($success)) {
                                                                        echo set_value('welcome_mail_subject');
                                                                    } else {
                                                                        echo $web_data[0]->welcome_mail_subject;
                                                                    }
                                                                    ?>" placeholder="Enter Welcome Mail Subject" class="form-control <?php if (form_error('welcome_mail_subject')) { ?>  is-invalid  <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('welcome_mail_subject')) {
                                                                            echo form_error('welcome_mail_subject');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Welcome Message of mail in your format</label>
                                                                    <textarea name="welcome_mail_message" class="summernote"><?php
                                                                        if (set_value('welcome_mail_message') && !isset($success)) {
                                                                            echo set_value('welcome_mail_message');
                                                                        } else {
                                                                            echo $web_data[0]->welcome_mail_message;
                                                                        }
                                                                        ?></textarea> 
                                                                    <div class="error-text">
                                                                        <?php
                                                                        echo form_error('welcome_mail_message');
                                                                        ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <div class="tab-pane fade" id="inquiry" role="tabpanel" aria-labelledby="inquiry-tab">
                                                                <!-- Inquiry -->
                                                                <div class="form-group">
                                                                    <label class="control-label">Inquiry Mail Subject</label>
                                                                    <input type="text" name="inquiry_mail_subject" value="<?php
                                                                    if (set_value('inquiry_mail_subject') && !isset($success)) {
                                                                        echo set_value('inquiry_mail_subject');
                                                                    } else {
                                                                        echo $web_data[0]->inquiry_mail_subject;
                                                                    }
                                                                    ?>" placeholder="Enter Inquiry Mail Subject" class="form-control <?php if (form_error('inquiry_mail_subject')) { ?>  is-invalid  <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('inquiry_mail_subject')) {
                                                                            echo form_error('inquiry_mail_subject');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Inquiry Message of mail in your format</label>
                                                                    <textarea name="inquiry_mail_message" class="summernote"><?php
                                                                        if (set_value('inquiry_mail_message') && !isset($success)) {
                                                                            echo set_value('inquiry_mail_message');
                                                                        } else {
                                                                            echo $web_data[0]->inquiry_mail_message;
                                                                        }
                                                                        ?></textarea> 
                                                                    <div class="error-text">
                                                                        <?php
                                                                        echo form_error('inquiry_mail_message');
                                                                        ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <div class="tab-pane fade" id="active" role="tabpanel" aria-labelledby="active-tab">
                                                                <!-- Inquiry -->
                                                                <div class="form-group">
                                                                    <label class="control-label">Active Profile Mail Subject</label>
                                                                    <input type="text" name="active_mail_subject" value="<?php
                                                                    if (set_value('active_mail_subject') && !isset($success)) {
                                                                        echo set_value('active_mail_subject');
                                                                    } else {
                                                                        echo $web_data[0]->active_mail_subject;
                                                                    }
                                                                    ?>" placeholder="Enter Active Mail Subject" class="form-control <?php if (form_error('active_mail_subject')) { ?>  is-invalid  <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('active_mail_subject')) {
                                                                            echo form_error('active_mail_subject');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Active Profile Message of mail in your format</label>
                                                                    <textarea name="active_mail_message" class="summernote"><?php
                                                                        if (set_value('active_mail_message') && !isset($success)) {
                                                                            echo set_value('active_mail_message');
                                                                        } else {
                                                                            echo $web_data[0]->active_mail_message;
                                                                        }
                                                                        ?></textarea> 
                                                                    <div class="error-text">
                                                                        <?php
                                                                        echo form_error('active_mail_message');
                                                                        ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <div class="tab-pane fade" id="inactive" role="tabpanel" aria-labelledby="inactive-tab">
                                                                <!-- Inquiry -->
                                                                <div class="form-group">
                                                                    <label class="control-label">Inactive Profile Mail Subject</label>
                                                                    <input type="text" name="inactive_mail_subject" value="<?php
                                                                    if (set_value('inactive_mail_subject') && !isset($success)) {
                                                                        echo set_value('inactive_mail_subject');
                                                                    } else {
                                                                        echo $web_data[0]->inactive_mail_subject;
                                                                    }
                                                                    ?>" placeholder="Enter Inactive Mail Subject" class="form-control <?php if (form_error('inactive_mail_subject')) { ?>  is-invalid  <?php } ?>">
                                                                    <div class="error-text">
                                                                        <?php
                                                                        if (form_error('inactive_mail_subject')) {
                                                                            echo form_error('inactive_mail_subject');
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>  
                                                                <div class="form-group">
                                                                    <label class="control-label">Enter Inactive Profile Message of mail in your format</label>
                                                                    <textarea name="inactive_mail_message" class="summernote"><?php
                                                                        if (set_value('inactive_mail_message') && !isset($success)) {
                                                                            echo set_value('inactive_mail_message');
                                                                        } else {
                                                                            echo $web_data[0]->inactive_mail_message;
                                                                        }
                                                                        ?></textarea> 
                                                                    <div class="error-text">
                                                                        <?php
                                                                        echo form_error('inactive_mail_message');
                                                                        ?>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div> 
                                                    </div>  
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="mail_save">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'system'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form id="system-form" method="post">
                                            <div class="card" id="settings-card">
                                                <div class="card-header">
                                                    <h4>System Settings</h4>
                                                </div>
                                                <div class="card-body"> 
                                                    <div class="form-group">
                                                        <label class="control-label">Date format</label>
                                                        <select name="date_format" class="form-control select2">
                                                            <option <?php echo ($web_data[0]->date_format == 'd-m-Y' ? 'selected' : ''); ?> value="d-m-Y">DD-MM-YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'm-d-Y' ? 'selected' : ''); ?> value="m-d-Y">MM-DD-YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'Y-m-d' ? 'selected' : ''); ?> value="Y-m-d">YYYY-MM-DD</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'm/d/Y' ? 'selected' : ''); ?> value="m/d/Y">MM/DD/YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'd/m/Y' ? 'selected' : ''); ?> value="d/m/Y">DD/MM/YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'Y/m/d' ? 'selected' : ''); ?> value="Y/m/d">YYYY/MM/DD</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'm.d.Y' ? 'selected' : ''); ?> value="m.d.Y">MM.DD.YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'd.m.Y' ? 'selected' : ''); ?> value="d.m.Y">DD.MM.YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'Y.m.d' ? 'selected' : ''); ?> value="Y.m.d">YYYY.MM.DD</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'd,M Y' ? 'selected' : ''); ?> value="d,M Y">DD,MMM YYYY</option>
                                                            <option <?php echo ($web_data[0]->date_format == 'd-M-Y' ? 'selected' : ''); ?> value="d-M-Y">DD-MMM-YYYY</option>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('date_format')) {
                                                                echo form_error('date_format');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="control-label">Time format</label> <Br/>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="time_format_1" value="h" <?php echo ($web_data[0]->time_format == 'h') ? 'checked' : '' ?> name="time_format" class="custom-control-input">
                                                            <label class="custom-control-label" for="time_format_1">12 Hours</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="time_format_2" value="H" <?php echo ($web_data[0]->time_format == 'H') ? 'checked' : '' ?> name="time_format" class="custom-control-input">
                                                            <label class="custom-control-label" for="time_format_2">24 Hours</label>
                                                        </div> 
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('time_format')) {
                                                                echo form_error('time_format');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="control-label">Time Zone <code>*Data comes from tbl_timezone table, you can add more entries as per your need.</code> </label>
                                                        <select name="timezone" class="form-control select2 text-capitalize <?php if (form_error('timezone')) { ?> is-invalid <?php } ?>">
                                                            <?php
                                                            $fields = $this->md->select('tbl_timezone');
                                                            if (!empty($fields)):
                                                                foreach ($fields as $fields_val):
                                                                    ?>
                                                                    <option <?php echo ($web_data[0]->timezone == $fields_val->timezone_id ? 'selected' : ''); ?> value="<?php echo $fields_val->timezone_id; ?>"><?php echo $fields_val->timezone; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>  
                                                        </select> 
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('timezone')) {
                                                                echo form_error('timezone');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="system">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'security'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="security" novalidate="">
                                            <div class="card" id="settings-card">
                                                <div class="card-header">
                                                    <h4>Security Settings</h4>
                                                </div>
                                                <div class="card-body"> 
                                                    <div class="form-group pass_hide_show">
                                                        <label class="control-label">Current Password</label>
                                                        <input type="password" name="current" value="<?php
                                                        if (set_value('current') && !isset($success)) {
                                                            echo set_value('current');
                                                        }
                                                        ?>" placeholder="Enter Current Password" class="form-control <?php if (form_error('current')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="action-btn">
                                                            <i class="fa fa-eye" title="Show Password"></i>
                                                        </div>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('current')) {
                                                                echo form_error('current');
                                                            }
                                                            ?>
                                                        </div> 
                                                    </div>  
                                                    <div class="form-group pass_hide_show">
                                                        <label class="control-label">New Password</label>
                                                        <input type="password" name="new" value="<?php
                                                        if (set_value('new') && !isset($success)) {
                                                            echo set_value('new');
                                                        }
                                                        ?>" placeholder="Enter New Password" class="form-control <?php if (form_error('new')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="action-btn">
                                                            <i class="fa fa-eye" title="Show Password"></i>
                                                        </div>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('new')) {
                                                                echo form_error('new');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group pass_hide_show">
                                                        <label class="control-label">Confirm Password</label>
                                                        <input type="password" name="confirm" value="<?php
                                                        if (set_value('confirm') && !isset($success)) {
                                                            echo set_value('confirm');
                                                        }
                                                        ?>" placeholder="Enter Confirm Password" class="form-control <?php if (form_error('confirm')) { ?> is-invalid <?php } ?>">
                                                        <div class="action-btn">
                                                            <i class="fa fa-eye" title="Show Password"></i>
                                                        </div>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('confirm')) {
                                                                echo form_error('confirm');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="send">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'captcha'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="captcha" novalidate="">
                                            <div class="card" id="settings-card">
                                                <div class="card-header">
                                                    <h4>Google Captcha Settings</h4>
                                                </div>
                                                <div class="card-body">  
                                                    <div class="form-group">
                                                        <label class="control-label">Site Key</label>
                                                        <input type="text" name="site" value="<?php
                                                        if (set_value('site') && !isset($success)) {
                                                            echo set_value('site');
                                                        } else {
                                                            echo $web_data[0]->captcha_site_key;
                                                        }
                                                        ?>" placeholder="Paste Site Key" class="form-control <?php if (form_error('site')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('site')) {
                                                                echo form_error('site');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group">
                                                        <label class="control-label">Secret Key</label>
                                                        <input type="text" name="secret" value="<?php
                                                        if (set_value('secret') && !isset($success)) {
                                                            echo set_value('secret');
                                                        } else {
                                                            echo $web_data[0]->captcha_secret_key;
                                                        }
                                                        ?>" placeholder="Paste Secret Key" class="form-control <?php if (form_error('secret')) { ?>  is-invalid  <?php } ?>">
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('secret')) {
                                                                echo form_error('secret');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="control-label">Captcha Visibility</label> <br/>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadioInline1" value="1" <?php echo ($web_data[0]->captcha_visibility) ? 'checked' : '' ?> name="visibility" class="custom-control-input">
                                                            <label class="custom-control-label" for="customRadioInline1">Visible</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadioInline2" value="0" <?php echo (!$web_data[0]->captcha_visibility) ? 'checked' : '' ?> name="visibility" class="custom-control-input">
                                                            <label class="custom-control-label" for="customRadioInline2">Hidden</label>
                                                        </div> 
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('visibility')) {
                                                                echo form_error('visibility');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="captcha">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'theme'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="theme_form" novalidate=""> 
                                            <div class="card"  id="settings-card">
                                                <div class="card-header">
                                                    <h4>Theme Settings</h4>
                                                </div> 
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label class="form-label">Select Theme</label> 
                                                        <div class="row gutters-sm"> 
                                                            <div class="col-6 col-sm-4"> 
                                                                <label class="imagecheck mb-4">
                                                                    <input <?php echo ($admin_data[0]->theme) ? (($admin_data[0]->theme == 'theme-1') ? 'checked' : '') : 'checked'; ?> name="theme" type="radio" value="theme-1" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img01.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                            <div class="col-6 col-sm-4">
                                                                <label class="imagecheck mb-4">
                                                                    <input  <?php echo ($admin_data[0]->theme == 'theme-2') ? 'checked' : '' ?> name="theme" type="radio" value="theme-2" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img02.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                            <div class="col-6 col-sm-4">
                                                                <label class="imagecheck mb-4">
                                                                    <input <?php echo ($admin_data[0]->theme == 'theme-3') ? 'checked' : '' ?> name="theme" type="radio" value="theme-3" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img03.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                            <div class="col-6 col-sm-4">
                                                                <label class="imagecheck mb-4">
                                                                    <input <?php echo ($admin_data[0]->theme == 'theme-4') ? 'checked' : '' ?> name="theme" type="radio" value="theme-4" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img04.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                            <div class="col-6 col-sm-4">
                                                                <label class="imagecheck mb-4">
                                                                    <input <?php echo ($admin_data[0]->theme == 'theme-5') ? 'checked' : '' ?> name="theme" type="radio" value="theme-5" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img05.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                            <div class="col-6 col-sm-4">
                                                                <label class="imagecheck mb-4">
                                                                    <input <?php echo ($admin_data[0]->theme == 'theme-6') ? 'checked' : '' ?> name="theme" type="radio" value="theme-6" class="imagecheck-input"  />
                                                                    <figure class="imagecheck-figure">
                                                                        <img src="admin_asset/img/theme/img06.jpg" alt="theme" class="imagecheck-image">
                                                                    </figure>
                                                                </label>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="error-text">
                                                        <?php
                                                        if (form_error('theme')) {
                                                            echo form_error('theme');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="theme_btn">Save</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                elseif ($type == 'payment'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <div class="card" id="payment-card">
                                            <div class="card-header">
                                                <h4>Payment Settings</h4>
                                            </div> 
                                            <div class="card-body">
                                                <div class="alert alert-info alert-has-icon p-4">
                                                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                                    <div class="alert-body">
                                                        <div class="alert-title">Attention!</div> 
                                                        <p>Hello,
                                                            Sorry for inconvenience of service. We will provide <strong>Payment functionality</strong> in our next update. Stay tuned with us.</p> 
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div> 
                                    <?php
                                    endif;
                                elseif ($type == 'language'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?> 
                                        <div class="card" id="language-card">
                                            <div class="card-header">
                                                <h4>Language Settings</h4>
                                            </div> 
                                            <div class="card-body">
                                                <div class="alert alert-info alert-has-icon p-4">
                                                    <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
                                                    <div class="alert-body">
                                                        <div class="alert-title">Attention!</div> 
                                                        <p>Hello,
                                                            Sorry for inconvenience of service. We will provide <strong>Language functionality</strong> in our next update. Stay tuned with us.</p> 
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div> 
                                    <?php
                                    endif;
                                elseif ($type == 'backup'):
                                    if (!$superAdmin && (!in_array($type, $allowed_feature))):
                                        $this->load->view('admin/common/access_denied');
                                    else:
                                        ?>
                                        <form method="post" name="backup_form" novalidate=""> 
                                            <div class="card" id="backup-card">
                                                <div class="card-header">
                                                    <h4>MYSQL Database Backup Settings</h4>
                                                </div> 
                                                <div class="card-body row">
                                                    <div class="form-group col-md-6">
                                                        <label class="control-label">Select Table Option</label> <br/>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="all_tab" onclick="$('#table_list').hide();" checked="" value="all" name="type" class="custom-control-input">
                                                            <label class="custom-control-label" for="all_tab">All Tables</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="sele_tab" onclick="$('#table_list').show();" value="select_table" name="type" class="custom-control-input">
                                                            <label class="custom-control-label" for="sele_tab">Selected Tables</label>
                                                        </div> 
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('type')) {
                                                                echo form_error('type');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div> 
                                                    <div class="form-group col-md-6">
                                                        <label class="control-label">Export as</label>
                                                        <select name="export" class="form-control select2"> 
                                                            <option>zip</option>
                                                            <option>gzip</option>
                                                            <option>txt</option>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('export')) {
                                                                echo form_error('export');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>  
                                                    <div class="form-group col-md-12" id="table_list" style="display: none">
                                                        <label class="control-label">Select Table From Below List</label>
                                                        <select multiple name="table[]" class="form-control select2"> 
                                                            <?php
                                                            $tables = $this->db->list_tables();
                                                            foreach ($tables as $table) {
                                                                echo '<option value="' . $table . '">' . $table . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                        <div class="error-text">
                                                            <?php
                                                            if (form_error('table')) {
                                                                echo form_error('table');
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>   
                                                </div>
                                                <div class="card-footer bg-whitesmoke text-md-right pt-10 pb-10">
                                                    <button type="submit" class="mr-2 btn-hover-shine btn btn-shadow btn-info" value="send" name="backup_btn">Download Now</button>
                                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                    endif;
                                endif;
                                ?> 
                            </div>
                        </div>
                    </div>
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer        ?> 
        </div>
    </div>    
    <?php
    if ($type == 'social'):
        ?>
        <!-- Country Code Script -->
        <script src="admin_asset/modules/country_code/build/js/intlTelInput.js"></script>
        <script>
            // Enable Country Code Script
            var input = document.querySelector("#whatsapp_country_code");
            window.intlTelInput(input, {
                utilsScript: "admin_asset/modules/country_code/build/js/utils.js"
            });
        </script>
        <?php
    endif;
    $alert_data['success'] = $success;
    $alert_data['error'] = $error;
    $this->load->view('admin/common/alert', $alert_data);  // Load Notification Alert Message & Footer script
    ?>  
</body> 