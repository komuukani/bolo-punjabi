<?php
echo $page_head;  //  Load Head Link and Scripts  
// Getting All field of tbl_control table and list it in a submenu
$control_data = $this->md->select('tbl_control_feature');
$fields = $this->db->list_fields('tbl_control_feature');
$superAdmin = true;
if ($admin_data):
    if ($admin_data[0]->admin_id != 1):
        $superAdmin = false;
        $admin_menu = $this->md->select_where('tbl_permission_assign', array('role_id' => $admin_data[0]->user_role_id, 'type' => 'feature'));   // get current user's permisison   
    endif;
endif; 
?> 
<body> 
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <?php echo $page_header; //  Load Header   ?>  
            <?php echo $page_sidebar; //  Load Sidebar   ?> 
            <!-- >> Main Content Start
            ================================================== -->
            <div class="main-content">
                <section class="section">
                    <?php echo $page_breadcrumb; // Load Breadcrumb  ?> 
                    <div class="section-body">
                        <h2 class="section-title">Overview</h2>
                        <p class="section-lead">
                            Organize and adjust all settings about this site.
                        </p>
                        <div class="row">
                            <?php
                            if ($superAdmin):
                                foreach ($fields as $fields_val):
                                    if ($fields_val != 'control_feature_id' && $control_data[2]->$fields_val):
                                        ?>
                                        <div class="col-lg-6">
                                            <div class="card card-large-icons">
                                                <div class="card-icon bg-primary text-white">
                                                    <i class="<?php echo $control_data[1]->$fields_val; ?>"></i>
                                                </div>
                                                <div class="card-body">
                                                    <h4><?php echo ucfirst($fields_val); ?></h4>
                                                    <p><?php echo $control_data[0]->$fields_val; ?> </p>
                                                    <a href="<?php echo base_url('admin-setting/' . strtolower($fields_val)); ?>" class="card-cta">Change Setting <i class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endif;
                                endforeach;
                            else:
                                $per_id = array();
                                foreach ($admin_menu as $ext_per) {
                                    $per = json_decode($ext_per->permission_id);
                                    foreach ($per as $per_val):
                                        $per_id[] = $per_val;
                                    endforeach;
                                }
                                $allowed_menu = array_unique($per_id);
                                sort($allowed_menu);    // Sort Array 
                                if (!empty($allowed_menu)):
                                    foreach ($allowed_menu as $per_val):
                                        $exist_prmsn = $this->md->select_where('tbl_permission', array('permission_id' => $per_val));
                                        $field_data = $this->md->my_query("SELECT `" . $exist_prmsn[0]->feature . "` FROM `tbl_control_feature` ")->result_array();
                                        ?>
                                        <div class="col-lg-6">
                                            <div class="card card-large-icons">
                                                <div class="card-icon bg-primary text-white">
                                                    <i class="<?php echo $field_data[1][$exist_prmsn[0]->feature] ?>"></i>
                                                </div>
                                                <div class="card-body">
                                                    <h4><?php echo ucfirst($exist_prmsn ? $exist_prmsn[0]->feature : ''); ?></h4>
                                                    <p><?php echo $field_data[2][$exist_prmsn[0]->feature] ?></p>
                                                    <a href="<?php echo base_url('admin-setting/' . strtolower($exist_prmsn[0]->feature)); ?>" class="card-cta">Change Setting <i class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                            endif;
                            ?> 
                        </div>
                    </div>
                </section>
            </div> 
            <!-- << Main Content End
            ================================================== -->
            <?php echo $page_footer;  //  Load Footer   ?> 
        </div>
    </div> 
    <?php echo $page_footerscript; // Load Footer script ?>
</body> 