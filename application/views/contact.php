<?php
echo $page_head;
$admin_data = $this->md->select('tbl_web_data')[0];
$web_data = ($web_data) ? $web_data[0] : '';
?>
<body>
<div class="main-wrapper">
 <?php echo $page_header; ?>
<?php echo $page_breadcumb; ?>

    <div class="edu-contact-us-area eduvibe-contact-us edu-section-gap bg-color-white">
        <div class="container eduvibe-animated-shape">
            <div class="row g-5">
                <div class="col-lg-12">
                    <div class="section-title text-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <span class="pre-title">Need Help?</span>
                        <h3 class="title">Get In Touch With us</h3>
                    </div>
                </div>
            </div>
            <div class="row g-5 mt--20">
                <div class="col-lg-6">
                    <div class="contact-info pr--70 pr_lg--0 pr_md--0 pr_sm--0">
                        <div class="row g-5">
                            <!-- Start Contact Info  -->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="contact-address-card-1 website">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="ri-global-line"></i>
                                        </div>
                                        <div class="content">
                                            <h6 class="title">Our Website</h6>
                                            <p><a href="https://bolopunjabi.com" target="_blank">www.bolopunjabi.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Contact Info  -->

                            <!-- Start Contact Info  -->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
                                <div class="contact-address-card-1 phone">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="icon-Headphone"></i>
                                        </div>
                                        <div class="content">
                                            <h6 class="title">Call Us On</h6>
                                            <?php
                                            if ($web_data) {
                                                if ($web_data->phone) {
                                                    $phones = explode(",", $web_data->phone);
                                                    if ($phones) {
                                                        foreach ($phones as $phone) {
                                                            echo "<p><a href='tel:" . $phone . "'>" . $phone . "</a></p>";
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Contact Info  -->

                            <!-- Start Contact Info  -->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-sal-delay="250" data-sal="slide-up" data-sal-duration="800">
                                <div class="contact-address-card-1 email">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="icon-mail-open-line"></i>
                                        </div>
                                        <div class="content">
                                            <h6 class="title">Email Us</h6>
                                            <?php
                                            if ($web_data) {
                                                if ($web_data->email_address) {
                                                    $emails = explode(",", $web_data->email_address);
                                                    if ($emails) {
                                                        foreach ($emails as $email) {
                                                            echo "<p><a href='mailto:" . $email . "' target='_blank'>" . $email . "</a></p>";
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Contact Info  -->

                            <!-- Start Contact Info  -->
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12" data-sal-delay="300" data-sal="slide-up" data-sal-duration="800">
                                <div class="contact-address-card-1 location">
                                    <div class="inner">
                                        <div class="icon">
                                            <i class="icon-map-pin-line"></i>
                                        </div>
                                        <div class="content">
                                            <h6 class="title">Our Location</h6>
                                            <p><?php echo($admin_data->address); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Contact Info  -->

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <form class="rnt-contact-form rwt-dynamic-form row" method="POST">
                        <?php
                        if (isset($error)) {
                            ?>
                            <div class="alert alert-danger p-1">
                                <?php echo $error; ?>
                            </div>
                            <?php
                        }
                        if (isset($success)) {
                            ?>
                            <div class="alert alert-success p-1">
                                <?php echo $success; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input  name="name" id="fname" type="text" class="form-control form-control-lg" placeholder="Name*" value="<?php
                                if (set_value('fname') && !isset($success)) {
                                    echo set_value('fname');
                                }
                                ?>">
                                <div class="error-text">
                                    <?php
                                    if (form_error('fname')) {
                                        echo form_error('fname');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Email*" value="<?php
                                if (set_value('email') && !isset($success)) {
                                    echo set_value('email');
                                }
                                ?>">
                                <div class="error-text">
                                    <?php
                                    if (form_error('email')) {
                                        echo form_error('email');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-lg"  name="phone" id="phone" placeholder="Phone" value="<?php
                                if (set_value('phone') && !isset($success)) {
                                    echo set_value('phone');
                                }
                                ?>">
                                <div class="error-text">
                                    <?php
                                    if (form_error('phone')) {
                                        echo form_error('phone');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control" name="message"
                                          id="message" placeholder="Your Message"><?php
                                    if (set_value('message') && !isset($success)) {
                                        echo set_value('message');
                                    }
                                    ?></textarea>
                                <div class="error-text">
                                    <?php
                                    if (form_error('message')) {
                                        echo form_error('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <?php
                            if ($admin_data->captcha_visibility) :
                                echo '<div class="g-recaptcha" data-sitekey="' . $admin_data->captcha_site_key . '"></div>';
                            endif;
                            ?>
                        </div>
                        <div class="col-lg-12">
                            <button class="rn-btn edu-btn w-100" name="send" type="submit" value="Send">
                                <span>Submit Now</span><i class="icon-arrow-right-line-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
                <div class="shape-image scene shape-image-1">
                        <span data-depth="-2.2">
                            <img src="assets/images/shapes/shape-04-01.png" alt="Shape Thumb">
                        </span>
                </div>
                <div class="shape-image shape-image-2">
                    <img src="assets/images/shapes/shape-02-08.png" alt="Shape Thumb">
                </div>
                <div class="shape-image shape-image-3">
                    <img src="assets/images/shapes/shape-15.png" alt="Shape Thumb">
                </div>
            </div>
        </div>
    </div>

    <div class="edu-contact-map-area edu-section-gapBottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="map" data-sal="slide-up" data-sal-delay="150" data-sal-duration="800">
                        <?php echo($admin_data->map); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>