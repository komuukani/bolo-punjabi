<?php
echo $page_head;
?>
<body class="theme-color2 dark ltr">
<!-- template sections -->
<?php echo $page_header; ?>
<?php $this->load->view('mobileMenu'); ?>
<?php echo $page_breadcumb; ?>

<div class="space-top space-extra-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="accordion-area accordion mb-30" id="faqAccordion">  
                    <?php
                    if (empty($faq)) :
                        echo "Sorry, content not available";
                    else :
                        foreach ($faq as $key => $faq_data) {
                            ?>
                            <div class="accordion-card style4 <?php echo ($key == 0) ? 'active' : ''; ?>">
                                <div class="accordion-header" id="collapse-item-<?php echo $key; ?>">
                                    <button class="accordion-button <?php echo ($key == 0) ? '' : 'collapsed'; ?>"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapse-<?php echo $key; ?>"
                                            aria-expanded="<?php echo ($key == 0) ? 'true' : 'false'; ?>"
                                            aria-controls="collapse-<?php echo $key; ?>"><?php echo $key + 1; ?>.
                                        <?php echo $faq_data->question; ?>
                                    </button>
                                </div>
                                <div id="collapse-<?php echo $key; ?>"
                                     class="accordion-collapse collapse <?php echo ($key == 0) ? 'show' : ''; ?>"
                                     aria-labelledby="collapse-item-<?php echo $key; ?>"
                                     data-bs-parent="#faqAccordion">
                                    <div class="accordion-body style4">
                                        <p class="faq-text"><?php echo $faq_data->answer; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $page_footer; ?>

<?php echo $page_footerscript; ?>
</body>