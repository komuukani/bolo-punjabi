<?php
$admin_data = $this->md->select('tbl_admin')[0];
$web_data = ($web_data) ? $web_data[0] : '';
$meta_data = $this->md->select_where('tbl_seo', array('page' => $page));
$category = $this->md->select('tbl_category');
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<!doctype html>
<html class="no-js" lang="en">
<!--<![endif]-->
<?php ob_start(); ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="<?php echo base_url(); ?>"/>
    <?php
    if (!empty($meta_data)) {
        ?>
        <meta name="author" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="title" content="<?php echo $meta_data ? $meta_data[0]->title : ''; ?>">
        <meta name="description" content="<?php echo $meta_data ? $meta_data[0]->description : ''; ?>">
        <meta name="keywords" content="<?php echo $meta_data ? $meta_data[0]->keyword : ''; ?>">
        <title><?php echo $meta_data ? ($meta_data[0]->title . " | " . $web_data->project_name) : (' | ' . $web_data->project_name); ?></title>
        <?php
    } else {
        ?>
        <title><?php echo $page_title . ' | ' . $web_data->project_name; ?></title>
        <?php
        if (isset($blog_data)) {
            ?>l
            <meta name="title"
                  content="<?php echo $blog_data[0]->meta_title ? $blog_data[0]->meta_title : $blog_data[0]->title; ?>">
            <meta name="description" content="<?php echo $blog_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $blog_data[0]->meta_keyword; ?>">
            <?php
        }
        if (isset($product_data)) {
            ?>
            <meta name="title"
                  content="<?php echo $product_data[0]->meta_title ? $product_data[0]->meta_title : $product_data[0]->title; ?>">
            <meta name="description" content="<?php echo $product_data[0]->meta_desc; ?>">
            <meta name="keywords" content="<?php echo $product_data[0]->meta_keyword; ?>">
            <?php
        }
    }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo $page_title . " - " . $web_data->project_name; ?></title>
    <!-- Favicon Icon -->
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url(($web_data) ? $web_data->favicon : FILENOTFOUND); ?>">
    <link href="<?php echo base_url('admin_asset/'); ?>css/responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/vendor/remixicon.css">
    <link rel="stylesheet" href="assets/css/vendor/eduvibe-font.css">
    <link rel="stylesheet" href="assets/css/vendor/magnifypopup.css">
    <link rel="stylesheet" href="assets/css/vendor/slick.css">
    <link rel="stylesheet" href="assets/css/vendor/odometer.css">
    <link rel="stylesheet" href="assets/css/vendor/lightbox.css">
    <link rel="stylesheet" href="assets/css/vendor/animation.css">
    <link rel="stylesheet" href="assets/css/vendor/jqueru-ui-min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<!-- page wrapper -->
<?php $page_head = ob_get_clean(); ?>

<?php
/*
* HEADER
*/
ob_start();
?>
<header class="edu-header  header-sticky <?php echo ($active_page == 'index') ? 'header-transparent header-style-2 header-default' : 'disable-transparent'; ?>">
    <?php if ($active_page != 'index'){ ?>
    <div class="container">
        <?php } ?>
        <div class="row <?php echo $active_page == 'index' ? 'align-items-center' : 'justify-content-between'; ?>">
            <div class="<?php echo ($active_page == 'index') ? 'col-lg-4 col-xl-2 col-md-6 col-6 ' : 'col-lg-6 col-xl-2 col-md-6 col-6'; ?> ">
                <div class="logo">
                    <a href="<?php echo base_url(); ?>">
                        <img class="logo-light"
                             src="<?php echo base_url(($web_data) ? $web_data->logo : FILENOTFOUND); ?>"
                             alt="Site Logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 d-none d-xl-block">
                <nav class="mainmenu-nav d-none d-lg-block">
                    <ul class="mainmenu">
                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li><a href="<?php echo base_url('aboutus'); ?>">About us</a></li>
                        <li><a href="<?php echo base_url('levels'); ?>">Courses</a></li>
                        <li><a href="<?php echo base_url('contact'); ?>">Contact us</a></li>
                    </ul>
                </nav>
            </div>
            <?php if ($active_page == 'index') { ?>
                <div class="col-lg-8 col-xl-4 col-md-6 col-6">
                    <div class="header-right d-flex justify-content-end">
                        <div class="header-menu-bar">
                            <div class="quote-icon quote-search">
                                <button class="white-box-icon search-trigger header-search"><i
                                            class="ri-search-line"></i></button>
                            </div>
                            <div class="quote-icon quote-user d-none d-md-block ml--15 ml_sm--5">
                                <a class="edu-btn btn-medium left-icon header-button" href="##"><i
                                            class="ri-user-line"></i>Register</a>
                            </div>
                            <div class="quote-icon quote-user d-block d-md-none ml--15 ml_sm--5">
                                <a class="white-box-icon" href="##"><i class="ri-user-line"></i></a>
                            </div>
                        </div>

                        <div class="mobile-menu-bar ml--15 ml_sm--5 d-block d-xl-none">
                            <div class="hamberger">
                                <button class="white-box-icon hamberger-button header-menu">
                                    <i class="ri-menu-line"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-lg-8 col-xl-4 col-md-6 col-6">
                    <div class="header-right">
                        <div class="header-menu-bar mt-20 d-flex justify-content-end align-items-center">
                            <div class="quote-icon quote-search">
                                <button class="white-box-icon search-trigger header-search"><i
                                            class="ri-search-line"></i></button>
                            </div>
                            <div class="quote-icon quote-user d-none d-md-block ml--15 ml_sm--5">
                                <a class="edu-btn btn-medium left-icon header-button" href="##"><i
                                            class="ri-user-line"></i>Register</a>
                            </div>
                            <div class="hamberger d-block d-md-none">
                                <button class="white-box-icon hamberger-button header-menu">
                                    <i class="ri-menu-line"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            <?php } ?>
        </div>
        <?php if ($active_page != 'index'){ ?>
    </div>
<?php } ?>
</header>
<div class="popup-mobile-menu">
    <div class="inner">
        <div class="header-top">
            <div class="logo">
                <a href="index.html">
                    <img src="assets/images/logo/logo.png" alt="Site Logo">
                </a>
            </div>
            <div class="close-menu">
                <button class="close-button">
                    <i class="ri-close-line"></i>
                </button>
            </div>
        </div>
        <ul class="mainmenu">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url('aboutus'); ?>">About us</a></li>
            <li><a href="<?php echo base_url('levels'); ?>">Courses</a></li>
            <li><a href="<?php echo base_url('contact'); ?>">Contact us</a></li>
        </ul>
    </div>
</div>
<!-- Start Search Popup  -->
<div class="edu-search-popup">
    <div class="close-button">
        <button class="close-trigger"><i class="ri-close-line"></i></button>
    </div>
    <div class="inner">
        <form class="search-form" action="#">
            <input type="text" class="eduvibe-search-popup-field" placeholder="Search Here...">
            <button class="submit-button"><i class="icon-search-line"></i></button>
        </form>
    </div>
</div>
<!-- End Search Popup  -->
<!-- Main Header Area End Here -->
<?php $page_header = ob_get_clean(); ?>

<?php
/*
* BREADCRUMB
*/
ob_start();
?>
<div class="edu-breadcrumb-area breadcrumb-style-1 ptb--60 ptb_md--40 ptb_sm--40 bg-image">
    <div class="container eduvibe-animated-shape">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner text-start">
                    <div class="page-title">
                        <h3 class="title"><?php echo $page_title; ?></h3>
                    </div>
                    <nav class="edu-breadcrumb-nav">
                        <ol class="edu-breadcrumb d-flex justify-content-start liststyle">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class="separator"><i class="ri-arrow-drop-right-line"></i></li>
                            <?php
                            if($page == 'courses_detail'){
                                ?>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('levels'); ?>">Courses</a></li>
                                <li class="separator"><i class="ri-arrow-drop-right-line"></i></li>
                                <?php
                            }
                            ?>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
                <div class="shape-image shape-image-1">
                    <img src="assets/images/shapes/shape-11-07.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-2">
                    <img src="assets/images/shapes/shape-01-02.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-3">
                    <img src="assets/images/shapes/shape-03.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-4">
                    <img src="assets/images/shapes/shape-13-12.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-5">
                    <img src="assets/images/shapes/shape-36.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-6">
                    <img src="assets/images/shapes/shape-05-07.png" alt="Shape Thumb"/>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $page_breadcumbs = ob_get_clean(); ?>

<?php
/*
* FOOTER
*/
ob_start();
?>

<!-- Start Footer Area  -->
<footer class="eduvibe-footer-one edu-footer footer-style-default">
    <div class="footer-top">
        <div class="container eduvibe-animated-shape">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="edu-footer-widget">
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>">
                                <img class="logo-light"
                                     src="<?php echo base_url(($web_data) ? $web_data->logo : FILENOTFOUND); ?>"
                                     alt="Site Logo">
                            </a>
                        </div>
                        <p class="description">It is a long established fact that a reader will be distracted by the
                            readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        <ul class="social-share">
                            <li><a href="<?php echo $web_data->facebook; ?>"><i class="icon-Fb"></i></a></li>
                            <li><a href="<?php echo $web_data->linkedin; ?>"><i class="icon-linkedin"></i></a></li>
                            <li><a href="<?php echo $web_data->youtube; ?>"><i class="icon-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="edu-footer-widget explore-widget">
                        <h5 class="widget-title">Explore</h5>
                        <div class="inner">
                            <ul class="footer-link link-hover">
                                <li><a href="<?php echo base_url(); ?>"><i class="icon-Double-arrow"></i>Home</a></li>
                                <li><a href="<?php echo base_url('aboutus'); ?>"><i class="icon-Double-arrow"></i>About
                                        Us</a></li>
                                <li><a href="<?php echo base_url('levels'); ?>"><i class="icon-Double-arrow"></i>Courses</a>
                                </li>
                                <li><a href="<?php echo base_url('contact'); ?>"><i class="icon-Double-arrow"></i>Contact
                                        us</a></li>
                                <li><a href="<?php echo base_url('policy'); ?>"><i class="icon-Double-arrow"></i>Privacy
                                        Policy</a></li>
                                <li><a href="<?php echo base_url('terms'); ?>"><i class="icon-Double-arrow"></i>Terms &
                                        Condition</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="edu-footer-widget quick-link-widget">
                        <h5 class="widget-title">Our Categories</h5>
                        <div class="inner">
                            <ul class="footer-link link-hover">
                                <?php
                                if (!empty($category)):
                                    foreach ($category as $category_data):
                                        ?>
                                        <li><a href="<?php echo base_url('courses'); ?>"><i
                                                        class="icon-Double-arrow"></i><?php echo $category_data->title; ?>
                                            </a></li>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="edu-footer-widget">
                        <h5 class="widget-title">Contact Info</h5>
                        <div class="inner">
                            <div class="widget-information">
                                <ul class="information-list">
                                    <li><i class="icon-map-pin-line"></i><?php echo $web_data->address; ?>
                                    </li>
                                    <li><i class="icon-phone-fill"></i><a
                                                href="tel:<?php echo $web_data->phone; ?>"><?php echo $web_data->phone; ?></a>
                                    </li>
                                    <li><i class="icon-mail-line-2"></i><a target="_blank"
                                                                           href="mailto:<?php echo $web_data->email_address; ?>"><?php echo $web_data->email_address; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="shape-dot-wrapper shape-wrapper d-md-block d-none">
                <div class="shape-image shape-image-1">
                    <img src="assets/images/shapes/shape-21-01.png" alt="Shape Thumb"/>
                </div>
                <div class="shape-image shape-image-2">
                    <img src="assets/images/shapes/shape-35.png" alt="Shape Thumb"/>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-area copyright-default">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner text-center">
                        <p><?php echo $web_data->footer; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer Area  -->
<?php $page_footer = ob_get_clean(); ?>


<?php
/*
* FOOTER SCRIPT
*/
ob_start();
?>
<div class="rn-progress-parent">
    <svg class="rn-back-circle svg-inner" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
    </svg>
</div>
<!-- JS
============================================ -->
<!-- Modernizer JS -->
<script src="assets/js/vendor/modernizr.min.js"></script>
<!-- jQuery JS -->
<script src="assets/js/vendor/jquery.js"></script>
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/vendor/sal.min.js"></script>
<script src="assets/js/vendor/backtotop.js"></script>
<script src="assets/js/vendor/magnifypopup.js"></script>
<script src="assets/js/vendor/slick.js"></script>
<script src="assets/js/vendor/countdown.js"></script>
<script src="assets/js/vendor/jquery-appear.js"></script>
<script src="assets/js/vendor/odometer.js"></script>
<script src="assets/js/vendor/isotop.js"></script>
<script src="assets/js/vendor/imageloaded.js"></script>
<script src="assets/js/vendor/lightbox.js"></script>
<script src="assets/js/vendor/wow.js"></script>
<script src="assets/js/vendor/paralax.min.js"></script>
<script src="assets/js/vendor/paralax-scroll.js"></script>
<script src="assets/js/vendor/jquery-ui.js"></script>
<script src="assets/js/vendor/tilt.jquery.min.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>
<script type="text/javascript">
    /*--------------------
     NOTIFY.JS INITIALIZATION
     ---------------------*/
    $.notify.defaults({
        position: "bottom right",
        gap: '5',
        autoHideDelay: 7000
    });

    // Set custom Notify Alert
    function setNotifyAlert(title, message, icon, type) {
        $.notify({
            icon: icon,
            title: title,
            message: message,
        }, {
            element: "body",
            position: null,
            type: type,
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: true,
            placement: {
                from: "top",
                align: "right",
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            animate: {
                enter: "animated fadeInDown",
                exit: "animated fadeOutUp",
            },
            icon_type: "class",
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="btn-close" data-notify="dismiss"></button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-info progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                "</div>" +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                "</div>",
        });
    }
</script>
<?php
if (isset($success)) {
    ?>
    <script>
        setNotifyAlert('Success!', '<?php echo $success; ?>', 'fa fa-check', 'info');
    </script>
    <?php
}
if (isset($error)) {
    ?>
    <script>
        setNotifyAlert('Oops!', '<?php echo $error; ?>', 'fa fa-warning', 'info');
    </script>
    <?php
}
$page_footerscript = ob_get_clean(); ?>
<?php
$this->load->view($page, array(
    'page_title' => $page_title,
    'page_head' => $page_head,
    'page_header' => $page_header,
    'page_breadcumb' => ($page_breadcumb) ? $page_breadcumbs : '',
    'page_footer' => $page_footer,
    'page_footerscript' => $page_footerscript
));
?>
<style>
    .error-text {
        width: 100%;
        margin-top: 5px;
        font-size: 13px;
        color: #dc3545;
        background: #ff4073e6;
        padding-left: 10px;
    }

    .error-text p {
        margin-bottom: 0 !important;
        color: #ffe7e9 !important;
        margin-left: 12px;
        font-weight: 500;
    }

</style>
</html>