<?php
echo $page_head;

$blogs = $this->md->select_limit_order('tbl_blog', 3, 'blog_id', 'desc');
$products = $this->md->select_limit_order('tbl_product', 5, 'product_id', 'desc', array('featured' => 1));
$reviews = $this->md->select('tbl_review');
$category = $this->md->select('tbl_category');

$web_data = ($web_data) ? $web_data[0] : '';
$active_page = $this->uri->segment(1) ? $this->uri->segment(1) : 'index';
?>
<body>
<div class="main-wrapper">
<?php echo $page_header; ?>
<?php //$this->load->view('mobileMenu'); ?>
<?php //$this->load->view('slider'); ?>
<!-- Start Sldier Area  -->
<div class="slider-area banner-style-2 bg-image d-flex align-items-center">
    <div class="container">
        <div class="row g-5 align-items-center">
            <div class="col-lg-6 col-md-12">
                <div class="inner">
                    <div class="content">
                        <span class="pre-title" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">Better Learning Future With Us</span>
                        <h1 class="title" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
                            Committed To Learn Excellence In Punjabi</h1>
                        <p class="description" data-sal-delay="250" data-sal="slide-up" data-sal-duration="800">
                            It is long established that a reader will be distracted by the <br /> readable popular and best content.</p>
                        <div class="read-more-btn" data-sal-delay="300" data-sal="slide-up" data-sal-duration="800">
                            <a class="edu-btn" href="<?php echo base_url('contact')?>">Get Started Today <i class="icon-arrow-right-line-right"></i></a>
                        </div>
                        <div class="arrow-sign d-lg-block d-none">
                            <img src="assets/images/banner/banner-02/arrow.png" alt="Banner Images" data-sal-delay="150" data-sal="fade" data-sal-duration="800">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="banner-thumbnail">
                    <img class="girl-thumb" src="assets/images/banner/banner-02/banner-01.png" alt="Girl Images" data-sal-delay="150" data-sal="fade" data-sal-duration="800" />
                </div>
                <div class="banner-bg d-lg-block d-none">
                    <img class="girl-bg" src="assets/images/banner/banner-02/girl-bg.png" alt="Girl Background" data-sal-delay="150" data-sal="fade" data-sal-duration="800" />
                </div>
            </div>
        </div>

        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-19.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-05-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-3">
                <img src="assets/images/shapes/shape-19-01.png" alt="Shape Thumb" />
            </div>
        </div>
    </div>
</div>
<!-- End Sldier Area  -->

<!-- Start About Area  -->
<div id="about-us" class="edu-about-area about-style-1 edu-section-gap bg-color-white">
    <div class="container">
        <div class="row g-5">
            <div class="col-lg-6">
                <div class="about-image-gallery">
                    <img class="image-1" src="assets/images/about/about-09/about-image-01.jpg" alt="About Main Thumb" />
                    <div class="image-2"><img src="assets/images/about/about-09/about-image-02.jpg" alt="About Parallax Thumb" /></div>
                    <div class="badge-inner"><img class="image-3" src="assets/images/about/about-09/badge.png" alt="About Circle Thumb" /></div>
                    <div class="shape-image shape-image-1"><img src="assets/images/shapes/shape-04-01.png" alt="Shape Thumb" /></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="inner">
                    <div class="section-title" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <span class="pre-title">About Us</span>
                        <h3 class="title">Creating A Community Of Life Long Learners</h3>
                    </div>
                    <p class="description" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nunc null liobortis nibh porttitor. Facilisi arcu, nibh vel risus, morbi pharetra.
                    </p>
                    <div class="about-feature-list">
                        <!-- Start Single Feature  -->
                        <div class="our-feature" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                            <div class="icon">
                                <i class="icon-Hand---Book"></i>
                            </div>
                            <div class="feature-content">
                                <h6 class="feature-title">Flexible Classes</h6>
                                <p class="feature-description">It is a long established fact that a reader will be distracted by this on readable content of when looking at its layout.</p>
                            </div>
                        </div>
                        <!-- End Single Feature  -->

                        <!-- Start Single Feature  -->
                        <div class="our-feature" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                            <div class="icon">
                                <i class="icon-Campus"></i>
                            </div>
                            <div class="feature-content">
                                <h6 class="feature-title">Learn From Anywhere</h6>
                                <p class="feature-description">It is a long established fact that a reader will be distracted by this on readable content of when looking at its layout.</p>
                            </div>
                        </div>
                        <!-- End Single Feature  -->
                    </div>
                    <div class="read-more-btn" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <a class="edu-btn" href="<?php echo base_url('aboutus')?>">Know About Us<i class="icon-arrow-right-line-right"></i></a>
                    </div>
                    <div class="shape shape-6 about-parallax-2 d-xl-block d-none">
                        <img src="assets/images/shapes/shape-07.png" alt="Shape Thumb" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start Service Area  -->
<div class="home-one-cat edu-service-area service-wrapper-1 edu-section-gap bg-image">
    <div class="container eduvibe-animated-shape">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <span class="pre-title">Course Categories</span>
                    <h3 class="title">Popular Topics To Learn</h3>
                </div>
            </div>
        </div>
        <div class="row g-5 mt--25">
            <?php
            $fields = $this->md->select('tbl_category');
            if (!empty($fields)):
            foreach ($fields as $fields_val):
            ?>
            <!-- Start Service Card  -->
            <div class="col-lg-3 col-md-6 col-sm-6 col-12" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                <div class="service-card service-card-1 radius-small">
                    <div class="inner">
                        <div class="thumbnail">
                            <a href="#">
                                <img src="<?php echo base_url(($fields_val->path) ? $fields_val->path : FILENOTFOUND); ?>" alt="<?php echo $fields_val->title; ?>">
                            </a>
                        </div>
                        <div class="content">
<!--                            <span class="course-total">23 Course</span>-->
                            <h6 class="title"><a href="#"><?php echo $fields_val->title; ?></a></h6>
                            <p class="description"><?php echo $fields_val->description; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Service Card  -->
            <?php
            endforeach;
            endif;
            ?>
        </div>
        <div class="row text-center mt--60">
            <div class="col-lg-12">
                <div class="load-more-btn">
                    <a class="edu-btn" href="<?php echo base_url('courses')?>">All Categories<i
                                class="icon-arrow-right-line-right"></i></a>
                </div>
            </div>
        </div>

        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-03-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-08.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-3">
                <img src="assets/images/shapes/shape-04-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-4">
                <img src="assets/images/shapes/shape-03-02.png" alt="Shape Thumb" />
            </div>
        </div>

    </div>
</div>
<!-- End Service Area  -->

<!-- Start Newsletter Area  -->
<div class="edu-newsletter-area bg-image newsletter-style-3 edu-section-gap bg-color-primary">
    <div class="container">
        <div class="row g-5 align-items-center">
            <div class="col-lg-6">
                <div class="inner">
                    <div class="section-title text-white text-start" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <span class="pre-title">Let Us Help</span>
                        <h3 class="title">Finding Your Right Courses</h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="newsletter-right-content d-block d-sm-flex align-items-center justify-content-start justify-content-lg-end">
                    <div class="contact-btn">
                        <a class="edu-btn btn-white" href="<?php echo base_url('contact')?>">Get Started Now<i class="icon-arrow-right-line-right"></i></a>
                    </div>
                    <div class="contact-info">
                        <a href="tel:<?php echo $web_data->phone?>"><i class="icon-phone-line"></i><?php echo $web_data->phone;?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Newsletter Area  -->

<!-- Start Testimonial Area  -->
<div class="eedu-testimonial-area eduvibe-home-two-testimonial bg-color-white testimonial-card-box-bg edu-section-gap position-relative bg-image">
    <div class="container eduvibe-animated-shape">
        <div class="row g-5">
            <div class="col-lg-12">
                <div class="section-title text-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <span class="pre-title">TESTIMONIALS</span>
                    <h3 class="title">Our Lovely Students Feedback</h3>
                </div>
            </div>
        </div>

        <div class="edu-testimonial-activation testimonial-item-3 mt--40 edu-slick-button">
            <?php
            $fields = $this->md->select('tbl_review');
            if (!empty($fields)):
                foreach ($fields as $fields_val):
            ?>
            <!-- Start Tastimonial Card  -->
            <div class="testimonial-card-box">
                <div class="inner">
                    <div class="client-info">
                        <div class="thumbnail">
                            <img src="<?php echo base_url(($fields_val) ? $fields_val->path : FILENOTFOUND); ?>" alt="<?php echo $fields_val->username;?>">
                        </div>
                        <div class="content">
                            <h6 class="title"><?php echo $fields_val->username;?></h6>
                        </div>
                    </div>
                    <p class="description">“ <?php echo $fields_val->review;?> ”</p>
                </div>
            </div>
            <!-- End Tastimonial Card  -->
            <?php
            endforeach;
            endif;
            ?>
        </div>

        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-23.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-14-02.png" alt="Shape Thumb" />
            </div>
        </div>

    </div>
</div>
<!-- End Testimonial Area  -->

<div class="eduvibe-home-two-counter edu-counterup-area counterup-wrapper-1 edu-section-gapBottom bg-color-white">
    <!-- Start Counterup Area  -->
    <div class="container eduvibe-animated-shape">
        <div class="row gy-5 align-items-center">
            <div class="col-lg-6">
                <div class="row g-5 pr--75 pr_md--0 pr_sm--0">
                    <!-- Start Single Counterup  -->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="edu-counterup">
                            <div class="inner">
                                <div class="icon">
                                    <img src="assets/images/icons/winner-04.png" alt="Icons Images">
                                </div>
                                <div class="content">
                                    <h3 class="counter"><span class="odometer" data-count="449">00</span>
                                    </h3>
                                    <span>Learners & counting</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Counterup  -->

                    <!-- Start Single Counterup  -->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="edu-counterup">
                            <div class="inner">
                                <div class="icon">
                                    <img src="assets/images/icons/winner-05.png" alt="Icons Images">
                                </div>
                                <div class="content">
                                    <h3 class="counter"><span class="odometer" data-count="330">00</span>
                                    </h3>
                                    <span>Courses & Video</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Counterup  -->

                    <!-- Start Single Counterup  -->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="edu-counterup">
                            <div class="inner">
                                <div class="icon">
                                    <img src="assets/images/icons/winner-06.png" alt="Icons Images">
                                </div>
                                <div class="content">
                                    <h3 class="counter"><span class="odometer" data-count="275">50</span>
                                    </h3>
                                    <span>Certified Students</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Counterup  -->

                    <!-- Start Single Counterup  -->
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="edu-counterup">
                            <div class="inner">
                                <div class="icon">
                                    <img src="assets/images/icons/winner-07.png" alt="Icons Images">
                                </div>
                                <div class="content">
                                    <h3 class="counter"><span class="odometer" data-count="378">00</span>
                                    </h3>
                                    <span>Winning Award</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Counterup  -->

                </div>
            </div>
            <div class="col-lg-6">
                <div class="choose-us-2">
                    <div class="inner">
                        <div class="section-title text-left" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                            <span class="pre-title" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">Why Choose Us</span>
                            <h3 class="title">Creating A Community Of Life Long Learners</h3>
                        </div>
                        <p class="description mt--40 mb--30">There are many variations of passages of the
                            Ipsum available, but the majority have suffered alteration in some form, by
                            injected humour.</p>
                        <div class="feature-style-4">
                            <div class="edu-feature-list" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="icon">
                                    <i class="icon-Smile"></i>
                                </div>
                                <div class="content">
                                    <h6 class="title">Trusted By Thousands</h6>
                                    <p>There are many variations of passages of the Ipsum available, but the majority have suffered alteration</p>
                                </div>
                            </div>

                            <div class="edu-feature-list color-var-2" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                                <div class="icon">
                                    <i class="icon-Support"></i>
                                </div>
                                <div class="content">
                                    <h6 class="title">Unlimited Resources With Strong Support</h6>
                                    <p>There are many variations of passages of the Ipsum available, but the majority have suffered alteration</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shape-dot-wrapper shape-wrapper d-xl-block d-none">
            <div class="shape-image shape-image-1">
                <img src="assets/images/shapes/shape-04-01.png" alt="Shape Thumb" />
            </div>
            <div class="shape-image shape-image-2">
                <img src="assets/images/shapes/shape-11.png" alt="Shape Thumb" />
            </div>
        </div>
    </div>
    <!-- End Counterup Area  -->
</div>


<?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>