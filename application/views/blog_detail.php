<?php
echo $page_head;
$web_data = ($web_data) ? $web_data[0] : '';
$blogs = $this->md->select_limit_order('tbl_blog', 5, 'blog_id', 'desc');
?>
<body>
<?php
echo $page_head;
?>
<body class="theme-color2 dark ltr">
<?php echo $page_header; ?>
<?php $this->load->view('mobileMenu'); ?>
<?php echo $page_breadcumb; ?>

<section class="masonary-blog-section">
    <div class="container">
        <div class="row g-4">
            <div class="col-lg-9 col-md-8 order-md-1 ratio_square">
                <div class="row g-4">
                    <div class="col-12">
                        <div class="blog-details">
                            <div class="blog-image-box">
                                <img src="<?php echo base_url($blog_data ? $blog_data[0]->path : FILENOTFOUND); ?>"
                                     alt="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"
                                     title="<?php echo $blog_data ? $blog_data[0]->title : ''; ?>"
                                     style="width: 100%;" class="card-img-top"/>
                                <div class="blog-title">
                                    <div class="sharethis-inline-share-buttons"></div>
                                </div>
                            </div>

                            <div class="blog-detail-contain">
                                <span
                                    class="font-light"><?php echo date('d-M-Y', strtotime($blog_data ? $blog_data[0]->blogdate : '')); ?></span>
                                <h2 class="card-title"><?php echo $blog_data ? $blog_data[0]->title : ''; ?></h2>
                                <p class="font-light"> <?php echo $blog_data ? $blog_data[0]->description : ''; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4">
                <div class="left-side">
                    <!-- Search Bar Start -->
                    <div class="search-section">
                        <div class="input-group search-bar">
                            <input type="search" class="form-control search-input" placeholder="Search">
                            <button class="input-group-text search-button" id="basic-addon3">
                                <i class="fas fa-search text-color"></i>
                            </button>
                        </div>
                    </div>
                    <!-- Search Bar End -->

                    <!-- Popular Post Start -->
                    <div class="popular-post mt-4">
                        <div class="popular-title">
                            <h3>Popular Posts</h3>
                        </div>
                        <?php
                        if (!empty($blogs)) {
                            foreach ($blogs as $key => $blog) {
                                $url = base_url('blog/' . strtolower($blog->slug));
                                ?>
                                <div class="popular-image">
                                    <div class="popular-number">
                                        <h4 class="theme-color"><?php echo $key + 1; ?></h4>
                                    </div>
                                    <div class="popular-contain">
                                        <h3><a href="<?php echo $url; ?>" class="text-FFF"><?php echo $blog->title; ?></a></h3>
                                        <div class="review-box">
                                            <span class="font-light clock-time"><i
                                                    data-feather="clock"></i><?php echo date('d-M-Y', strtotime($blog->blogdate)); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- Popular Post End -->

                </div>
            </div>
        </div>
    </div>
</section>


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
<script type="text/javascript"
        src="https://platform-api.sharethis.com/js/sharethis.js#property=642c03d2faaa470019ff1ae6&product=inline-share-buttons&source=platform"
        async="async"></script>
</body>