<?php $slider_data = $this->md->select('tbl_banner');
?>
<!-- home slider start -->
<section class="th-hero-wrapper hero-4" id="hero">
    <div class="hero-slider-4 th-carousel number-dots" data-slide-show="1" data-md-slide-show="1" data-fade="true"
         data-dots="true" data-xl-dots="true" data-ml-dots="true" data-lg-dots="true">
        <?php
        if (!empty($slider_data)) {
        foreach ($slider_data as $slider) {
        ?>
        <div class="th-hero-slide">
            <div class="th-hero-bg" data-bg-src="<?php echo base_url($slider->path); ?>"><img
                        src="<?php echo base_url($slider->path); ?>" alt="Hero Image"></div>
            <div class="container">
                <div class="hero-style4"><span class="hero-subtitle" data-ani="slideindown" data-ani-delay="0.2s"><?php echo $slider->subtitle; ?></span>
                    <h1 class="hero-title" data-ani="slideindown" data-ani-delay="0.3s"><?php echo $slider->title ?></h1>
                    <p class="hero-text" data-ani="slideindown" data-ani-delay="0.5s">Take payments online with a
                        scalable platform that grows with your perfect business.</p><a href="<?php echo base_url('contact')?>"
                                                                                       class="th-btn style2"
                                                                                       data-ani="slideindown"
                                                                                       data-ani-delay="0.6s">Get A
                        Quote</a></div>
            </div>
        </div>
        <?php }
        } ?>
    </div>
</section>
<!-- home slider end -->