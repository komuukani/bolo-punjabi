<?php
echo $page_head;
?>
<body class="theme-color2 dark ltr">

<?php echo $page_header; ?>
<?php $this->load->view('mobileMenu'); ?>
<?php echo $page_breadcumb; ?>

<!-- Blog Section Start -->
<section class="left-sidebar-section masonary-blog-section section-b-space">
    <div class="container">
        <div class="row g-4">
            <div class="col-12 ratio3_2">
                <div class="row g-4">
                    <?php
                    if (!empty($blog)) {
                        foreach ($blog as $blog_data) {
                            $url = base_url('blog/' . strtolower($blog_data->slug));
                            ?>
                            <div class="col-lg-4 col-md-6">
                                <div class="card blog-categority">
                                    <a href="<?php echo $url; ?>" class="blog-img">
                                        <img data-src="<?php echo base_url($blog_data->path); ?>"
                                             class="card-img-top blur-up lazyload bg-img"
                                             style="height:300px;object-fit: cover"
                                             src="<?php echo base_url($blog_data->path); ?>"
                                             alt="<?php echo $blog_data->title; ?>">
                                    </a>
                                    <div class="card-body">
                                        <a href="<?php echo $url; ?>">
                                            <h2 class="card-title"><?php echo $blog_data->title; ?>
                                            </h2>
                                        </a>
                                        <div class="blog-profile">
                                            <div class="image-name">
                                                <h6><?php echo date('d-M-Y', strtotime($blog_data->blogdate)); ?></h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->


<?php echo $page_footer; ?>
<?php echo $page_footerscript; ?>
</body>