<?php
echo $page_head;
?>
<body>
<div class="main-wrapper">
    <?php echo $page_header; ?>
    <?php echo $page_breadcumb; ?>

    <div class="edu-privacy-policy-area edu-privacy-policy edu-section-gap bg-color-white">
        <div class="container">
            <div class="row g-5">
                <div class="content">
                    <div class="">
                        <?php
                        if (empty($policy)) :
                            echo "Sorry, content not available";
                        else :
                            foreach ($policy as $key => $policy_data) {
                                echo $policy_data->policy;
                            }
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $page_footer; ?>
</div>
<?php echo $page_footerscript; ?>
</body>