"use strict";
// Common delete function
const actionOnDatatable = ($action, $tbl, $id, $photo = null) => {
    var message = '';
    var data = {};
    if ($action === 'delete') {
        message = 'Once deleted, you will not be able to recover this data!';
        data = {tbl: $tbl, id: $id, path: $photo};
    }
    if ($action === 'inactive') {
        message = 'Once inactive, user will not able to access their account!';
        data = {tbl: $tbl, id: $id, type: 'inactive', mail: true};
    }
    if ($action === 'active') {
        message = 'Once Active, user will able to access their account.';
        data = {tbl: $tbl, id: $id, type: 'active', mail: true};
    }
    swal({
        title: 'Are you sure?',
        text: message,
        icon: 'warning',
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            var sendData = data;
            var url = (($action === 'delete') ? "Admin/Pages/delete" : "Admin/Pages/statusUpdate");
            jQuery.post(url, sendData, function (status) {
                // status [website status is live or demo]
                if (status === 'false') {
                    swal('Sorry, action does not perfom in demo mode!', {
                        icon: 'warning'
                    });
                } else {
                    swal('Yeah! Your Data or file has been ' + $action + '!', {
                        icon: 'success'
                    });
                    // ($action === 'delete') && $('a[data-itemid="' + $id + '"]').parent().parent().remove(); // remove deleted row from table
                    $('#' + $tbl + 'Table').DataTable().ajax.reload();  // refresh the datatable
                }
            });
        }
    });
};

// Get data from model - common
const getDataTable = ($tbl, $url, $column, $photo_column_name = null, $responsive = false) => {
    $(document).ready(function () {
        $('#' + $tbl + 'Table').DataTable({
            dom: 'Bfrtlip', // enable Export buttons, per page, search, pagination
            'processing': true, // true/false 
            'responsive': $responsive, // enable expand(+) mode (view more in table)
            'serverSide': true, // true/false 
            'serverMethod': 'post', // get/post
            'ajax': {
                'url': $url
            },
            'columns': $column,
            "fnDrawCallback": function () {
                $('[data-toggle="tooltip"]').tooltip(); // enable tooltip in datatable
            },
            "columnDefs": [
                {"sortable": false, "targets": [0]}
            ]
        });
        $(document).on("click", "#deleteItem", function () {
            const id = $(this).data('itemid'); // getting record ID   
            actionOnDatatable('delete', $tbl, id, $photo_column_name);  // call delete item function
        }); // Delete Call
        $(document).on("click", "#inactiveItem", function () {
            const userid = $(this).data('userid'); // getting User ID      
            actionOnDatatable('inactive', $tbl, userid);  // call inactive item function 
        }); // InActive Call
        $(document).on("click", "#activeItem", function () {
            const userid = $(this).data('userid'); // getting User ID      
            actionOnDatatable('active', $tbl, userid);  // call active item function 
        }); // Active Call
        $(document).on("click", "#roleID", function () {
            const roleid = $(this).data('roleid'); // getting User ID      
            var data = {roleid: roleid};    //Role ID      
            $('#permission_area').html('<h4>Loading...</h4>');
            var url = "Admin/Pages/get_permission_table";
            jQuery.post(url, data, function (data) { 
                jQuery('#permission_area').html(data);
            });
        });
    });
};

// Add More Content JQUERY (Add/Remove) 
$(document).on("click", ".addContent .addMore", function () {
    let minus = '<button title="Remove Item" class="btn btn-md btn-danger removeLast" type="button"><i class="fa fa-minus"></i></button>';
    let content = $(this).parent().clone(true);  // clone the input 
    content.find('input,textarea').val('');  // Clear Input & Textarea value
    content.find('button').replaceWith(minus);  // find + plus and replace with -
    $(this).parent().parent().append(content);    // append it onto addContent
});
$(document).on("click", ".addContent .removeLast", function () {
    $(this).closest('.item').remove();  // remove closest item
});

// Check all radio button for pages
$("#check_all").click(function () {
    $('#radio-pages input:checkbox').not(this).prop('checked', this.checked);
});

// Check all radio/checkbox button in permission
const checkAllPermission = (e) => {
    $(e).parent().parent().parent().find('input:checkbox').not(e).prop('checked', e.checked).prop('disabled', e.checked);
};

// get Submenu Position
$(document).on("change", "#setSubmenuPosition", function () {
    let mainMenuId = $(this).val();  // get mainmenu id 
    var data = {mainMenuId: mainMenuId};
    var url = "Admin/Pages/get_submenuPosition";
    jQuery.post(url, data, function (data) {
        jQuery('#getSubmenuPosition').html(data);
    });
});

// Hide/Show password
$(document).on("click", ".pass_hide_show .action-btn i", function () {
    const textBox = $(this).parent().prev();   // Get Textbox type 
    (textBox.attr('type') === 'text') ? ($(this).removeClass('fa-eye-slash').addClass('fa-eye').attr('title', 'Show Password').parent().prev().attr('type', "password")) : ($(this).addClass('fa-eye-slash').removeClass('fa-eye').attr('title', 'Hide Password').parent().prev().attr('type', "text"));
}); 

// Load Datatables to table (Multiple Datatable in same page)
$(document).ready(function () {
    $('table#dataTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'responsive': true
    });
});